import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import * as firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
import { StringService } from './static/string.service';
import { NodeService } from './static/node.service';
import { User } from '../model/user';
import { Subject, Observable } from 'rxjs';
import { HttpParams, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { findReadVarNames } from '@angular/compiler/src/output/output_ast';
import * as _ from 'lodash';
import { ArrayService } from './static/array.service';
import { daLocale } from 'ngx-bootstrap';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private userAuth: firebase.User;

  private _userData = new Subject<User>();
  userData = this._userData.asObservable();
  user: User;
  unsub: any;
  private db: firebase.firestore.Firestore;
  private userRoles: string[];

  constructor(private router: Router, private str: StringService,
    private node: NodeService, private http: HttpClient, private arr: ArrayService) {
    this.initializer();
  }

  initializer(): void {
    this.db = firebase.firestore();
    //const settings = {/* your settings... */ timestampsInSnapshots: true};
    //this.db.settings(settings);

    firebase.auth().onAuthStateChanged((user) => {

      let sessionTimeout = null;

      console.log("Checking Auth");

      if (user !== null) {
        console.log('user', user);

        this.userAuth = user;
        // if (!this.user) {
          console.log('user2', user);

          //Get user informations
          this.unsub = this.db.collection(this.node.users).doc(user.uid).onSnapshot((doc) => {
            if (doc.exists) {
              if (location.pathname == '/') this.router.navigate(['/dashboard/content']);
              let data = doc.data();
              this._userData.next(data as User);
              let user = data as User;
              this.user = user;
              console.log('INFO USER UPDATE', this.user);
            }
          });

          // User is logged in.
          // Fetch the decoded ID token and create a session timeout which signs the user out.
          user.getIdTokenResult().then((idTokenResult) => {
            // Make sure all the times are in milliseconds!
            const authTime = idTokenResult.claims.auth_time * 1000;
            const sessionDuration = 1000 * 60 * 4000; /// 
            const millisecondsUntilExpiration = sessionDuration - (Date.now() - authTime);
            sessionTimeout = setTimeout(() => this.signOut(), millisecondsUntilExpiration);
          });

        // }

      } else {
        // User is logged out.
        // Clear the session timeout.
        sessionTimeout && clearTimeout(sessionTimeout);
        sessionTimeout = null;

        if (this.unsub) this.unsub();
        this.router.navigate(['/']);
      }
    });
  }

  getUser(user: firebase.User): firebase.firestore.DocumentReference {
    return this.db.collection(this.node.users).doc(user.uid);
  }

  updatePass(pass: string): Promise<void> {
    return this.currentUser.updatePassword(pass);
  }

  get authenticated(): boolean {
    console.log("USER", this.currentUser);
    return this.currentUser !== null;
  }

  get currentUser(): firebase.User {
    return this.userAuth;
  }

  private updateUserData() {
    let str = this.str;
    let user = this.currentUser;
    if (user) {
      let data: any = {
        email: user.email,
        displayName: user.displayName,
        uid: user.uid
      };

      this.db = firebase.firestore();
      const usersRef = this.db.collection(this.node.users).doc(data.uid);
      usersRef.get().then((doc) => {
        console.log("GET DOC UPDATE");

        if (doc.exists) {
          console.log("USER UPDATE");

          usersRef.update(data);
        } else {
          data.roles = { root: true };
          data.city = { name: str.defaultCity, code: str.defaultCityCode };
          data.sector = str.simvis;
          data.subSector = str.publicWatchSystem;
          ///Add City and other datas
          usersRef.set(data, { merge: true });
        }
      });
    }
  }

  private oAuthSignIn(provider: any): Promise<any> {
    if (!firebase.auth().currentUser) {
      return firebase.auth().signInWithRedirect(provider)
        .then((result) => {
          console.log("oAuthSignIn");
          console.log("Login", JSON.stringify(result));

        }).catch(error => console.log(error));
    } else {
      console.log("oAuthSignOut");

      this.signOut();
    }
  }

  // getRedirectResult(): void {
  //   firebase.auth().getRedirectResult().then((result) => {
  //     if (result.credential) {
  //       // This gives you a Google Access Token. You can use it to access the Google API.
  //       this.router.navigate(['/dashboard/content']);
  //       this.updateUserData();
  //       let credential: any = result.credential;
  //       let token = credential.accessToken;
  //       // [START_EXCLUDE]
  //       console.log(token);
  //     } else {
  //       console.log("null");
  //       // [END_EXCLUDE]
  //     }
  //     // The signed-in user info.
  //     var user = result.user;
  //   }).catch(function (error) {
  //     console.log(error);

  //   });
  // }

  public fetchSignInForEmail(email: string): Promise<string[]> {
    return firebase.auth().fetchSignInMethodsForEmail(email);
  }

  public createUserAccount(email: string, pass: string): Promise<firebase.auth.UserCredential> {
    return firebase.auth().createUserWithEmailAndPassword(email, pass);
  }
  // Cloud Functions ----------------------------- START
  createUser(doc: User): Observable<Object> {
    const params = new HttpParams().set('email', doc.email)
      .set('password', doc.password)
      .set('name', doc.fullName)
    return this.http.get(environment.createUser, { params });
  }
  // Cloud Functions ----------------------------- END

  resetPassword(email: string): Promise<void> {
    return firebase.auth().sendPasswordResetEmail(email).then(function () {
      // Email sent.
      console.log('send');

    }).catch(function (error) {
      // An error happened.
    });
  }

  public signIn(email: string, pass: string): Promise<firebase.auth.UserCredential> {
    return firebase.auth().signInWithEmailAndPassword(email, pass);
  }

  public googleSignIn(): Promise<any> {
    const provider = new firebase.auth.GoogleAuthProvider();
    provider.setCustomParameters({
      prompt: 'select_account'
    });
    return this.oAuthSignIn(provider);
  }

  public signOut(): void {
    firebase.auth().signOut()
      .then(() => {
        this._userData = new Subject<User>()
        this.userAuth = undefined;
        this.user = undefined;
        this.router.navigate(['/']);
        console.log("SignOUT");
      }).catch(error => console.log(error));
  }

  /// Role-based Auth ////

  canRead(): boolean {
    const allowed = ['admin', 'editor', 'subscriber'];
    return this.matchingRole(allowed);
  }

  canEdit(role: string[], redirect?: boolean): boolean {
    let allow: boolean = false;
    for (let i = 0; i < role.length; i++) {
      allow = this.matchingRole(role[i]);
      if (!allow) break;
    }
    if (redirect && !allow) this.router.navigate(['/dashboard/content']);
    return allow;
  }

  canDelete(): boolean {
    const allowed = ['admin']
    return this.matchingRole(allowed);
  }


  /// Helper to determine if any matching roles exist
  private matchingRole(allowedRoles, role?: string): boolean {
    if (!this.userRoles) this.userRoles = _.keys(_.get(this.user, 'roles'));
    let b = !_.isEmpty(_.intersection(allowedRoles, this.userRoles));
    //Level Thanos
    if (this.userRoles.indexOf(this.str.thanos) >= 0) b = true;

    return b
  }

  // determines if user has matching role
  private checkAuthorization(allowedRoles: string[]): boolean {
    const user = this.user;
    if (user === undefined) return false;
    for (const role of allowedRoles) {
      if (user.roles[role]) {
        return true;
      }
    }
    return false;
  }
}
