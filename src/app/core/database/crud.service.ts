import { Injectable } from '@angular/core';
import { Service } from '../../model/service';
import { HttpClient, HttpParams } from '@angular/common/http';
// import * as firebase from 'firebase';
// import 'firebase/firestore';
// import 'firebase/storage';
import * as firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/storage';

import { AuthService } from '../../core/auth.service';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { NodeService } from '../static/node.service';
import { Address } from '../../model/address';
import { Person } from '../../model/person';
import { Group } from '../../model/group';
import { District } from '../../model/district';
import { User } from 'src/app/model/user';
import { Core } from 'src/app/model/core';
import { StringService } from '../static/string.service';


@Injectable({
  providedIn: 'root'
})
export class CrudService {

  private db: firebase.firestore.Firestore;

  constructor(private http: HttpClient,
    private auth: AuthService,
    private node: NodeService,
    private str: StringService) {
    this.db = firebase.firestore();
  }

  getOldFamilyData(id: string): void {
    window.open('https://simvis.com.br/aplicacao/simvis/classes/imprimir-familia.php' 
      + '?key=MTdDb7LGkgrvLPVyhamFAzQJWQALRzEq' 
      + '&id_family=' + id
      + '&user_id=101' 
      + '&user_city=SERRINHA' 
      + '&user_uf=BA' 
      + '&user_db=u282363839_cg'
      + '&user_sector=SECRETARIA'
      + '&user_address=RUAXXX'
      + '&cnpj=26.834.179/0001-76');
  
  }

  //ElasticSearch by Cloud Functions ----------------------------- START
  getReportData(doc: any): Observable<Object> {
    let params = new HttpParams();
    params = params.append('startDate', String(Math.round(doc.startDate.getTime() / 1000)));
    params = params.append('endDate', String(Math.round(doc.endDate.getTime() / 1000)));
    params = params.append('index', doc.index);
    params = params.append('cityCode', doc.cityCode);

    //if (doc.userUid) params = params.append('userUid', doc.userUid);
    doc.filter.forEach((f, i) => {
      // console.log('filter: ' + f, i);
      if(f != -1) params = params.append('filter' + (i + 1), f);
    });
    
    console.log('params', params);
    console.log('params', doc);

    return this.http.get(environment.getReportData, { params });
  }

  searchPeople(query: string): Observable<Object> {
    const params = new HttpParams().set('search', query);
    return this.http.get(environment.searchPeople, { params });
  }

  searchUsers(query: string, code: string): Observable<Object> {
    const params = new HttpParams().set('search', query).set('code', code);
    return this.http.get(environment.searchUsers, { params });
  }

  //ElasticSearch by Cloud Functions ----------------------------- 

  //Firestore ----------------------------------------------------- START
  getReportDataFire(doc: any): firebase.firestore.Query {

    return null;
  }

  setReportFilter(doc: any, query: firebase.firestore.Query): firebase.firestore.Query {
    let index = doc.index;
    if (index == this.str.serviceHc) {
      query.where(doc.filter$[0].prop, '==', doc.filter$[0].index)
    }

    return this.limitData(query);

  }
  limitData(query: firebase.firestore.Query, limit?: number): firebase.firestore.Query {
    if (limit) {
      return query.limit(limit);
    } else {
      return query;
    }
  }

  listDistrict(code: string, limit?: number): firebase.firestore.Query {
    let query: firebase.firestore.Query = this.db.collection(this.node.districts)
      .where('city.code', '==', code);
    return this.limitData(query, limit);
  }

  getNotes(uid: string, limit?: number): firebase.firestore.Query {
    //let query: firebase.firestore.Query = this.db.collection(this.node.families).doc(uid).collection(this.node.notes)
    let query: firebase.firestore.Query = this.db.collection(this.node.familyNotes)
      .where('familyUid', '==', uid)
      .where("city.code", "==", this.auth.user.city.code)
      .orderBy("updateAt", "desc");
    return this.limitData(query, limit);
  }

  getGroup(uid: string): firebase.firestore.Query {
    return this.db.collection(this.node.groups).where('familyUid', '==', uid);
  }

  getPeople(uid: string): firebase.firestore.Query {
    return this.db.collection(this.node.people).where('familyUid', '==', uid).where('deleted', '==', false);
  }

  getPerson(uid: string): firebase.firestore.DocumentReference {
    return this.db.collection(this.node.people).doc(uid);
  }

  getFamilyPerson(doc: any): firebase.firestore.DocumentReference {
    return this.db.collection(this.node.people).doc(doc.person.uid);
  }

  getAddress(uid: string): firebase.firestore.Query {
    //return this.db.collection(this.node.families).doc(uid).collection(this.node.address).where('familyUid', '==', uid);
    return this.db.collection(this.node.address).where('familyUid', '==', uid);
  }

  getService(uid: string, node: string, limit?: number): firebase.firestore.Query {
    //let query: firebase.firestore.Query = this.db.collection(this.node.families)
    let query: firebase.firestore.Query = this.db.collection(node)
      .where('familyUid', '==', uid)
      .where("city.code", "==", this.auth.user.city.code)
      .orderBy("updateAt", "desc");
    return this.limitData(query, limit);
  }

  getServiceBySector(node: string): firebase.firestore.Query {
    return this.db.collection(node)
      .where("user.subSector", "==", this.auth.user.subSector)
      .where('active', '==', true)
      .where("city.code", "==", this.auth.user.city.code)
      .orderBy("updateAt", "desc");
  }

  getServiceRoute(node: string): firebase.firestore.Query {
    return this.db.collection(node)
      .where("sector", "==", this.auth.user.subSector)
      .where('active', '==', true)
      .where("city.code", "==", this.auth.user.city.code)
      .orderBy("updateAt", "desc");
  }


  updateAddressCoords(doc: Address): Promise<void> {
    const coords = new firebase.firestore.GeoPoint(doc.coords._lat, doc.coords._long);
    //return this.db.collection(this.node.families).doc(doc.familyUid).collection(this.node.address).doc(doc.uid).update({ coords: coords });
    return this.db.collection(this.node.address).doc(doc.uid).update({ coords: coords });
  }

  writeStaticString(doc: any, uid: string) {
    this.db.collection(this.node.static).doc(uid).update(doc);
  }

  writeCity(doc: any, code: string): Promise<void> {
    let ref: firebase.firestore.CollectionReference;
    ref = this.db.collection(this.node.cities);
    // doc.uid = code;
    // console.log(doc);

    // return ref.set(JSON.parse(JSON.stringify(doc)));
    return this.write(doc, ref);
  }

  writePerson(doc: Person): Promise<void> {
    let ref: firebase.firestore.CollectionReference;
    ref = this.db.collection(this.node.people);

    return this.write(doc, ref);
  }

  writeGroupMeet(doc: Group): Promise<void> {
    let ref: firebase.firestore.CollectionReference;
    let code = this.auth.user.city.code;
    ref = this.db.collection(this.node.groupMeeting);

    return this.write(doc, ref);
  }

  writeCoreMeet(doc: Core): Promise<void> {
    let ref: firebase.firestore.CollectionReference;
    let code = this.auth.user.city.code;
    ref = this.db.collection(this.node.coreMeeting);

    return this.write(doc, ref);
  }

  writeAddress(doc: Address): Promise<void> {
    let ref: firebase.firestore.CollectionReference;
    //ref = this.db.collection(this.node.families).doc(doc.familyUid).collection(this.node.address);
    ref = this.db.collection(this.node.address);

    return this.write(doc, ref)
  }

  private write(data: any, collection: firebase.firestore.CollectionReference): Promise<void> {
    let docRef: firebase.firestore.DocumentReference;

    let doc = JSON.parse(JSON.stringify(data));
    doc.updateAt = firebase.firestore.FieldValue.serverTimestamp();
    console.log(doc);
    if (!doc.uid) {
      docRef = collection.doc();
      doc.uid = docRef.id;
      data.uid = doc.uid;
      doc.writeAt = firebase.firestore.FieldValue.serverTimestamp();
      return docRef.set(doc);
    } else if (doc.authUid) {
      doc.writeAt = firebase.firestore.FieldValue.serverTimestamp();
      return collection.doc(doc.authUid).set(doc);
    } else {
      delete doc.writeAt;
      docRef = collection.doc(doc.uid);

      return docRef.update(doc);
    }
  }

  //serviceRef(service: string): firebase.firestore.DocumentReference {
  //return this.db.collection(service).doc();
  //}

  writeService(doc: Service): Promise<void> {
    let ref: firebase.firestore.CollectionReference;
    console.log('WriteService =  Crud', doc);

    //ref = this.db.collection(this.node.families).doc(doc.familyUid).collection(doc.node);
    ref = this.db.collection(doc.node);
    return this.write(doc, ref);
  }

  writeRouteAnswer(doc: Service): Promise<void> {
    let ref: firebase.firestore.CollectionReference;

    ref = this.db.collection(doc.node);
    return this.write(doc, ref);
  }

  writeNote(doc: any): Promise<void> {
    console.log('note', doc);

    let ref: firebase.firestore.CollectionReference
    //ref = this.db.collection(this.node.families).doc(doc.familyUid).collection(this.node.notes)
    ref = this.db.collection(this.node.familyNotes)
    return this.write(doc, ref);
  }

  writeNoteFsp(doc: any): Promise<void> {
    let ref: firebase.firestore.CollectionReference;
    /*     ref = this.db.collection(this.node.families)
          .doc(doc.familyUid)
          .collection(this.node.serviceFsp)
          .doc(doc.parentUid) */
    ref = this.db.collection(this.node.fspNotes);
    return this.write(doc, ref);
  }

  writeNoteHc(doc: any): Promise<void> {
    let ref: firebase.firestore.CollectionReference;
    /* ref = this.db.collection(this.node.families)
      .doc(doc.familyUid)
      .collection(this.node.serviceHc)
      .doc(doc.parentUid) */
    ref = this.db.collection(this.node.hcNotes);
    return this.write(doc, ref);
  }

  writeNoteFindsVisit(doc: any): Promise<void> {
    let ref: firebase.firestore.CollectionReference;
    /* ref = this.db.collection(this.node.families)
      .doc(doc.familyUid)
      .collection(this.node.serviceVisit)
      .doc(doc.parentUid) */
      ref = this.db.collection(this.node.visitNotes);
    return this.write(doc, ref);
  }

  writeNoteService(doc: any, node: string): Promise<void> {
    let ref: firebase.firestore.CollectionReference;
    /* ref = this.db.collection(this.node.families)
      .doc(doc.familyUid)
      .collection(node)
      .doc(doc.parentUid) */
    ref = this.db.collection(node);
    return this.write(doc, ref);
  }

  writeGroup(doc: Group): Promise<void> {
    let ref: firebase.firestore.CollectionReference;
    //ref = this.db.collection(this.node.cities).doc(this.auth.user.city.code).collection(this.node.groups)
    ref = this.db.collection(this.node.groups);
    return this.write(doc, ref);
  }

  writeCore(doc: Group): Promise<void> {
    let ref: firebase.firestore.CollectionReference;
    ref = this.db.collection(this.node.cores);
    return this.write(doc, ref);
  }

  writeUser(doc: User): Promise<void> {
    let ref: firebase.firestore.CollectionReference;
    ref = this.db.collection(this.node.users);
    return this.write(doc, ref);
  }

  writeDistrict(doc: District): Promise<void> {
    let ref: firebase.firestore.CollectionReference;
    ref = this.db.collection(this.node.districts)
    return this.write(doc, ref);
  }

  writeFamily(doc: any): Promise<void> {
    delete doc.address;

    const familyRef = this.db.collection(this.node.families);
    const peopleRef = this.db.collection(this.node.people);

    doc.familyUid = familyRef.doc().id;

    let user = this.auth.user; 
    let family = {
      familyUid: doc.familyUid,
      updateAt: firebase.firestore.FieldValue.serverTimestamp(),
      writeAt: firebase.firestore.FieldValue.serverTimestamp(),
      city: user.city,
      user: {
        fullName: user.fullName,
        subSector: user.subSector,
        uid: user.uid
      },
      updateBy: {
        fullName: user.fullName,
        subSector: user.subSector,
        uid: user.uid
      }
    }
    
    familyRef.doc(doc.familyUid).set(family);
      
    return this.write(doc, peopleRef);
  }

  uploadFile(file: File, action: string, prop?: string): firebase.storage.UploadTask {
    const storageRef = firebase.storage().ref();
    return storageRef.child("/" + action + "/" + file.name + "-" + Date.now())
      .put(file);
  }
/* 
  getCityUsers(code: string, subSector: number): firebase.firestore.Query {
    return this.db.collection(this.node.cities).doc(code).collection(this.node.users + '-' + this.node.city).where('subSector', '==', subSector);
  } */

  getUsers(code: string, subSector: number): firebase.firestore.Query {
    console.log(`SS? `, subSector);
    
    return this.db.collection(this.node.users).where('subSector', '==', subSector)
      .where('city.code', '==', code);
  }

  getCity(code: string): firebase.firestore.Query {
    return this.db.collection(this.node.cities)
      .where('code', '==', code);
  }

  getStaticString(uid: string): firebase.firestore.DocumentReference {
    return this.db.collection(this.node.static).doc(uid);
  }

  getPeopleGroups(uid: string, node: string): firebase.firestore.DocumentReference {
    return this.db.collection(node)
      .doc(uid);
  }

  listUser(code: string): firebase.firestore.Query {
    return this.db.collection(this.node.users).where('city.code', '==', code);
  }

  listGroup(limit?: number): firebase.firestore.Query {
    let query: firebase.firestore.Query = this.db.collection(this.node.groups)
      .orderBy('updateAt', 'desc');
    return this.limitData(query, limit);
  }

  listMeeting(doc: any, node: string, prop: string): firebase.firestore.Query {
    console.log(node);
    let code = this.auth.user.city.code;
    return this.db.collection(node).where('city.code', '==', code).where(prop, '==', doc.uid)
      .orderBy('updateAt', 'desc')
      .limit(7);
  }

  listCores(limit?: number): firebase.firestore.Query {
    let query: firebase.firestore.Query = this.db.collection(this.node.cores)
      .orderBy('updateAt', 'desc');

    return this.limitData(query, limit);
  }

  listPeople(uid: string, node: string): firebase.firestore.Query {
    return this.db.collection(this.node.people);
  }

  listNotesFsp(doc: any, limit?: number): firebase.firestore.Query {
    let query: firebase.firestore.Query = this.db.collection(this.node.fspNotes).where('parentUid', '==', doc.uid)
      .orderBy("updateAt", "desc");
    return this.limitData(query, limit);
  }

  listNotesHc(doc: any, limit?: number): firebase.firestore.Query {
    let query: firebase.firestore.Query = this.db.collection(this.node.hcNotes).where('parentUid', '==', doc.uid)
      .orderBy("updateAt", "desc");
    return this.limitData(query, limit);
  }

  listNotesFindsVisit(doc: any): firebase.firestore.Query {
    let query: firebase.firestore.Query = this.db.collection(this.node.visitNotes).limit(3)
      .where('parentUid', '==', doc.uid);
    return query;
  }

  //On Documents change
  onDocChanges(doc: any, change: string, list: any): any {
    const index = list.map(d => d.uid).indexOf(doc.uid)

    if (change === "added") {
      list.push(doc)
    }
    if (change === "modified") {
      list[index] = doc
    }
    if (change === "removed") {
      list.splice(index, 1)
    }
    return list
  }

  //Firestore ----------------------------------------------------- END
}
