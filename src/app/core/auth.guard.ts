import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { AuthService } from './auth.service';
import { ActionService } from './util/action.service';
import { StringService } from './static/string.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private auth: AuthService, 
              private router: Router,
              private act: ActionService,
              private str: StringService) {

  }
  
  subscription: Subscription;

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      if(this.auth.authenticated) {
        //set unsub lit to detach onSnapshot
        // if(!this.act.text && sessionStorage.length) {
        //   this.act.text = JSON.parse(sessionStorage.getItem(this.str.text));
        // } else {
        //   this.act.getTextService();
        // }
        if(!this.act.unsubList) this.act.unsubList = [];
        return true;
      } else {
        this.router.navigate(['/']);
        return false;  
      }
  }
}
