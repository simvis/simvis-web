import * as _ from "lodash";
import { Injectable } from '@angular/core';
import * as pdfmake from 'pdfmake/build/pdfmake';
// import { createPdf } from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import * as moment from 'moment';
import { ArrayService } from '../static/array.service';
import { NodeService } from '../static/node.service';
import * as firebase from 'firebase/app';
import 'firebase/firestore';
//import undefined = require('firebase/empty-import');
moment.locale('pt-br');

const style = {
  header: {
    color: '#333333',
    fontSize: 14,
    bold: true,
  },
  subheader: {
    color: '#333333',
    fontSize: 12,
  },
  title: {
    color: '#333333',
    fontSize: 12,
    bold: true,
    margin: [0, 10, 0, 5]
  },
  text: {
    color: '#333333',
    fontSize: 10,
    margin: [0, 5, 0, 15]
  },
  footer: {
    color: '#333333',
    fontSize: 10
  },
  table: {
    color: '#333333',
    borderColor: '#333333',
    fontSize: 10,
    margin: [0, 5, 0, 15]
  },
  tableHeader: {
    bold: true,
    fontSize: 12,
    color: '#333333'
  }
};

@Injectable({
  providedIn: 'root'
})

export class PrintService {
  pdfMake: any;
  city: any;

  constructor(private arr: ArrayService,
    private node: NodeService) {
    this.pdfMake = pdfmake;
    this.pdfMake.vfs = pdfFonts.pdfMake.vfs;
  }

  //Nome Idade NIS/Responsável Celular Data Data Data Data Data Data Data 
  public frequency(array: any, city: any): any {
    let header = ['Nome', 'Idade', 'NIS Resp.', 'Telefone', 'Data', 'Data', 'Data', 'Data', 'Data', 'Data', 'Data'];
    let width = ['*', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;

    //set dates
    let dates = array[0][0].dates;
    let list = ['', '', '', ''];
    for(let i = 0; i < 7; i++) {
      (dates[i]) ? list.push(this.formatDate(dates[i]) || '') : list.push('----');
    }
    body.push(list);

    array[0].forEach((doc: any) => {
      
      body.push([
        doc.name || '',
        this.calcAge(doc.birthday) || '',
        this.formatNis(doc.nis) || '',
        this.formatPhone(doc.phoneNum) || '',

        doc.frequency[0] || '',
        doc.frequency[1] || '',
        doc.frequency[2] || '',
        doc.frequency[3] || '',
        doc.frequency[4] || '',
        doc.frequency[5] || '',
        doc.frequency[6] || '',
      ]);

    });

    let content: any = [
      { text: 'FREQUENCIA', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    this.printPdf(content, city, 'landscape');
    // return content;
  }

  private scfv(array: any, city: any): any {
    let header = ['Nome', 'Idade', 'NIS/CPF Resp.', 'Situação', 'Status'];
    let width = ['*', 'auto', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any) => {
      let item = doc._source ? doc._source : doc;
      if (item.reponsable) {
        item.doc = item.reponsable.cpf ? item.reponsable.cpf : item.reponsable.nis;
      }

      body.push([
        item.person.name || '',
        this.calcAge(item.person.birthday) || '',
        item.doc || '',
        arr.situationType[item.situation].text || '',
        arr.status[item.status].text || ''
      ])
    });

    let content: any = [
      { text: 'RELATÓRIO SCFV', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  private interview(array: any, city: any): any {
    let header = ['Responsável', 'NIS', 'Finalidade', 'Modalidade', 'Data', 'Cód. familiar'];
    let width = ['*', 'auto', 'auto', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any) => {
      let item = doc._source ? doc._source : doc;
      // let name = item.reponsable ? item.reponsable.name : '';
      if (!item.reponsable) item.reponsable = {};

      body.push([
        item.reponsable.name || '',
        this.formatNis(item.reponsable.nis) || '',
        arr.goalType[item.goal].text || '',
        arr.modalityType[item.modality].text || '',
        this.formatDate(item.pickedDate) || '',
        item.number || ''
      ])
    });

    let content: any = [
      { text: 'RELATÓRIO ENTREVISTA', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  // Sub-Setor Responsável familiar Nis Data Status

  private visit(array: any, city: any): any {
    let header = ['Sub-sertor', 'Responsável', 'NIS', 'Data', 'Status'];
    let width = ['*', 'auto', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any) => {
      let item = doc._source ? doc._source : doc;
      // let name = item.reponsable ? item.reponsable.name : '';

      if (!item.reponsable) item.reponsable = {};
      body.push([
        arr.sectorType[item.user.subSector].text || '',
        item.reponsable.name || '',
        this.formatNis(item.reponsable.nis) || '',
        this.formatDate(item.pickedDate) || '',
        arr.status[item.status].text || '',
      ])
    });

    let content: any = [
      { text: 'RELATÓRIO DE VISITAS', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  // Nome Nascimento Idade Parentesco

  private person(array: any, city: any): any {
    let header = ['Nome', 'Nascimento', 'Idade', 'Parentesco', 'NIS', 'CPF', 'Sexo', 'Telefone'];
    let width = ['*', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any) => {
      let item = doc._source ? doc._source : doc;
      // let name = item.reponsable ? item.reponsable.name : '';

      if (!item.reponsable) item.reponsable = {};
      body.push([
        item.name || '',
        this.formatDate(item.birthday) || '',
        this.calcAge(item.birthday) || '',
        arr.kinshipType[item.kinship].text || '',
        this.formatNis(item.nis) || '',
        this.formatCpf(item.cpf) || '',
        arr.gender[item.gender].text || '',
        this.formatPhone(item.phoneNum) || ''
      ])
    });

    let content: any = [
      { text: 'RELATÓRIO DE PESSOAS', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  //   Endereço: RUA SANTA CLARA, 206, VICENTE FERREIRA
  // Referencia: EM FRENTE A IGREJA CENTRAL

  private address(array: any, city: any): any {
    let header = ['Endereço', 'Número', 'Bairro', 'Cidade', 'Estado', 'Referência'];
    let width = ['*', 'auto', 'auto', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any) => {
      let item = doc._source ? doc._source : doc;
      // let name = item.reponsable ? item.reponsable.name : '';

      if (!item.reponsable) item.reponsable = {};
      body.push([
        item.street || '',
        item.number || '',
        item.district || '',
        item.city.name || '',
        arr.states[item.state].text || '',
        item.reference || '',
      ])
    });

    let content: any = [
      { text: 'RELATÓRIO DE ENDEREÇOS', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  // Sub-Setor Técnico Data Descrição

  private noteFamily(array: any, city: any): any {
    let header = ['Descrição', 'Técnico', 'Sub-setor', 'Data'];
    let width = ['*', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any) => {
      let item = doc._source ? doc._source : doc;
      // let name = item.reponsable ? item.reponsable.name : '';


      body.push([
        item.text || '',
        item.user.fullName || '',
        arr.sectorType[item.user.subSector].text || '',
        this.formatDate(this.tsToDate(item.writeAt).toString()) || ''
      ])
    });

    let content: any = [
      { text: 'RELATÓRIO DE ANOTAÇÕES', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  // Sub-Setor Grupo Inclusão Reunião Status

  private group(array: any, city: any): any {
    let header = ['Técnico', 'Sub-setor', 'Grupo', 'Inclusão', 'Reunião', 'Status'];
    let width = ['*', 'auto', 'auto', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any) => {
      let item = doc._source ? doc._source : doc;
      // let name = item.reponsable ? item.reponsable.name : '';


      body.push([
        item.user.fullName || '',
        arr.sectorType[item.user.subSector].text || '',
        item.group.name || '',
        this.formatDate(this.tsToDate(item.writeAt).toString()) || '',
        this.formatDate(this.tsToDate(item.writeAt).toString()) || '',
        arr.status[item.status].text || ''

      ])
    });

    let content: any = [
      { text: 'RELATÓRIO DE GRUPO', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  //Origem Destino Data encaminhamento Status
  private route(array: any, city: any): any {
    let header = ['Origem', 'Destino', 'Data Encaminhamento', 'Status'];
    let width = ['*', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any) => {
      let item = doc._source ? doc._source : doc;
      // let name = item.reponsable ? item.reponsable.name : '';


      body.push([
        arr.sectorType[item.user.subSector].text || '',
        arr.sectorType[item.sector].text || '',
        this.formatDate(this.tsToDate(item.writeAt).toString()) || '',
        arr.status[item.status].text || ''

      ])
    });

    let content: any = [
      { text: 'RELATÓRIO DE ENCAMINHAMENTO', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }
  ///Sub-Setor Técnico Data Tipo Descrição
  private benefit(array: any, city: any): any {
    let header = ['Sub-Setor', 'Técnico', 'Data', 'Tipo', 'Descrição'];
    let width = ['*', 'auto', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any) => {
      let item = doc._source ? doc._source : doc;
      // let name = item.reponsable ? item.reponsable.name : '';


      body.push([
        arr.sectorType[item.user.subSector].text || '',
        item.user.fullName || '',
        this.formatDate(item.pickedDate) || '',
        arr.benefitType[item.benefit].text || '',
        item.detail || ''

      ])
    });

    let content: any = [
      { text: 'RELATÓRIO BENEFÍCIOS EVENTUAIS', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  //Beneficiário Nascimento Parentesco
  private milk(array: any, city: any): any {
    let header = ['Beneficiário', 'Nascimento', 'Parentesco'];
    let width = ['*', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any) => {
      let item = doc._source ? doc._source : doc;
      // let name = item.reponsable ? item.reponsable.name : '';


      body.push([
        item.person.name || '',
        this.formatDate(item.person.birthday) || '',
        arr.kinshipType[item.person.kinship].text || ''

      ])
    });

    let content: any = [
      { text: 'RELATÓRIO PPA LEITE', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  //Nº caso\livro Técnico Atendimento
  private notifications(array: any, city: any): any {
    let header = ['Nº caso\livro', 'Técnico', 'Atendimento'];
    let width = ['*', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any) => {
      let item = doc._source ? doc._source : doc;
      // let name = item.reponsable ? item.reponsable.name : '';


      body.push([
        item.number || '',
        item.user.fullName || '',
        this.formatDate(item.pickedDate) || ''

      ])
    });

    let content: any = [
      { text: 'RELATÓRIO NOTIFICAÇÕES', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  //Sub-Setor Técnico Data Status
  private fsp(array: any, city: any): any {
    let header = ['Sub-Setor', 'Técnico', 'Data', 'Status'];
    let width = ['*', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any) => {
      let item = doc._source ? doc._source : doc;
      // let name = item.reponsable ? item.reponsable.name : '';


      body.push([
        arr.sectorType[item.user.subSector].text || '',
        item.user.fullName || '',
        this.formatDate(this.tsToDate(item.writeAt).toString()) || '',
        arr.status[item.status].text || ''

      ])
    });

    let content: any = [
      { text: 'RELATÓRIO PAF', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }
  //Sub-Setor Usuário Data Público Status
  private hc(array: any, city: any): any {
    let header = ['Sub-Setor', 'Usuário', 'Data', 'Público', 'Status'];
    let width = ['*', 'auto', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any) => {
      let item = doc._source ? doc._source : doc;
      // let name = item.reponsable ? item.reponsable.name : '';


      body.push([
        arr.sectorType[item.user.subSector].text || '',
        item.user.fullName || '',
        this.formatDate(this.tsToDate(item.writeAt).toString()) || '',
        arr.targetHc[item.target].text || '',
        arr.status[item.status].text || ''

      ])
    });

    let content: any = [
      { text: 'RELATÓRIO CRIANÇA FELIZ', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  //Sub-Setor Técnico PntSUAS Data Status Informações Complementares
  private reference(array: any, city: any): any {
    let header = ['Sub-Setor', 'Técnico', 'PntSUAS', 'Data', 'Status', 'Informações Complementares'];
    let width = ['*', 'auto', 'auto', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any) => {
      let item = doc._source ? doc._source : doc;
      // let name = item.reponsable ? item.reponsable.name : '';

      let info = '';
      item.extraInformations.forEach(e => {
        info += arr.extraInformations[e].text + '\n';
      });

      body.push([
        arr.sectorType[item.user.subSector].text || '',
        item.user.fullName || '',
        item.number || '',
        this.formatDate(item.pickedDate) || '',
        arr.status[item.status].text || '',
        info || ''

      ])
    });

    let content: any = [
      { text: 'RELATÓRIO REFERENCIAMENTO', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  //Tipo Indivíduo Nascimento parentesco Recurso Efeito suspenso?
  private condition(array: any, city: any): any {
    let header = ['Tipo', 'Indivíduo', 'Nascimento', 'Parentesco', 'Recurso', 'Efeito suspenso?'];
    let width = ['*', 'auto', 'auto', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any) => {
      let item = doc._source ? doc._source : doc;
      // let name = item.reponsable ? item.reponsable.name : '';

      body.push([
        arr.conditionType[item.type].text || '',
        item.person.name || '',
        this.formatDate(item.person.birthday) || '',
        arr.kinshipType[item.person.kinship].text || '',
        arr.resourceType[item.resource].text || '',
        arr.yesNoType[item.effect].text || ''

      ])
    });

    let content: any = [
      { text: 'RELATÓRIO CONDICIONAMENTO', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  private contentPageBreak(i: number, len: number, content: any[], content2: any[]): any {
    if (i < len - 1) content2[2]['pageBreak'] = 'after';
    content = content.length >= 3 ? content.concat(content2) : content2;
    return content;
  }

  public setContent(array: any, city: any): void {
    let content = [];
    let content2 = [];
    array.forEach((e, index) => {

      switch (e[0].node) {
        case this.node.serviceCondition:
          content2 = this.condition(e, city);
          console.log('INDEx: ', index);

          // content = this.contentPageBreak(index, array.length, content, content2);
          // if (index < array.length - 1) content2[2]['pageBreak'] = 'after';
          // content = content.length >= 3 ? content.concat(content2) : content2;
          break;
        case this.node.serviceReference:
          content2 = this.reference(e, city);
          console.log('INDEx: ', index);

          // content = this.contentPageBreak(index, array.length, content, content2);
          // if (index < array.length - 1) content2[2]['pageBreak'] = 'after';
          // content = content.length >= 3 ? content.concat(content2) : content2;
          break;
        case this.node.serviceHc:
          content2 = this.hc(e, city);
          console.log('INDEx: ', index);

          // content = this.contentPageBreak(index, array.length, content, content2);
          // if (index < array.length - 1) content2[2]['pageBreak'] = 'after';
          // content = content.length >= 3 ? content.concat(content2) : content2;
          break;
        case this.node.serviceFsp:
          content2 = this.fsp(e, city);
          console.log('INDEx: ', index);

          // content = this.contentPageBreak(index, array.length, content, content2);
          // if (index < array.length - 1) content2[2]['pageBreak'] = 'after';
          // content = content.length >= 3 ? content.concat(content2) : content2;
          break;
        case this.node.serviceNotification:
          content2 = this.notifications(e, city);
          console.log('INDEx: ', index);

          // content = this.contentPageBreak(index, array.length, content, content2);
          // if (index < array.length - 1) content2[2]['pageBreak'] = 'after';
          // content = content.length >= 3 ? content.concat(content2) : content2;
          break;
        case this.node.serviceMilk:
          content2 = this.milk(e, city);
          console.log('INDEx: ', index);

          // content = this.contentPageBreak(index, array.length, content, content2);
          // if (index < array.length - 1) content2[2]['pageBreak'] = 'after';
          // content = content.length >= 3 ? content.concat(content2) : content2;
          break;
        case this.node.serviceBenefit:
          content2 = this.benefit(e, city);
          console.log('INDEx: ', index);

          // content = this.contentPageBreak(index, array.length, content, content2);
          // if (index < array.length - 1) content2[2]['pageBreak'] = 'after';
          // content = content.length >= 3 ? content.concat(content2) : content2;
          break;
        case this.node.serviceRoute:
          content2 = this.route(e, city);
          console.log('INDEx: ', index);

          // content = this.contentPageBreak(index, array.length, content, content2);
          // if (index < array.length - 1) content2[2]['pageBreak'] = 'after';
          // content = content.length >= 3 ? content.concat(content2) : content2;
          break;
        case this.node.familyNotes:
          content2 = this.noteFamily(e, city);
          console.log('INDEx: ', index);

          // content = this.contentPageBreak(index, array.length, content, content2);
          // if (index < array.length - 1) content2[2]['pageBreak'] = 'after';
          // content = content.length >= 3 ? content.concat(content2) : content2;
          break;
        case this.node.serviceGroup:
          content2 = this.group(e, city);
          console.log('INDEx: ', index);

          // content = this.contentPageBreak(index, array.length, content, content2);
          // if (index < array.length - 1) content2[2]['pageBreak'] = 'after';
          // content = content.length >= 3 ? content.concat(content2) : content2;
          break;
        case this.node.address:
          content2 = this.address(e, city);
          console.log('INDEx: ', index);

          // content = this.contentPageBreak(index, array.length, content, content2);
          // if (index < array.length - 1) content2[2]['pageBreak'] = 'after';
          // content = content.length >= 3 ? content.concat(content2) : content2;
          break;
        // case this.node.serviceAttendance:
        //   content2 = this.attendance(e, city);
        //   console.log('INDEx: ', index);

        //   if (index < array.length - 1) content2[2]['pageBreak'] = 'after';
        //   content = content.length >= 3 ? content.concat(content2) : content2;
        //   break;
        case this.node.people:
          content2 = this.person(e, city);
          console.log('INDEx: ', index);

          // content = this.contentPageBreak(index, array.length, content, content2);
          // if (index < array.length - 1) content2[2]['pageBreak'] = 'after';
          // content = content.length >= 3 ? content.concat(content2) : content2;
          break;
        case this.node.serviceVisit:
          content2 = this.visit(e, city);
          console.log('INDEx: ', index);

          // content = this.contentPageBreak(index, array.length, content, content2);
          // if (index < array.length - 1) content2[2]['pageBreak'] = 'after';
          // content = content.length >= 3 ? content.concat(content2) : content2;
          break;
        case this.node.serviceSCFV:
          content2 = this.scfv(e, city);
          console.log('INDEx: ', index);

          // content = this.contentPageBreak(index, array.length, content, content2);
          // if (index < array.length - 1) content2[2]['pageBreak'] = 'after';
          // content = content.length >= 3 ? content.concat(content2) : content2;
          break;
        case this.node.serviceInterview:
          content2 = this.interview(e, city);
          console.log('INDEx: ', index);

          // content = this.contentPageBreak(index, array.length, content, content2);
          // if (index < array.length - 1) content2[2]['pageBreak'] = 'after';
          // content = content.length >= 3 ? content.concat(content2) : content2;
          break;
        default:
          break;
      }
      content = this.contentPageBreak(index, array.length, content, content2);
      console.log('LOG test: ', e[0].node, content);
    });
    this.printPdf(content, city, 'landscape');

  }

  //Nome', 'Idade', 'Doc. Resp.', 'Tipo', 'Entrada', 'Saída', 'Profissional', 'Status
  private attendanceReport(array: any): any {
    let header = ['Nome', 'Idade', 'Doc. Resp.', 'Tipo', 'Entrada', 'Saída', 'Profissional', 'Status']
    let width = ['*', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto']
    let body = []
    //for data
    let arr = this.arr;
    array.forEach((doc: any) => {
      let item = doc._source ? doc._source : doc;
      if (item.reponsable) {
        item.doc = item.reponsable.cpf ? item.reponsable.cpf : item.reponsable.nis;
      }

      body.push([
        item.person.name || '',
        this.calcAge(item.person.birthday) || '',
        item.doc || '',
        arr.attendanceType[item.type].text || '',
        arr.inputType[item.input.type].text || '',
        arr.outputType[item.output.type].text || '',
        item.professional.info || '',
        arr.status[item.status].text || ''
      ])
    });

    let content: any = [
      { text: 'RELATÓRIO DE ATENDIMENTOS', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  // Indivíduo Nis Localidade idade Responsável
  private aepetiReport(array: any): any {
    let header = ['Indivíduo', 'NIS', 'Localidade', 'Idade', 'Responsável']
    let width = ['*', 'auto', 'auto', 'auto', 'auto'];
    let body = []
    //for data
    let arr = this.arr;
    array.forEach((doc: any) => {
      let item = doc._source ? doc._source : doc;
      if (!item.address) item.address = {};
      if (!item.responsible) item.responsible = {};

      body.push([
        item.person.name || '',
        this.formatNis(item.person.nis) || '',
        item.address.district || '',
        this.calcAge(item.person.birthday) || '',
        item.responsible.name || ''
      ])
    });

    let content: any = [
      { text: 'RELATÓRIO ANALÍTICO - AEPETI', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  //Responsável familiar Nis Localidade Início do acompanhamento
  private fspReport(array: any): any {
    let header = ['Responsável', 'NIS', 'Localidade', 'Início do acompanhamento'];
    let width = ['*', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any) => {
      let item = doc._source ? doc._source : doc;
      // let name = item.reponsable ? item.reponsable.name : '';
      if (!item.address) item.address = {};
      if (!item.responsible) item.responsible = {};


      console.log();

      body.push([
        item.responsible.name || '',
        this.formatNis(item.responsible.nis) || '',
        item.address.district || '',
        this.formatDate(this.tsToDate(item.writeAt).toString()) || ''
      ])
    });

    let content: any = [
      { text: 'RELATÓRIO ANALÍTICO - PAF', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  //Nome Idade Localidade Responsável Nis Responsável
  private hcReport(array: any): any {
    let header = ['Nome', 'Idade', 'Localidade', 'Responsável', 'NIS Responsável'];
    let width = ['*', 'auto', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any) => {
      let item = doc._source ? doc._source : doc;
      // let name = item.reponsable ? item.reponsable.name : '';
      if (!item.address) item.address = {};
      if (!item.responsible) item.responsible = {};

      body.push([
        item.person.name || '',
        this.calcAge(item.person.birthday) || '',
        item.address.district || '',
        item.responsible.name || '',
        this.formatNis(item.responsible.nis) || ''
      ])
    });

    let content: any = [
      { text: 'RELATÓRIO ANALÍTICO - CRIANÇA FELIZ', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  //Responsável Nis Data Status
  private complaintReport(array: any): any {
    let header = ['Responsável', 'NIS', 'Data', 'Status'];
    let width = ['*', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any) => {
      let item = doc._source ? doc._source : doc;
      // let name = item.reponsable ? item.reponsable.name : '';
      if (!item.address) item.address = {};
      if (!item.responsible) item.responsible = {};

      body.push([
        item.responsible.name || '',
        this.formatNis(item.responsible.nis) || '',
        this.formatDate(this.tsToDate(item.writeAt).toString()) || '',
        arr.status[item.status].text || ''
      ])
    });

    let content: any = [
      { text: 'RELATÓRIO ANALÍTICO - DENÚNCIA', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  //Idoso Nis Data Serviço
  private oldReport(array: any): any {
    let header = ['Idoso', 'NIS', 'Data', 'Serviço'];
    let width = ['*', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any) => {
      let item = doc._source ? doc._source : doc;
      // let name = item.reponsable ? item.reponsable.name : '';
      // if (!item.address) item.address = {};
      // if (!item.responsible) item.responsible = {};

      body.push([
        item.person.name || '',
        this.formatNis(item.person.nis) || '',
        this.formatDate(this.tsToDate(item.writeAt).toString()) || '',
        arr.serviceType[item.type].text || ''
      ])
    });

    let content: any = [
      { text: 'RELATÓRIO ANALÍTICO - SERVIÇO AO IDOSO', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  //Individuo Nis Localidade Idade Data de cadastramento
  private acessuasReport(array: any): any {
    let header = ['Individuoo', 'NIS', 'Localidade', 'Idade', 'Data de cadastramento'];
    let width = ['*', 'auto', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any) => {
      let item = doc._source ? doc._source : doc;
      // let name = item.reponsable ? item.reponsable.name : '';
      // if (!item.address) item.address = {};
      // if (!item.responsible) item.responsible = {};

      body.push([
        item.person.name || '',
        this.formatNis(item.person.nis) || '',
        arr.locationType[item.local].text || '',
        this.calcAge(item.person.birthday) || '',
        this.formatDate(this.tsToDate(item.writeAt).toString()) || ''
      ]);
    });

    let content: any = [
      { text: 'RELATÓRIO ANALÍTICO - ACESSUAS', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  //Responsável Nis Finalidade Modalidade Data Arquivo
  private interviewReport(array: any): any {
    let header = ['Responsável', 'NIS', 'Finalidade', 'Modalidade', 'Data', 'Arquivo'];
    let width = ['*', 'auto', 'auto', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any) => {
      let item = doc._source ? doc._source : doc;
      // let name = item.reponsable ? item.reponsable.name : '';
      if (!item.address) item.address = {};
      if (!item.responsible) item.responsible = {};

      body.push([
        item.responsible.name || '',
        this.formatNis(item.responsible.nis) || '',
        arr.goalType[item.goal].text || '',
        arr.modalityType[item.modality].text || '',
        this.formatDate(item.pickedDate) || '',
        item.number || ''
      ]);
    });

    let content: any = [
      { text: 'RELATÓRIO ANALÍTICO - ENTREVISTA', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  //Responsável Nis Data Status
  private pendencyReport(array: any): any {
    let header = ['Responsável', 'NIS', 'Data', 'Status'];
    let width = ['*', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any) => {
      let item = doc._source ? doc._source : doc;
      // let name = item.reponsable ? item.reponsable.name : '';
      if (!item.address) item.address = {};
      if (!item.responsible) item.responsible = {};

      body.push([
        item.responsible.name || '',
        this.formatNis(item.responsible.nis) || '',
        this.formatDate(this.tsToDate(item.writeAt).toString()) || '',
        arr.status[item.status].text || ''
      ]);
    });

    let content: any = [
      { text: 'RELATÓRIO ANALÍTICO - PENDENCIA', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  //Responsável Nis Origem Destino Data Status
  private routeReport(array: any): any {
    let header = ['Responsável', 'NIS', 'Origem', 'Destino', 'Data', 'Status'];
    let width = ['*', 'auto', 'auto', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any) => {
      let item = doc._source ? doc._source : doc;
      // let name = item.reponsable ? item.reponsable.name : '';
      // if (!item.address) item.address = {};
      if (!item.responsible) item.responsible = {};

      body.push([
        item.responsible.name || '',
        item.responsible.nis || '',
        arr.sectorType[item.user.subSector].text || '',
        arr.sectorType[item.sector].text || '',
        this.formatDate(this.tsToDate(item.writeAt).toString()) || '',
        arr.status[item.status].text || ''
      ]);
    });

    let content: any = [
      { text: 'RELATÓRIO ANALÍTICO - ENCAMINHAMENTO', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  //Nº NOME DO RESPONSÁVEL(BENEFICIÁRIO) DOCUMENTOS QUANTIDADE(LITROS) ASSINATURA DO RESPONSÁVEL
  private milkReport(array: any): any {
    let header = ['Nº', 'Nome do Responsável (Beneficiário)', 'Documentos', 'Quantidade (Litros)', 'Assinatura do Responsável'];
    let width = ['auto', '*', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any, i: number) => {
      let item = doc._source ? doc._source : doc;
      // let name = item.reponsable ? item.reponsable.name : '';
      // if (!item.address) item.address = {};
      if (!item.responsible) item.responsible = {};

      let name = 'Responsável: ' + item.responsible.name + '\n' + 'Beneficiário: ' + item.person.name;
      let document = 'CPF (Responsável): ' + this.formatCpf(item.responsible.cpf) + '\n'
        + 'NIS (Beneficiário): ' + this.formatNis(item.person.nis);

      body.push([
        i + 1 || '',
        name || '',
        document || '',
        '',
        ''
      ]);
    });

    let content: any = [
      { text: 'RELATÓRIO ANALÍTICO - PAA LEITE', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  // Sub-Setor Responsável familiar Nis Data Status
  private visitReport(array: any): any {
    let header = ['Sub-setor', 'Responsável familiar', 'NIS', 'Data', 'Status'];
    let width = ['*', 'auto', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any, i: number) => {
      let item = doc._source ? doc._source : doc;
      // let name = item.reponsable ? item.reponsable.name : '';
      // if (!item.address) item.address = {};
      if (!item.responsible) item.responsible = {};

      body.push([
        arr.sectorType[item.user.subSector].text || '',
        item.responsible.name || '',
        item.responsible.nis || '',
        this.formatDate(this.tsToDate(item.writeAt).toString()) || '',
        arr.status[item.status].text || ''
      ]);
    });

    let content: any = [
      { text: 'RELATÓRIO ANALÍTICO - VISITA', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  //Responsável familiar Nis Endereço Data Tipo Sub-Setor
  private benefitReport(array: any): any {
    let header = ['Responsável familiar', 'NIS', 'Endereço', 'Data', 'Tipo', 'Sub-setor'];
    let width = ['*', 'auto', 'auto', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any, i: number) => {
      let item = doc._source ? doc._source : doc;
      // let name = item.reponsable ? item.reponsable.name : '';
      if (!item.address) item.address = {};
      if (!item.responsible) item.responsible = {};

      let address = item.address.street + ' ,' + item.address.number + ' - ' + item.address.district
        + ', ' + item.address.city + ' - ' + item.address.state;

      body.push([
        item.responsible.name || '',
        item.responsible.nis || '',
        address || '',
        this.formatDate(this.tsToDate(item.writeAt).toString()) || '',
        arr.benefitType[item.benefit].text || '',
        arr.sectorType[item.user.subSector].text || ''
      ]);
    });

    let content: any = [
      { text: 'RELATÓRIO ANALÍTICO - BENEFÍCIOS EVENTUAIS', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  //Responsável familiar Nis Localidade Data do referenciamento
  private referenceReport(array: any): any {
    let header = ['Responsável familiar', 'NIS', 'Localidade', 'Data do Referenciamento'];
    let width = ['*', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any, i: number) => {
      let item = doc._source ? doc._source : doc;
      // let name = item.reponsable ? item.reponsable.name : '';
      if (!item.address) item.address = {};
      if (!item.responsible) item.responsible = {};

      body.push([
        item.responsible.name || '',
        item.responsible.nis || '',
        item.address.district || '',
        this.formatDate(this.tsToDate(item.writeAt).toString()) || ''
      ]);
    });

    let content: any = [
      { text: 'RELATÓRIO ANALÍTICO - REFERENCIAMENTO', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  //Tipo Indivíduo Nis ind Nis Resp Localidade Escola/Psf Recurso Efeito suspenso?
  private conditionReport(array: any): any {
    let header = ['Tipo', 'Indivíduo', 'NIS ind.', 'NIS resp.', 'Localidade', 'Escola/PSF', 'Efeito suspenso?'];
    let width = ['*', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any, i: number) => {
      let item = doc._source ? doc._source : doc;
      // let name = item.reponsable ? item.reponsable.name : '';
      if (!item.address) item.address = {};
      if (!item.responsible) item.responsible = {};

      body.push([
        arr.conditionType[item.type].text || '',
        item.person.name || '',
        this.formatNis(item.person.nis) || '',
        this.formatNis(item.responsible.nis) || '',
        item.address.district || '',
        item.local.name || '',
        arr.yesNoType[item.effect].text || ''
      ]);
    });

    let content: any = [
      { text: 'RELATÓRIO ANALÍTICO - CONDICIONALIDADE', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  //Sub-Setor Nome Telefone Função
  private usersReport(array: any): any {
    let header = ['Sub-Setor', 'Nome', 'Telefone', 'Função'];
    let width = ['auto', '*', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any, i: number) => {
      let item = doc._source ? doc._source : doc;
      // let name = item.reponsable ? item.reponsable.name : '';

      body.push([
        arr.sectorType[item.subSector].text || '',
        item.fullName || '',
        this.formatPhone(item.phone) || '',
        item.fuction || ''
      ]);
    });

    let content: any = [
      { text: 'RELATÓRIO ANALÍTICO - EQUIPE', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  //Nome Idade NIS Responsável Frequenta?
  private scfvReport(array: any): any {
    let header = ['Nome', 'Idade', 'NIS', 'Responsável', 'Núcleo', 'Frequencia'];
    let width = ['*', 'auto', 'auto', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any, i: number) => {
      let item = doc._source ? doc._source : doc;
      if (!item.core) item.core = {};
      if (!item.responsible) item.responsible = {};
      if (!item.meet) item.meet = 0;


      body.push([
        item.person.name || '',
        this.calcAge(item.person.birthday) || '',
        this.formatNis(item.person.nis) || '',
        item.responsible.name || '',
        item.core.name || '',
        arr.yesNoType[item.meet].text || ''
      ]);
    });

    let content: any = [
      { text: 'RELATÓRIO ANALÍTICO - SCFV', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  //Responsável Nis Atendimento Observação Status
  private fpgbReport(array: any): any {
    let header = ['Responsável', 'NIS', 'Atendimento', 'Observação', 'Status'];
    let width = ['*', 'auto', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any, i: number) => {
      let item = doc._source ? doc._source : doc;
      if (!item.responsible) item.responsible = {};

      body.push([
        item.responsible.name || '',
        item.responsible.nis || '',
        item.number || '',
        item.detail || '',
        arr.status[item.status].text || ''
      ]);
    });

    let content: any = [
      { text: 'RELATÓRIO ANALÍTICO - FPGB', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }
  
  //implement later
  private notificationReport(array: any): any {
    let header = ['Responsável', 'NIS', 'Atendimento', 'Observação', 'Status'];
    let width = ['*', 'auto', 'auto', 'auto', 'auto'];
    let body = [];
    //for data
    let arr = this.arr;
    array.forEach((doc: any, i: number) => {
      let item = doc._source ? doc._source : doc;
      if (!item.responsible) item.responsible = {};

      body.push([
        item.responsible.name || '',
        item.responsible.nis || '',
        item.number || '',
        item.detail || '',
        arr.status[item.status].text || ''
      ]);
    });

    let content: any = [
      { text: 'RELATÓRIO ANALÍTICO - FPGB', style: 'title' },
      this.createTable(body, header, width, 'headerLineOnly'),
      { text: 'Total de Registros: ' + array.length, style: 'text' }
    ];

    return content;
  }

  public setContentReport(array: any, city: any): void {
    let content = [];
    let content2 = [];
    array.forEach((e, index) => {

      if (e && e[0]) {
        switch (e[0].node) {
          case this.node.serviceFpgb:
            content2 = this.fpgbReport(e);
            console.log('INDEx: ', index);

            // content = this.contentPageBreak(index, array.length, content, content2);
            break;
          case this.node.serviceSCFV:
            content2 = this.scfvReport(e);
            console.log('INDEx: ', index);

            // content = this.contentPageBreak(index, array.length, content, content2);
            break;
          case this.node.users:
            content2 = this.usersReport(e);
            console.log('INDEx: ', index);

            // content = this.contentPageBreak(index, array.length, content, content2);
            break;
          case this.node.serviceCondition:
            content2 = this.conditionReport(e);
            console.log('INDEx: ', index);

            // content = this.contentPageBreak(index, array.length, content, content2);
            break;
          case this.node.serviceReference:
            content2 = this.referenceReport(e);
            console.log('INDEx: ', index);

            // content = this.contentPageBreak(index, array.length, content, content2);
            break;
          case this.node.serviceBenefit:
            content2 = this.benefitReport(e);
            console.log('INDEx: ', index);

            // content = this.contentPageBreak(index, array.length, content, content2);
            break;
          case this.node.serviceVisit:
            content2 = this.visitReport(e);
            console.log('INDEx: ', index);

            // content = this.contentPageBreak(index, array.length, content, content2);
            break;
          case this.node.serviceMilk:
            content2 = this.milkReport(e);
            console.log('INDEx: ', index);

            // content = this.contentPageBreak(index, array.length, content, content2);
            break;
          case this.node.serviceRoute:
            content2 = this.routeReport(e);
            console.log('INDEx: ', index);

            // content = this.contentPageBreak(index, array.length, content, content2);
            break;
          case this.node.servicePendency:
            content2 = this.pendencyReport(e);
            console.log('INDEx: ', index);

            // content = this.contentPageBreak(index, array.length, content, content2);
            break;
          case this.node.serviceInterview:
            content2 = this.interviewReport(e);
            console.log('INDEx: ', index);

            // content = this.contentPageBreak(index, array.length, content, content2);
            break;
          case this.node.serviceAcessuas:
            content2 = this.acessuasReport(e);
            console.log('INDEx: ', index);

            // content = this.contentPageBreak(index, array.length, content, content2);
            break;
          case this.node.serviceOld:
            content2 = this.oldReport(e);
            console.log('INDEx: ', index);

            // content = this.contentPageBreak(index, array.length, content, content2);
            break;
          case this.node.serviceAttendance:
            content2 = this.attendanceReport(e);
            console.log('INDEx: ', index);

            // content = this.contentPageBreak(index, array.length, content, content2);
            break;
          case this.node.serviceAepeti:
            content2 = this.aepetiReport(e);
            console.log('INDEx: ', index);

            // content = this.contentPageBreak(index, array.length, content, content2);
            break;
          case this.node.serviceFsp:
            content2 = this.fspReport(e);
            console.log('INDEx: ', index);

            // content = this.contentPageBreak(index, array.length, content, content2);
            break;
          case this.node.serviceHc:
            content2 = this.hcReport(e);
            console.log('INDEx: ', index);

            // content = this.contentPageBreak(index, array.length, content, content2);
            break;
          case this.node.serviceComplaint:
            content2 = this.complaintReport(e);
            console.log('INDEx: ', index);

            // content = this.contentPageBreak(index, array.length, content, content2);
            break;
          case this.node.serviceNotification:
            content2 = this.notificationReport(e);
            console.log('INDEx: ', index);

            // content = this.contentPageBreak(index, array.length, content, content2);
            break;
          default:
            break;
        }
        content = this.contentPageBreak(index, array.length, content, content2);
      }
      console.log('LOG test: ', e, content);
    });
    this.printPdf(content, city, 'landscape');

  }


  private createHeader(city) {
    console.log('city', city);

    return {
      margin: [20, 20, 20, 10],
      columns: [
        {
          image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAACPCAYAAAC/BZRKAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH4wkSAB4pPfeZWgAAFmFJREFUeNrtnXmYXFWVwH/3VfWWpNOdPSERkpCALFEDAWSLhDgGZIuyDYjjwAwug6MG5wMGHBAUUZQwKqICIohDxGFQBEIEE2IggrKIEcKSBEKALIROp5PupLd6d/4473XfevVeVfVWHbrP7/vqS+fVfWvdc8967wNFURRFURRFURRFURRFURRFURRFURRFURRFURRFURRFURRFURRFURRFURRFURRFURRFURRFURRFURRFURRFURRFURRFURRFURRFURRFURRFURRFURRFURRFURRFURRFURRFURRFUZQ9F9PfF5DE1ddc2+19r7ryiv6+fGWAsEcJSB6hGAKMCD5DgTTQBjQC24DtQHPcjiosSk/YIwQkQTDeBxwBHAN8IPh/LVABeEAGEYp6YAPwV+AJ4Glgc/RgKihKd+h3AYkIhwccCnwKOBGYimiLYmkF1gAPAIuAvwM2/FKFROkq/SYgMVpjf+DfgbOB0b1wik3A/wA/AtaHG1VIlK7QLwISEY4y4FzgCmB6H5zuBeBq4D7ADzeqoCjFUHIBiQhHLfBfwL8BlQV2bbHQgDWNFtoMlGNstYEaRMjy0QjcAHwXaAo39kRI6iZNK6rdqLfWdmm/aPuBwtELlhRss/LGE/r7MnPoin3fYyLCMRq4ETgvzy4NwNPG2D8Or2h9aZ+ahh0TqhttRbrd7Gors2/vqPbe2F5Ts7O1/EADH/GMPQwYFnOcYYggjgH+E9jR1Wsv0LFTyGBjkeBBzn6j3lrrHiMFHAbMQrTaX4DnAL9u0rQBJyQR4ZgBHIUMiH8D/oT4jnskJRUQhxpgIcnCsR2411pzxzH7bHh57vR1R2HN6VhzBDACawzGtgXtXgOe3tI47PpfrToova258syUsZ8AqiPH9IAvIB34UmD31ddcW5QWiRGOCcCHkOjaVGAkosUyiLbaCrwJPAqsjuybAv4juIYRwbZ3gW8AN+GYgQOQ84BvAxOD/zcCNwNXkRCm729KZmI52iMNXAtcktB0BXANsLw8lclcdtZ9nt00vhaYBJyAPOQZMfs1GGMf291WducPnjrMa24ru8QYe0RMu3ZEm3yHIMKVT0giwrEvcAEwP/i7osBtXxacx9UgBwLLgHGRtm8Cc4B1A1SDjAH+gAwqLo1IxPKJPdHE8kpxkohpdQbwxZhmGQM/3VVmzgGWApnJtQ3YTeN9JBm4CrgeeZiXAq9E9q+x1syvTLffdenslXNmT37jYt+aX+KEeQPSiHDOK3TdEeH4BBI+vhzp5IWEA2BnzLY08Zo7jWiXgUoKKE+470I+ZL9REgFxmIxEq4ZEtmc8y8JFMyoX3XZoVfWPDq8C4JyLbsCbvQRvdpYN+zbwE8Rudzt/K9ACDLW+98U5U9dff8Ehz9/uW3NLzHWMAK4kGMWLKGs5HbgVOKAL92oJfJ2IRngZ+BnZJsUu4BbEXByobEbMqQZnWzsSin+6vy8uiVL7IJ8DDo5u9Cw/v/sDlcvqq8wXyjP2y5Br9nizl+Cv6FDBX0ZMLddEfA6xb8uBkdaaaXvXNpx89owXb//1CweONtLJXY4EPoNopRwc7TEd+BYwqov32iEgEVoRm/vx4BosUgHwGNJhBjI3I5bAHMRJfxZYgphZRREXDetL06zPBcQZnQ9AMuRZGMtTK/cu+0V9lfm+sbwCvFPgkMOBk8n1n/ZCyk02hBusNbUHTNw4JP3iAVdkrDkAMY1czgd+BWzI47B/Gtgv4Vq2If7EKiR8XAWMB6YhPlNdwn7NwIPBJ4diQ8ghrobq6r6nnnlTUe3iOmExodsIGeCPwacgxR7fbdfbwlJKDXI6Uk/l0tScNgufm5D+VMoyE4n62ALHscRHegy5Nvx221Kxvd0ajGiKW8i2g98PnIpEj+KoAT6W8N1mRCM+RCS0i/gnI5E6MWK+q0EiV+F9pJCw93bETAyZiAwIcc/ERxz73Qmh4dGIY5z0PDcH5yNoNzqmrY8MOM1HL1iS1PkMYjoPj/muDngr+DsdnKeO7LDuKGA3YmYCsYJREZxjfzpr8tLBPnXAS8Cf6YMIYKkEZARwSnSjZ1l8z4yKtpTt0CwdHWrCwws72m068WJ3t51IVvwIsrXIQ0iHyfJZrr7m2rDRfYg2mBu5jPnAz3ESiA7jgL0T7ukR4HcJ37UgpS5x7Iv4MxuQEhgDTEE0zgV0Bh9SSDTvNOJNLx8pzfm/mO884OvAPybsa4CLEfsf4BwkspeJtGlHkrj3Rw/gdOJRwJ1IdMrdPw38EPia0+4uRBheCZ7RXohlcQkSvYwKRwUSlPkMcDgiYHEO/VKkf+2ml+lTAXHMq4PJNW9adpeZexvLzdme7UjuVSEdohZx3FaDCEtESH6MdKAzEI3wKKIhcjrDVVdewfE/vp+Z21/bWdPW9AuLOY5sTTMTGZmeizGzhgXXFEeW1uhCaDYdPIujItvryNWAteT3febhCIhjXo0DPlpgXzdQ8gydWixKGL1LGp1nIUnPaCVEE+JbhXjAPoj5ebKzvY34iOAoJDf0zyT/BiE+fZQ/KlUU63AiGW5jWf3YlLJ6Yzk+2GSBi4D/Be4AFuM41nWTplF/7lpIGxCn7ttIJ5iDJN62JJ387zWTueH9Z1SuHzLuGXIjRSORCuI42sk1n0Lm4YSK6yZN6/gUgV/ktkLm5pEEkbjIeWciJkk+3GM/jeQo4vhIeKwEn+DjxJcJPYUEIgrdjx9ud45fAXwTSewWEg4Qk+09KyAG+GDMiZ9aX5vaz8BYp90oOlXoPojaz0qo1Z+1hm3zX8UM9UBs6DqcBx8JCTPm7r/Jv7u3feDhCYfu71k/LqT4QeKpI96PADEP7kAKIffHMfe6ICg9Zd+Eaz+e4vI0IW3AvcSbY/sgg1AHTkceH5wrjgeIN1uLYR5iVhVLO30kIKXwQYYQM5plDC9kDB9K5R8jJyOOaqgdjgOGmyHe77ad9mpO4wJmztEtXtlEa8wqpHrYZSpiqkVrgrYg4eOk3j4eyaecDzwM/Bp4ksDhdOuw+oiq4Jk84myrBY7txrEeR3yDgyLbDWLf3xXzfI4gvgJ7M8kaqRAeYjnEaY4WxO97HBGKkUiE8SXewwIylFxbuLWx3LxjxDkluLmdSHTH5d3gE7I3ksl+FXgDmTtSD9w/sn5rh7aIoQbxV95t8cqeL/fbfbK15xjkB4l2gHakY5wU3EcS7wM+i4SxVyA+0hJkZKaPCxBnIxGkMOdyIBKdK4qVN54QaoRNiKAdFNPsyOCYqxztYRDzKi47/iQycc09frEMT7gGEME4n2zNFEYvC5mj3aIUJlYFuaNBa1O5aUZGO5CH+eXg35AmJPy6IeN1WC/3IqPF94C7kcrcSoBjr7krbDMSOAQZ2UKn9xTgwwaGN6fKdxF0XIchxP/QAL9HojHFjFBDkajLIqRSeUwJnu9BiIkXEgpMd3iQeLNoLLnh7omI9opig+N0t0K3ktxC05BapFA0er4+S7CWygfxIhv8jMHS2YHvQEKFZyAmy/XAmchsQLbMW+A+oCZkRK9GQrT3tKXSvDJhHxCH8gEkebccyYBPRUKVHpDy8TqcwshzyLpGZ8RvQxzGr5Oc+IsyFAk4/CC45r70SWrpNKmqiPgLXeRZ4PmE704iO9ByFPJso2ygyERgAm1k54JcZiEh568hvleWBdQXGfVSCEgbkdHEQllVu00jGeWXEDMGJCP9DaQY8eFgX7ZJ5zoO6fwTkZxGmiDrfvC3fw3izH8P+eFqECd6AZLjCKNUTeV+Wzm54dQWcrWKSxMiJPOBXyLl7MVwNvAvJXjGc4LnMZXkgEMiTsdqQPJJcRziHNtDzKs4E30F8HrkuF1hB7Auz/cHIn3kEWRgPZYgQNKNzH5BSiEgu8mtSaqobrE1FjYC9yAFiLG8cOHN4Z8ppHOegSTIqgk6n+f7IAmnqO1ahpgc5QAWNlZlWt1IWUgDMfMRIn5DWDN1PhJluQoZcfMJlkGSk31tas1E/KDD6IwKdpclZPt9IcOBfwj+noSsNhOlHTGveuIwtyEDYCGzaSzi8/0G+CrBb9rbQlIKJ70JEQQ31+CVZ5gGrESqcoGcjLnQaZosDT4hlxHYqsFUvtC3SIybe9auTtvM9IzJGRfeImHCTigkjonUjtR8/RUxAechmmpWwmn3RXyEYrVOdxiHjO6z6OYcH8eZXo3kME6OaTYHyT/NQsK/UdYiv2lPuQ8Z2P61iLajEO2+Dbi9F86dRSkEpA0xo7JKTazhiLFN/qXvDPPeTvnxwrFjweWxB2x/6EHsrl2Phv/3jQFZnOEBYgoiw8ONaWl42WJOi/luNQVGvVBQDPBup7DUIcGCPyNlG3ETtMICxt4i9J9cQUgDHyY++hON2BWiJXiOJ5ErbAciwY9jie87Swmsge6YV46QNiHJ3/WIkEwusGsF4mf+jnjt121KVYv1NDLydpzPwiFnvtjiDWu1W796Zm7QxRGONPKDHIOYaovTJ528xjY20r74ITAdv+Gu4KG+DhyN+CfDkagSvjHPzn3neesbE53R1oKYSkVhidUq65AKgDgByQlS9JB6JCQeHcGPIbcY1EdG9a6uL7YMqWuL1qGNRGrZDo/Zp5lk/6U7NCAzT+9BfsOTkEGgJqH9fojw9qqAlKrU5BmCQkKH0cAZDRWGb/52Z75JS58HfosU7v03Uns00wwbRtlZZ0f9hM1I9v3jSAFemBjxPWvvGde8fS65D/i1sF203N0tH3E/5aZLVkwLvfuj7STejDkECUxE2z5R8IgBzqj/GhIFjOIhSda4PMtqHHO5u6y88YSo9lmLhNnnI3VhzyXsWknnHP9eo08FxOlwG4h/4OekLO/PGLhycwM7FlweNasmI/a9q2JmIOq0o5duPTcncNOMDPa1AL4xK07Z+Je11nhnxVzDUpIrb0FG3rE4eZJNE/eNhm0nkzshK2QLvTtT0CKTq6KTjKrI1RKvIKtLdvV39hEzKy4AMQvRJFEeIQiD9yTcevSCJUmOdnNw379P2DVcirZXKZWJ5SOq8iyyM9KTgYs9uAhsJmjnCslY4iNA05ARo6O82RWSIKOeCvZtGNbevHBK0+bzfONNihxnB2Ia5WMSEoauR0avNUin3x1cw8HI7MYkJ305udqzJ3iIxnsV0Rr5eBzRXt1x3J8IzhFXehJlJ1Jc2lsMQxKTmxCfJpwrMgMxteLYRufck16jlBOmViCj9amR7ee1w8qW8vSzSKj278hI1IwUI24iN7O6moTRwik3qQHG+8ZcfcHrj07yjXdOTPMliIOdj6GIczoSCTRYxGzKIEKYb8G7rcjcjwy9h4d0+ifILyCtwfOu7crBHUd5MzKN4KAidnueTnO2NxgHfD+49ncRbekhObAkH+RJnCVme4s+90EcM2s3klneHmlS5cG3bq4dOj0lIdHFdArTbTEP5E+ITZpYe2MBi9k/Y7xFn1u35C0kCx6tbq1DSllaItcZpYrsxKJBhGIo+YWjDVny50998Fjbg+eTLwfzOhJ86MlvnFR6EuVhglxXL2Wzy4PPMMTKCOcTJQlHPbKQR68vQFfqRRseQ2LV0ZjuXjs8s/AHtUO++sXtu74DXGnjHcEM8CJOKYLrs9w4YijpNfezM13FbVM+tu7z65ZMrfDbFhKfPLuF4hzYKrq+LM0ORDh+GG6IrKzYU1JI4GM9yesZP4lE8noiIM8g1Q1H5mlTR3Y1cW9QTvF9swmZcrC0yPZdoiRRLGd09pH1cXNqdQxMbjXmJzeMGDpkS8r7dEoK/qKOaAq4EDGNLkFGFwNQnfHxgXaToirTWvultQ9cUOG3/RDxIaL8ASkmLLhwHKIpip1b0Ypov3AFwVbIycgbktfFitr3cetkpZDfbSP5zcOw3NwQ7zck/vaR0pNCvsUzyKBVjPaIu5+4+66k8DO3iPBeiFgCtshr6BL9sfToZqQ04G5yVwsZY+C7dw2vWlzt21vP3bl7UY1vT/ThOCtx/3Ca6H7ICH2hgYdSlgeob1reNmZ4eYW1c4CvWMxc4jvB6uD8xWa2tyACOQXxQ8JoUThnuxHprM8hpsZyHDMypsy9ASm4c/0qE+zjluRYRAuk6PRhDGKTh07rInLXIg6PFc7mexMpx4h2wteLvP/fIH5InDnpIQGMYqJHzcFznEi2edxG7ko2TUjnn0znG8X84BjvIr/hI4gJ2FGm1BfFiiVd3T2S6zgecWCnJjTf6cPSFDx4cEv7+kNa2oaMyPgTUjDaQIWFlgxs257ytjxbUdb4YkV6ii+x8tkkz91Yg9RvdUwFLXLZ0XAlklok5FyJdI7wDVdbyfWtklZ2DxOH0Wcfrtbidp6cKmPAWvCNtMt3rExQghO32guI0Nl881SccGuK5L6SocDoXcRxMoB1AgRe8LxHI7mNSmQw2oEISB0R/6uv1sYqqQa56sorXCFZhhT+3UT8WrvVHsy3cOqqivTmv1Wk11gZ9bYiPkgtMN3AVCP/jie/wP8VKUF/0r2eImlBRrlCa3YBBWcQ5qwAn4fYxQhMkceynf/0dL5EwestsoN25b7rSZ7u3NXzdps94QU6BwDXISHUvvCJMoiZcDnZE7L0JTpKQUq9Ni+Q0zFfAv6Jzjqq3mQt8BVk6Zg1SMXrXj04njLI6BcBgRwh2YFElU5EnO91dH+OsUUE47rgeOE7Nz6LlE9cxsBeRV3pRfr9LbcQu7r6FKTUYB7yopoJ5E/KNSMZ9+eRSNKjSI4ghVQCfwUpYCxD7NpTCAr+1MxS8rFHCAgkvoKgDDGJpiNZ9kmIc16GRDG2I2G+tYgJtTHYnkZetvNpRMjcDGw7Mt/9t6ACouRnjxEQlyLe11GIamRW2kcTvv8lUkbfBCokSjJ7pIDEkU9o3A7utDsdEYQ40yyDrJ5yHcVl05VByntGQIrFEZBK4EtI7iNuhfYGJA/zm3CDCokSZcBFc5Y/tow5c+aC+BorEef9ZHKTopXIaiArCJY2nTNnLssfW9bft6DsQfRbmLeEPEj8bEaQmq7riV/2X1EGnonl4phbn0RWHUkKFd+ILFbXBmpqKZ0MBg0CUvn5eJ7vv4Cz3H4vRNGUAcKA80FcHH8kfEX0aSTPSZiFzMB7A9QfUYTBokFAVsN4Ms/3ewE34LwLRDWJMuAFxPEnGpDFjvOVXB+K1ILV9vd1K3sGA15AIiym8Oobn0Qc9hSoFhnsDGgfJMTxRZqQabMfLbDLTKS+60VQf2QwM9g0CMh88LcLtBmKLBW0X8GjKQOaQaFBIEuLbENmMRZalXA0skDDEsBXLTI4GYwaxCd+SaE4zkZe66YMUgajgICEe1cU0a4amVMyaDStks2gEhAn5LsLWeGxpYjdjiL3zarKIGFQCUiE3yNLDxViLCogg5ZBJyCOFmkEFhKz4FsMA7qoU0lm0AlIhGXAzQXabKQP3juhvDcYlAISWUz7emSl97gSFB+4ExESZRDSH4tX72k0IK9jWIWs2zsNeS6bgZ/hvMJAGXwMats6ps5qNLK8UCVS9r4+/OK2W3/Km29u6O9LVkrMoBaQkGIKEnWWoaIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoSu/y/0cNSpmSROaCAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE5LTA5LTE4VDAwOjMwOjQxKzAwOjAwRTu3kQAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxOS0wOS0xOFQwMDozMDo0MSswMDowMDRmDy0AAAAZdEVYdFNvZnR3YXJlAEFkb2JlIEltYWdlUmVhZHlxyWU8AAAAAElFTkSuQmCC',
          fit: [112, 80],
        },
        [
          { text: city.orgName, alignment: 'right', style: 'header' },
          { text: city.address.city + '-' + city.address.state, alignment: 'right', style: 'subheader' },
          { text: moment().format('LL'), alignment: 'right', style: 'subheader' }
        ]
      ]
    }
  }

  private createFooter() {
    return (currentPage, pageCount) => {
      return {
        margin: [20, 20, 20, 10],
        columns: [
          { text: '© 2019 SIMVIS. All Rights Reserved. www.simvis.com.br', alignment: 'left', style: 'footer' },
          { text: 'página ' + currentPage.toString(), alignment: 'right', style: 'footer' }
        ]
      }
    }

    // return {
    //   margin: [20, 20, 20, 10],
    //   columns: [
    //     { text: '© 2019 SIMVIS. All Rights Reserved. www.simvis.com.br', alignment: 'left', style: 'footer' },
    //     { text: 'página', alignment: 'right', style: 'footer' }
    //   ]
    // }
  }

  private createTable(data = [], header = [], width = [], layout = 'lightHorizontalLines'): any {
    //create body
    let tableBody = []
    //set header
    let tableHeader = []
    for (let i in header) {
      tableHeader.push({ text: header[i], style: 'tableHeader' })
    }
    //set header in doby
    tableBody.push(tableHeader)

    let tableContent = []

    data.forEach((e, index) => {
      let line = [];
      e.forEach(i => {
        let fc = (index % 2) === 0 ? '#e3e3e3' : null;
        line.push({ text: i, fillColor: fc, fillOpacity: 1 });
      });

      tableContent.push(line);
    });

    //fillColor: 'yellow'
    // fillColor: (rowIndex) => {
    //   return (rowIndex % 2 === 0) ? 'red' : 'yellow';
    // }

    // tableBody = tableBody.concat(data);
    tableBody = tableBody.concat(tableContent);

    let table = {
      style: 'table',
      table: {
        headerRows: 1,
        widths: width,
        body: tableBody
      },
      layout: layout
    }

    return table;
  }

  // private printPdf(content, city, orientation = 'portrait') {

  //   let docDefinition = {
  //     header: this.createHeader(city) as Content,
  //     footer: this.createFooter() as Content,
  //     content: content as Content,
  //     styles: style as StyleDictionary,
  //     pageSize: 'A4' as PageSize,
  //     pageMargins: [20, 90, 20, 50] as Margins,
  //     pageOrientation: orientation as PageOrientation
  //   }

  //   createPdf(docDefinition as TDocumentDefinitions, null, null, pdfFonts.pdfMake.vfs).open();
  // }

  private printPdf(content, city, orientation = 'portrait') {
    let docDefinition = {
      header: this.createHeader(city),
      footer: this.createFooter(),
      content: content,
      styles: style,
      pageSize: 'A4',
      pageMargins: [20, 90, 20, 50],
      pageOrientation: orientation
    }
    let df = docDefinition;
    this.pdfMake.createPdf(docDefinition).open();
  }

  //Calc age
  calcAge(s: string): string {
    const date = new Date(s);
    var timeDiff = Math.abs(Date.now() - date.getTime());
    let age = Math.floor((timeDiff / (1000 * 3600 * 24)) / 365);
    return String(age);
  }

  formatCpf(s: string): string {
    let doc = s ? s.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4") : '';
    return doc;
  }


  formatPhone(s: string): string {
    let doc = s ? s.replace(/(\d{2})(\d{5})(\d{4})/, "($1) $2-$3") : '';
    return doc;
  }

  formatNis(s: string): string {
    let doc = s ? s.replace(/(\d{3})(\d{5})(\d{2})(\d{1})/, "$1.$2.$3-$4") : '';
    return doc;
  }

  formatDate(s: string): string {
    return moment(s).format("DD/MM/YYYY");
  }

  tsToDate(ts: any): Date {
    let time = new firebase.firestore.Timestamp(ts._seconds, ts._nanoseconds);
    return time.toDate();
  }
}


