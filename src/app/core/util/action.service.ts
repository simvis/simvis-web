import { Injectable, TemplateRef } from '@angular/core';
import { TextService } from '../static/text.service';
import { ArrayService } from '../static/array.service';
import { Person } from '../../model/person';
import { StringService } from '../static/string.service';
import { IO } from '../../model/io';
import * as _ from "lodash";
import * as firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/storage';
import { CrudService } from '../database/crud.service';
import { Data } from '../../model/data';
import { Group } from '../../model/group';
import { AuthService } from '../auth.service';
import { BsModalService, BsModalRef, AlertComponent } from '../../../../node_modules/ngx-bootstrap';
import { Service } from '../../model/service';
import { NodeService } from '../static/node.service';
import { Coords } from '../../model/coords';
import { Note } from '../../model/note';
import { Address } from '../../model/address';
import { Hits } from '../../model/hits';
import { Elastic } from '../../model/elastic';
import { Router, ActivatedRoute } from '@angular/router';
import { District } from '../../model/district';
import { User } from '../../model/user';
import { Subscription } from 'rxjs';
import { Location } from '@angular/common';
import { ValueService } from '../static/value.service';
import { PrintService } from './print.service';
import { Core } from '../../model/core';
import { Roles } from '../../model/roles';

@Injectable({
  providedIn: 'root'
})

export class ActionService {
  //Array to services --------------- START
  hcList: Service[];
  schedulesList: Service[];
  fspList: Service[];
  groupsList: Service[];
  routeList: Service[];
  benefitList: Service[];
  referencesList: Service[];
  visitList: Service[];
  oldServiceList: Service[];
  notificationList: Service[];
  termList: Service[];
  demandList: Service[];
  acessuasList: Service[];
  attendanceList: Service[];
  aepetiList: Service[];
  conditionList: Service[];
  complaintList: Service[];
  pendencyList: Service[];
  interviewList: Service[];
  fpgbList: Service[];
  milkList: Service[];
  scfvList: Service[];

  // uidList: string [];

  //Array to services --------------- END
  unsubList: any[];
  //show boolean -------------------- START
  searchDiv: boolean
  infoCE: boolean
  infoCS: boolean
  peopleR1: boolean
  visitPlan: boolean
  //show boolean -------------------- END
  districtsSchedule: Data[];
  districtsVisit: Data[];
  nestedList: any;
  listNoteHc: any[];
  listNoteFsp: any[];
  list: any;
  filterList: any[];
  nestedList2: any[];
  listUser: User[];
  listSearch: any[];
  listSearch2: any[];
  districts: any;
  cores: Core[];
  listQueryVisit: any;
  listQuerySchedule: any;
  openMenuInt: number
  notes: Note[]
  address: any;
  dialogRef: BsModalRef;
  modalRef: BsModalRef;
  modalPrint: BsModalRef;
  onAction: string;
  familyUid: string;
  oldFamilyId: string;
  people: Person[];
  members: any;
  users: User[];
  pages: any[];
  meeting: any;
  time: Date;
  sortPeople: Person[];
  progress: number;
  maxDate: Date
  isRequired: boolean;
  //onAlert: boolean
  groups: Group[];
  doc: any;
  doc2: any;
  docPrint: any;
  $doc: any;
  $_doc: any;
  _doc: any;
  _arr: any;
  isOk: boolean;
  valid: boolean[];
  hide: boolean[];
  personOk: boolean;
  nisOk: boolean;
  changeStatus: boolean;
  minDate: Date;
  nodeDoc: string;
  feedback: string;
  hits: Hits[];
  _hits: Hits[];
  query: string
  subscription: Subscription;
  modal: TemplateRef<any>
  action: string
  title: string
  model: string
  isNested: boolean;
  //hidden: string
  alerts: any[];
  dismissible = true;
  merge: boolean;
  daysElapsed: boolean;
  // id: string;
  validPerson: boolean;
  querySchedule: boolean;
  queryVisit: boolean;
  promises: any[];
  // text: any;
  loading: boolean;

  constructor(
    public text: TextService,
    private arr: ArrayService,
    private str: StringService,
    private crud: CrudService,
    public auth: AuthService,
    private val: ValueService,
    private modalService: BsModalService,
    private node: NodeService,
    private printService: PrintService,
    private router: Router,
    private location: Location) {
    this.alerts = [];
  }

  download() {
    //window.print();
    if (this.modalRef) this.modalRef.hide();
    this.router.navigate(['/sheet-attendance']);
  }

  //Setter ---------------------------------------- START
  //Setter _DOC ---------------------------------------- START

  setExtraDetail(s: string, check: boolean) {
    if (check) this._doc.extraDetail = this.text.inAnalyze;
  }

  checkNewDate(_doc: any): boolean {
    if (_doc.pickedDate
      && _doc.pickedDate.getTime() > new Date(this.$doc.pickedDate).getTime()) {
      return true;
    }
    return false;
  }

  setActive(checked: boolean): void {
    console.log('active: ', checked);

    this.doc.active = !checked;
  }

  setDeleted(checked: boolean): void {
    this._doc.deleted = checked;
  }

  setStatus(status: number, checked?: boolean): void {
    this._doc.status = status;

    if (!checked) { this._doc.status = this.$doc.status; }
    console.log(this._doc.status);

  }
  //Setter _DOC ---------------------------------------- END

  setList(list: any): void {
    this.list = list;
  }


  setNode(node: string) {
    this.nodeDoc = node;
  }

  setMinDate(date: Date) {
    this.minDate = date;
  }

  setMaxDate(date: Date) {
    this.maxDate = date
  }


  //Setter to Document ---------------------------------------- STRAT

  setCoords(coords: Coords) {
    this.doc.address.coords = coords
  }

  setPerson(person: Person) {
    this.doc.person = person
  }

  //Setter to Document ---------------------------------------- END

  //Setter ---------------------------------------- END

  //CRUD - User functions in CRUCD ---------------------------------- START
  // get static text to app
  // getTextService(): void {
  //   let uid = this.str.text;
  //   this.text = {};
  //   this.crud.getStaticString(uid)
  //     .get()
  //     .then((doc) => {
  //       if (doc.exists) {
  //         console.log('textService', 'TEXT HERE');
  //         let text = doc.data();
  //         sessionStorage.setItem(uid, JSON.stringify(text));
  //         this.text = text;
  //       }
  //     });
  // }

  getOldFamilyData(id: string): void {
    this.crud.getOldFamilyData(id);
  }

  // get city to page
  getCity(code: string, snapshot: boolean, print?: boolean): Promise<void> {
    let query: firebase.firestore.Query = this.crud.getCity(code);
    let promise: any;

    if (snapshot) {
      promise = query.onSnapshot((snapshot) => {
        // this.fspList = [];
        let doc = snapshot.docs[0].data();
        delete doc.writeAt;
        this.doc = doc;
        console.log(this.doc);

        // snapshot.forEach((doc) => {
        //   this.doc = doc.data;
        // });
      });
    } else {
      promise = query.get().then((snapshot) => {
        snapshot.forEach((doc) => {
          console.log("city--", doc.data());
          if (doc.exists) {
            let data = doc.data();
            if (print) {
              if (!this.docPrint) this.docPrint = {};
              this.docPrint.city = data;
            } else {
              delete data.writeAt;
              this.doc = data;
            }
          }
        });
      })
    }

    this.unsubList.push(promise);
    return promise;
  }

  // get users of city
  getUsers(code: string, subSector: number): Promise<any> {
    return this.crud.getUsers(code, subSector)
      .get()
      .then((snapshot) => {
        this.users = [];
        snapshot.forEach((doc) => {
          this.users.push(doc.data() as User);
          console.log(doc.data());

        });
      });
  }

  formatDate(date: Date): string {
    return date.getFullYear() + '/' + date.getMonth() + '/' + date.getDay();
  }

  setFilterReport(doc: any): any {

    if (doc.report.action == this.str.esAttendance && doc.filter[0] == 0) {
      doc.filter[0] = this.auth.user.uid;
    }

    return doc;
  }

  // get Report Data From Elasticsearch
  getReportData(doc: any): void {
    this.loading = true;
    let report = _.cloneDeep(doc.report);
    // let data = _.cloneDeep(doc);
    doc = { ...doc, ...report };
    doc.cityCode = this.auth.user.city.code;
    // console.log(doc);
    // console.log(JSON.stringify(doc.endDate));


    doc = this.setFilterReport(doc);

    this.crud.getReportData(doc)
      .subscribe((data: Elastic) => {
        if (data.hits.total == 0) {
          this.alertFeedback(this.text.noResultFoundToSearch);
        } else {
          this.getCity(this.auth.user.city.code, false, true).then(() => {
            let list = [];
            data.hits.hits.forEach(element => {
              list.push(element._source);
            });

            let listReport = [];

            if (list.length) {
              listReport.push(_.cloneDeep(list));
              this.printService.setContentReport(listReport, this.docPrint.city);
            } else {
              console.log('no data');
              this.alertFeedback(this.text.noSearchResult);
            }

            this.loading = false;
            console.log(data.hits.hits);
          });
        }
      });
  }

  getReportFamily(): void {

    this.getCity(this.auth.user.city.code, false, true).then(() => {
      let list = [];
      console.log("notes: ", this.notes);

      if (this.address.uid) list.push(_.cloneDeep([this.address]));
      if (this.people.length) list.push(_.cloneDeep(this.people));
      if (this.notes.length) list.push(_.cloneDeep(this.notes));
      if (this.groupsList.length) list.push(_.cloneDeep(this.groupsList));
      if (this.routeList.length) list.push(_.cloneDeep(this.routeList));
      if (this.benefitList.length) list.push(_.cloneDeep(this.benefitList));
      if (this.interviewList.length) list.push(_.cloneDeep(this.interviewList));
      if (this.milkList.length) list.push(_.cloneDeep(this.milkList));
      if (this.notificationList.length) list.push(_.cloneDeep(this.notificationList));
      if (this.fspList.length) list.push(_.cloneDeep(this.fspList));
      if (this.hcList.length) list.push(_.cloneDeep(this.hcList));
      if (this.referencesList.length) list.push(_.cloneDeep(this.referencesList));
      if (this.scfvList.length) list.push(_.cloneDeep(this.scfvList));
      if (this.conditionList.length) list.push(_.cloneDeep(this.conditionList));
      if (this.visitList.length) list.push(_.cloneDeep(this.visitList));
      this.printService.setContent(list, this.docPrint.city);

    });
  }

  // get filter list to a report
  getReportFilter(index: number): void {
    let report = this.list[index];
    this.doc.report = report;
    let type = report.text;
    this.doc.filter = [];
    this.doc.filter1 = {};
    this.doc.filter2 = {};
    this.doc.filter3 = {};
    this.doc.filter4 = {};

    let arr = this.arr;
    let all = { index: -1, text: this.text.defaultOption };

    console.log('type', report);
    // if (type == this.str.scfvReport) {
    //   this.listCores().then(() => {
    //     this.doc.filter1 = this.cores[0];
    //   });
    // }
    let str = this.str;
    switch (type) {
      case str.scfvReport: {
        this.listCores().then(() => {
          this.doc.filter[0] = this.cores[0];
        });
        break;
      }
      case str.searchReport: {
        this.doc.filter[0] = this.arr.searchReport[0].value;
        break;
      }
      case str.attendanceReport: {
        this.doc.filter[0] = this.arr.attendanceReport[0].index;
        this.arr.attendanceReport[1].index = this.auth.user.uid;
        break;
      }
      case str.hcReport: {
        if (!arr.targetHc.find(i => i.text == all.text)) arr.targetHc.unshift(all);
        if (!arr.statusHcFilter.find(i => i.text == all.text)) arr.statusHcFilter.unshift(all);
        if (!arr.rangeAgeHc.find(i => i.text == all.text)) arr.rangeAgeHc.unshift(all);
        this.listUser = [{ uid: '-1', fullName: this.text.defaultOption } as User]

        this.doc.filter[0] = arr.statusHcFilter[0].index;
        this.doc.filter[1] = arr.rangeAgeHc[0].index;
        this.doc.filter[2] = this.listUser[0].uid;
        this.doc.filter[3] = arr.targetHc[0].index;

        this.setListUser(undefined, true);
        break;
      }
      case str.acessuasReport: {
        arr.locationType.unshift(all);
        this.doc.filter[0] = arr.locationType[0].index;
        break;
      }
      case str.interviewReport: {
        arr.modalityType.unshift(all);
        this.doc.filter[0] = arr.modalityType[0].index;
        break;
      }
      case str.benefitReport: {
        arr.benefitType.unshift(all);
        this.doc.filter[0] = arr.benefitType[0].index;
        break;
      }
      default: {
        //statements;
        //this.doc.filter[0] = report;
        break;
      }
    }

    console.log(this.doc);

  }

  //Search users
  searchUsers(): void {

    let query = this.query;
    if (query) {
      //Elastic
      let code: string = this.auth.user.city.code;
      this.crud.searchUsers(query, code)
        .subscribe((data: Elastic) => {
          this.users = [];
          if (data.hits.total == 0) {
            this.alertFeedback(this.text.noResultFoundToSearch);
          } else {
            data.hits.hits.forEach((doc: any) => {

              let data = doc._source;
              delete data.writeAt;
              this.users.push(data as User);
            });
          }
        });
    } else {
      //Firebase
      let code = this.auth.user.city.code;
      this.unsubList.push(
        this.crud.listUser(code)
          .onSnapshot((snapshot) => {
            this.users = [];
            snapshot.forEach((doc) => {
              let data = doc.data();
              delete data.writeAt;
              this.users.push(data as User);
            });
            if (this.users.length == 0) {
              this.alertFeedback(this.text.noResultFoundToSearch);
            }
          }));
    }
  }

  //Search people
  searchPeople(query: string, hide?: boolean): void {

    if (query) {
      this.feedback = null;
      this.hits = [];

      this.crud.searchPeople(query)
        .subscribe((data: Elastic) => {
          if (data.hits.total.value == 0) {
            hide ? this.feedback = null : this.feedback = this.text.noResultFoundToSearch;
          } else {
            console.log(data);
            hide ? this._hits = data.hits.hits : this.hits = data.hits.hits;
          }
        });
        
    } else {
      this.feedback = this.text.typeSomething;
    }

  }

  //Writing a Document
  writeDoc(action: string, data?: any): void {
    //setUser that write data
    let user = {
      fullName: this.auth.user.fullName, uid: this.auth.user.uid,
      subSector: this.auth.user.subSector
    };
    if (data.uid || data.user) {
      data.updateBy = user;
    } else {
      data.user = user;
    }

    let doc = _.cloneDeep(data);
    // let doc = data;

    let str = this.str;
    let onAction = this.onAction;
    if (action) onAction = action;

    let file: File;
    let files: any = [];
    let alertCode: number;
    console.log('action', action);

    console.log('Acction', this.onAction);
    console.log('WriteDOC', doc);

    //Add fsp
    if (this.fspList && this.auth.canEdit([this.str.esFsp])) {
      //if (doc.fspUid) doc.fspUid = '';
      let _doc = this.fspList.find(i => i.status == 1
        && i.user.subSector == user.subSector);
      if (_doc && action != str.editPerson
        && action != str.editAddress)
        doc.fspUid = _doc.uid;
    }

    if (onAction == str.editServiceGroup
      || onAction == str.editServiceVisit
      || onAction == str.editServiceBenefit
      || onAction == str.editServiceReference
      || onAction == str.editServiceAttendance
      || onAction == str.editServiceSchedule
      || onAction == str.editServiceFsp
      || onAction == str.editServiceRoute
      || onAction == str.editServiceHc
      || onAction == str.editServiceOld
      || onAction == str.editServiceNotification
      || onAction == str.editServiceTerm
      || onAction == str.editServiceDemand
      || onAction == str.editServiceAcessuas
      || onAction == str.editServiceAepeti
      || onAction == str.editServiceCondition
      || onAction == str.editServiceComplaint
      || onAction == str.editServiceInterview
      || onAction == str.editServiceFpgb
      || onAction == str.editServiceMilk
      || onAction == str.editServiceScfv
      || onAction == str.editServicePendency
    ) {

      if (doc.file) {
        file = doc.file;
        delete doc.file;
        alertCode = new Date().getTime();
        this.alertProcessing(alertCode);
      }

      if (doc.person) {
        let person: any = {
          birthday: doc.person.birthday,
          uid: doc.person.uid,
          name: doc.person.name,
          kinship: doc.person.kinship,
          nis: doc.person.nis || '',
          cpf: doc.person.cpf || ''
        };
        doc.person = person;
        // console.log('person', doc);

      }

      this.crud.writeService(doc).then(() => {
        if (file && doc.uid) {
          this.uploadFiles(file, doc, alertCode);
        } else {
          this.alertFeedback(this.text.operationSuccess);
          console.log("SignAt - writeDoc", doc);
        }
      });
    }

    if (onAction == str.editCity) {
      if (doc.prefLogo || doc.orgLogo) {

        alertCode = new Date().getTime();
        this.alertProcessing(alertCode);
        if (doc.orgLogo) {
          console.log('orgLogo');

          delete doc.fileUrlOrg;
          files.push({ prop: 'fileUrlOrg', file: doc.orgLogo });
        }
        if (doc.prefLogo) {
          console.log('preLogo')
          delete doc.fileUrlPref;
          files.push({ prop: 'fileUrlPref', file: doc.prefLogo });
        }
        delete doc.prefLogo;
        delete doc.orgLogo;
      }

      if (files.length) {
        this.uploadFiles2(files, doc, str.editCity).then(() => {
          this.onAlertClosed(this.alerts.find(alert => alert.code == alertCode));;
          this.crud.writeCity(doc, this.auth.user.city.code).then(() => {

            this.alertFeedback(this.text.operationSuccess);
          });
        });
      } else {
        console.log("WRITED");

        this.crud.writeCity(doc, this.auth.user.city.code).then(() => {
          this.alertFeedback(this.text.operationSuccess);
          this.doc = doc;

        });
      }

    }

    if (onAction == this.str.esRouteAnswer) {
      this.crud.writeRouteAnswer(doc).then(() => {
        this.alertFeedback(this.text.operationSuccess);
      });
    }

    if (onAction == str.registerCoreMeet) {
      this.crud.writeCoreMeet(doc).then(() => {
        this.alertFeedback(this.text.operationSuccess);
      });
    }


    if (onAction == str.registerGroupMeet) {
      this.crud.writeGroupMeet(doc).then(() => {
        this.alertFeedback(this.text.operationSuccess);
      });
    }

    if (onAction == str.editLocation) {
      this.crud.updateAddressCoords(doc.address).then(() => {
        this.alertFeedback(this.text.operationSuccess);
      });
    }

    if (onAction == str.editAddress) {
      this.crud.writeAddress(doc).then(() => {
        this.alertFeedback(this.text.operationSuccess);
      });
    }

    if (onAction == str.editNote) {
      this.crud.writeNote(doc).then(() => {
        this.alertFeedback(this.text.operationSuccess);
      });
    }

    if (onAction == str.editPerson) {
      this.crud.writePerson(doc).then(() => {

        if (doc.deleted) {
          let note: any = {
            node: this.node.familyNotes,
            familyUid: doc.familyUid,
            city: this.auth.user.city,
            log: true,
            text: doc.name + ' - ' + this.text.deleted + ' => ' + this.text.reason + ': ' + doc.reason,
            user: user
          };
          this.crud.writeNote(note).then(() => { });
        }
        this.alertFeedback(this.text.operationSuccess);
      });
    }

    if (onAction == str.editFamily) {
      this.crud.writeFamily(doc).then(() => {
        this.alertFeedback(this.text.operationSuccess);
        this.initDoc(null, str.editFamily);
        this.routerToFamily(doc.familyUid);
      });
    }

    if (onAction == str.editDistrict) {
      this.crud.writeDistrict(doc).then(() => {
        this.alertFeedback(this.text.operationSuccess);
        this.initDoc(null);
      });
    }

    if (onAction == str.esFspNote || onAction == str.esFspNotes) {
      // this.deleteUserData(doc);
      doc.node = this.node.serviceFsp + '-' + this.node.notes;

      this.crud.writeNoteFsp(doc).then(() => {
        this.alertFeedback(this.text.operationSuccess);
        this.initDoc(this.doc, str.esFspNote);
      });
      let service = { uid: doc.parentUid, node: this.node.serviceFsp, familyUid: doc.familyUid } as Service;
      console.log(service);

      this.crud.writeService(service).then(() => {

      });
    }

    if (onAction == str.esHcNote || action == str.esHcNotes) {
      // this.deleteUserData(doc);
      doc.node = this.node.serviceHc + '-' + this.node.notes;
      this.crud.writeNoteHc(doc).then(() => {
        this.alertFeedback(this.text.operationSuccess);
        this.initDoc(this.doc, str.esHcNote);
      });

      let service = { uid: doc.parentUid, familyUid: doc.familyUid, node: this.node.serviceHc } as Service;
      if (action == str.esHcNote) {
        if (doc.frequency == 0) service.monthUpdate = new Date();
        if (doc.frequency == 1) service.quartUpdate = new Date();
      }
      console.log(service);


      this.crud.writeService(service).then(() => {

      });

    }

    if (onAction == str.esNoteFinds) {
      console.log(doc);
      doc.node = this.node.serviceVisit + '-' + this.node.notes;
      this.crud.writeNoteFindsVisit(doc).then(() => {
        this.alertFeedback(this.text.operationSuccess);
        this.initDoc(doc, str.esNoteFinds);
      });
    }

    if (onAction == str.editGroup) {
      this.crud.writeGroup(doc).then(() => {
        this.alertFeedback(this.text.operationSuccess);
      });
    }

    if (onAction == str.newCore) {
      this.crud.writeCore(doc).then(() => {
        this.alertFeedback(this.text.operationSuccess);
      });
    }

    if (onAction == str.editUser) {
      if (doc.password) {
        //create a new user account
        alertCode = new Date().getTime();
        this.alertProcessing(alertCode);

        this.auth.createUser(doc).subscribe((result: any) => {
          if (result.message == 'success') {
            delete doc.password;
            doc.uid = result.uid
            doc.authUid = result.uid;
            this.crud.writeUser(doc).then(() => {
              this.onAlertClosed(this.alerts.find(alert => alert.code == alertCode));
              this.alertFeedback(this.text.operationSuccess);
            });
            console.log("createUser", result.message);
          }
        });
      } else {
        this.crud.writeUser(doc).then(() => {
          this.alertFeedback(this.text.operationSuccess);
        });
      }
    }
  }

  //get list of all meeting
  listMeeting(doc: any, node: string, prop: string): void {
    this.unsubList.push(
      this.crud.listMeeting(doc, node, prop)
        .onSnapshot((snapshot) => {
          this.meeting = [];
          snapshot.forEach((doc) => {
            this.meeting.push(doc.data() as Group);
          });
        }));
  }

  //get list of all meeting report
  listMeetingReport(doc: any, node: string, prop: string): Promise<void> {

    return this.crud.listMeeting(doc, node, prop)
      .get()
      .then((snapshot) => {
        this.meeting = [];
        snapshot.forEach((doc) => {
          console.log("listMeetingReport", doc.data());

          this.meeting.push(doc.data() as Group);
        });
      });
  }

  //Get list cores to a specific city
  listCoresToCity(limit?: number): void {
    this.cores = [];
    this.unsubList.push(
      this.crud.listCores(limit)
        .onSnapshot((snapshot) => {
          this.cores = [];
          snapshot.forEach((doc) => {
            this.cores.push(doc.data() as Core);

          });
        }));
  }

  //Get list cores to a specific city
  listCores(isPromise?: boolean): Promise<void> {
    this.cores = [];
    return this.crud.listCores()
      .get()
      .then((snapshot) => {
        snapshot.forEach((doc) => {
          this.cores.push(doc.data() as Core);

        });
      });
  }

  // set address to this.people
  setAddresToPeople(): Promise<any> {
    let promises = [];
    this.people.forEach((p, index) => {
      console.log(p);

      promises.push(this.getAddress(p.familyUid).then(() => {
        console.log(this.address);
        this.people[index].address = this.address;
      }));
    });
    return Promise.all(promises);
  }

  //get listPeople by list of uid in key\value object
  listPeople(members: any): Promise<any> {
    let promises = [];
    Object.keys(members).forEach((uid, index) => {
      //Remove this for
      //for (let i = 0; i < 90; i++) {
      //  if (members[uid] && index == 1) promises.push(this.getPersonList(uid));
      //}
      if (members[uid]) promises.push(this.getPersonList(uid));
      //      console.log(uid);

    });
    return Promise.all(promises);
  }

  //Get a list of people to one service group or core
  listPeopleData(uid: string, node: string): Promise<void> {
    this.members = {};
    return this.crud.getPeopleGroups(uid, node).get()
      .then((doc) => {
        if (doc.exists) {
          this.members = doc.data();
        }
      });
  }

  //Get list group to a specific city
  listGroup(): Promise<void> {
    this.groups = [];
    return this.crud.listGroup()
      .get()
      .then((snapshot) => {
        snapshot.forEach((doc) => {
          this.groups.push(doc.data() as Group);
        });

        if (this.groups.length) {
          let data = this.groups[0];
          let doc = this.doc;
          doc.name = data.name;
        }
      });
  }

  //Get list of notes to a specific FSP
  listNotesFsp(doc: any, limit?: number): void {
    this.listNoteFsp = [];
    this.unsubList.push(
      this.crud.listNotesFsp(doc, limit).onSnapshot((snapshot) => {
        this.listNoteFsp = [];
        snapshot.forEach((doc) => {
          console.log('list....');

          this.listNoteFsp.push(doc.data() as Note);
        });
        if (this.list && (this.action == this.str.esFspNotes)) this.list = this.listNoteFsp;
      }));
  }

  //Get list of notes to a specific HC
  listNotesHc(doc: any, limit?: number): void {
    this.listNoteHc = [];
    this.unsubList.push(
      this.crud.listNotesHc(doc, limit).onSnapshot((snapshot) => {
        this.listNoteHc = [];
        snapshot.forEach((doc) => {
          this.listNoteHc.push(doc.data() as Note);
        });
        if (this.list && (this.action == this.str.esHcNotes)) this.list = this.listNoteHc;
      }));
  }

  //Get list of notes to a specific Visit
  listNotesFindsVisit(doc: any): void {
    this.unsubList.push(
      this.crud.listNotesFindsVisit(doc).onSnapshot((snapshot) => {
        this.nestedList = [];
        snapshot.forEach((doc) => {
          console.log("VISITI NOTEs...");

          this.nestedList.push(doc.data() as Data);
        });
      }));
  }

  //Upload files to firebase Storate
  uploadFiles2(files: any, doc: any, action: string, alertCode?: number): Promise<any> {
    const promises = [];

    files.forEach(item => {
      const upload = this.crud.uploadFile(item.file, action, item.prop);
      promises.push(upload);
      upload.on(firebase.storage.TaskEvent.STATE_CHANGED,
        (snapshot) => {
          console.log('Snapshot', snapshot);
          const snap: any = snapshot;
          this.progress = (snap.bytesTransferred / snap.totalBytes) * 100;
          console.log("Progress", this.progress);
        },
        (error) => {
          console.log('ERROR', error)
        },
        () => {
          upload.snapshot.ref.getDownloadURL().then((downloadURL) => {
            doc[item.prop] = downloadURL;
          });
        });
    });

    // try understand other day
    let waitUrl = new Promise((resolve, reject) => {
      (function waitForUrl() {
        if (doc.fileUrlPref && doc.fileUrlOrg) return resolve();
        setTimeout(waitForUrl, 30);
      })();
    });
    promises.push(waitUrl);
    return Promise.all(promises);
  }

  //Upload files to firebase Storate
  uploadFiles(file: File, doc, alertCode): any {

    //const promises = [];
    const upload = this.crud.uploadFile(file, this.onAction);
    //promises.push(upload);

    upload.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot) => {
        console.log('Snapshot', snapshot);
        const snap: any = snapshot;
        this.progress = (snap.bytesTransferred / snap.totalBytes) * 100;
        console.log("Progress", this.progress);
      },
      (error) => {
        console.log('ERROR', error)

      },
      () => {
        //return upload.snapshot.ref.getDownloadURL();
        upload.snapshot.ref.getDownloadURL().then((downloadURL) => {
          this.onAlertClosed(this.alerts.find(alert => alert.code == alertCode));
          doc.fileUrl = downloadURL;
          this.crud.writeService(doc);
          this.alertFeedback(this.text.operationSuccess);
        });
        // this.doc.fileUrl = upload.snapshot.downloadURL;
        // //write again
      });
    //return Promise.all(promises);
  }

  // Get list of services --------------------- START
  getHappyChild(limit?: number): void {
    this.unsubList.push(
      this.crud.getService(this.familyUid, this.node.serviceHc, limit).onSnapshot((snapshot) => {
        this.hcList = [];
        snapshot.forEach((snapshot) => {
          let doc = this.setPendencyHc(snapshot.data());

          this.hcList.push(doc as Service);
        });
        if (this.list && (this.action == this.str.editServiceHc)) this.list = this.hcList;
      }));
  }

  getScheduleList(limit?: number): void {
    this.unsubList.push(
      this.crud.getService(this.familyUid, this.node.serviceSchedule, limit).onSnapshot((snapshot) => {
        this.schedulesList = [];
        snapshot.forEach((doc) => {
          this.schedulesList.push(doc.data() as Service);
        });
        if (this.list && (this.action == this.str.editServiceSchedule)) this.list = this.schedulesList;
      }));
  }

  getFspList(limit?: number): void {
    this.unsubList.push(
      this.crud.getService(this.familyUid, this.node.serviceFsp, limit).onSnapshot((snapshot) => {
        this.fspList = [];
        snapshot.forEach((doc) => {
          this.fspList.push(doc.data() as Service);
        });
        if (this.list && (this.action == this.str.editServiceFsp)) this.list = this.fspList;
      }));
  }

  getGroups(limit?: number): void {
    this.unsubList.push(
      this.crud.getService(this.familyUid, this.node.serviceGroup, limit).onSnapshot((snapshot) => {
        this.groupsList = [];
        snapshot.forEach((doc) => {
          this.groupsList.push(doc.data() as Service);
        });
        if (this.list && (this.action == this.str.editServiceGroup)) this.list = this.groupsList;
      }));
  }

  getRoute(limit?: number): void {
    this.unsubList.push(
      this.crud.getService(this.familyUid, this.node.serviceRoute, limit).onSnapshot((snapshot) => {
        this.routeList = [];
        snapshot.forEach((doc) => {
          this.routeList.push(doc.data() as Service);
        });
        if (this.list && (this.action == this.str.editServiceRoute)) this.list = this.routeList;
      }));
  }

  getBenefit(limit?: number): void {
    this.unsubList.push(
      this.crud.getService(this.familyUid, this.node.serviceBenefit, limit).onSnapshot((snapshot) => {
        this.benefitList = [];
        snapshot.forEach((doc) => {
          this.benefitList.push(doc.data() as Service);
        });
        if (this.list && (this.action == this.str.editServiceBenefit)) this.list = this.benefitList;
      }));
  }

  getReference(limit?: number): void {
    this.unsubList.push(
      this.crud.getService(this.familyUid, this.node.serviceReference, limit).onSnapshot((snapshot) => {
        this.referencesList = [];
        snapshot.forEach((doc) => {
          this.referencesList.push(doc.data() as Service);
        });
        if (this.list && (this.action == this.str.editServiceReference)) this.list = this.referencesList;
      }));
  }

  getVisit(limit?: number): void {
    this.unsubList.push(
      this.crud.getService(this.familyUid, this.node.serviceVisit, limit).onSnapshot((snapshot) => {
        this.visitList = [];
        snapshot.forEach((doc) => {
          let data = doc.data() as Service;
          data.address = this.address;
          this.visitList.push(data);
        });
        if (this.list && (this.action == this.str.editServiceVisit)) this.list = this.visitList;
      }));
  }

  getOldServiceList(limit?: number): void {
    this.unsubList.push(
      this.crud.getService(this.familyUid, this.node.serviceOld, limit).onSnapshot((snapshot) => {
        this.oldServiceList = [];
        snapshot.forEach((doc) => {
          this.oldServiceList.push(doc.data() as Service);
        });
        if (this.list && (this.action == this.str.editServiceOld)) this.list = this.oldServiceList;
      }));
  }

  getNotification(limit?: number): void {
    this.unsubList.push(
      this.crud.getService(this.familyUid, this.node.serviceNotification, limit).onSnapshot((snapshot) => {
        this.notificationList = [];
        snapshot.forEach((doc) => {
          this.notificationList.push(doc.data() as Service);
        });
        if (this.list && (this.action == this.str.editServiceNotification)) this.list = this.notificationList;
      }));
  }

  getTerm(limit?: number): void {
    this.unsubList.push(
      this.crud.getService(this.familyUid, this.node.serviceTerm, limit).onSnapshot((snapshot) => {
        this.termList = [];
        snapshot.forEach((doc) => {
          this.termList.push(doc.data() as Service);
        });
        if (this.list && (this.action == this.str.editServiceTerm)) this.list = this.termList;
      }));
  }

  getDemand(limit?: number): void {
    this.unsubList.push(
      this.crud.getService(this.familyUid, this.node.serviceDemand, limit).onSnapshot((snapshot) => {
        this.demandList = [];
        snapshot.forEach((doc) => {
          this.demandList.push(doc.data() as Service);
        });
        if (this.list && (this.action == this.str.editServiceDemand)) this.list = this.demandList;
      }));
  }

  getAcessuas(limit?: number): void {
    this.unsubList.push(
      this.crud.getService(this.familyUid, this.node.serviceAcessuas, limit).onSnapshot((snapshot) => {
        this.acessuasList = []
        snapshot.forEach((doc) => {
          this.acessuasList.push(doc.data() as Service);
        });
        if (this.list && (this.action == this.str.editServiceAcessuas)) this.list = this.acessuasList;
      }));
  }

  getAttendance(limit?: number): void {
    this.unsubList.push(
      this.crud.getService(this.familyUid, this.node.serviceAttendance, limit).onSnapshot((snapshot) => {
        this.attendanceList = [];
        snapshot.forEach((doc) => {
          this.attendanceList.push(doc.data() as Service);
        });
        if (this.list && (this.action == this.str.editServiceAttendance)) this.list = this.attendanceList;
      }));
  }

  getAepeti(limit?: number): void {
    this.unsubList.push(
      this.crud.getService(this.familyUid, this.node.serviceAepeti, limit).onSnapshot((snapshot) => {
        this.aepetiList = [];
        snapshot.forEach((doc) => {
          this.aepetiList.push(doc.data() as Service);
        });
        if (this.list && (this.action == this.str.editServiceAepeti)) this.list = this.aepetiList;
      }));
  }

  getCondition(limit?: number): void {
    this.unsubList.push(
      this.crud.getService(this.familyUid, this.node.serviceCondition, limit).onSnapshot((snapshot) => {
        this.conditionList = [];
        snapshot.forEach((doc) => {
          this.conditionList.push(doc.data() as Service);
        });
        if (this.list && (this.action == this.str.editServiceCondition)) this.list = this.conditionList;
      }));
  }

  getComplaint(limit?: number): void {
    this.unsubList.push(
      this.crud.getService(this.familyUid, this.node.serviceComplaint, limit).onSnapshot((snapshot) => {
        this.complaintList = [];
        snapshot.forEach((doc) => {
          this.complaintList.push(doc.data() as Service);
        });
        if (this.list && (this.action == this.str.editServiceComplaint)) this.list = this.complaintList;
      }));
  }

  getPendency(limit?: number): void {
    this.unsubList.push(
      this.crud.getService(this.familyUid, this.node.servicePendency, limit).onSnapshot((snapshot) => {
        this.pendencyList = [];
        snapshot.forEach((doc) => {
          this.pendencyList.push(doc.data() as Service);
        });
        if (this.list && (this.action == this.str.editServicePendency)) this.list = this.pendencyList;
      }));
  }

  getInterview(limit?: number): void {
    this.unsubList.push(
      this.crud.getService(this.familyUid, this.node.serviceInterview, limit).onSnapshot((snapshot) => {
        this.interviewList = [];
        snapshot.forEach((doc) => {
          this.interviewList.push(doc.data() as Service);
        });
        if (this.list && (this.action == this.str.editServiceInterview)) this.list = this.interviewList;
      }));
  }

  getFpgb(limit?: number): void {
    this.unsubList.push(
      this.crud.getService(this.familyUid, this.node.serviceFpgb, limit).onSnapshot((snapshot) => {
        this.fpgbList = [];
        snapshot.forEach((doc) => {
          this.fpgbList.push(doc.data() as Service);
        });
        if (this.list && (this.action == this.str.editServiceFpgb)) this.list = this.fpgbList;
      }));
  }

  getMilk(limit?: number): void {
    this.unsubList.push(
      this.crud.getService(this.familyUid, this.node.serviceMilk, limit).onSnapshot((snapshot) => {
        this.milkList = [];
        snapshot.forEach((doc) => {
          this.milkList.push(doc.data() as Service);
        });
        if (this.list && (this.action == this.str.editServiceMilk)) this.list = this.milkList;
      }));
  }

  getSCFV(limit?: number): void {
    this.unsubList.push(
      this.crud.getService(this.familyUid, this.node.serviceSCFV, limit).onSnapshot((snapshot) => {
        this.scfvList = [];
        snapshot.forEach((doc) => {
          // delete lastData
          let data = doc.data();
          delete data.lastMap;
          delete data.core.lastUid;
          this.scfvList.push(data as Service);
        });
        if (this.list && (this.action == this.str.editServiceScfv)) this.list = this.scfvList;
      }));
  }

  //Get Services to dash fsp BY SECTOR
  getDemandBySector(): void {
    this.unsubList.push(
      this.crud.getServiceBySector(this.node.serviceDemand).onSnapshot((snapshot) => {
        this.demandList = [];
        snapshot.forEach((doc) => {
          this.demandList.push(doc.data() as Service);
        });
      }));
  }

  //Get Services to dash fsp BY SECTOR
  getHcBySector(): void {
    let str = this.str;
    let auth = this.auth;

    this.unsubList.push(
      this.crud.getServiceBySector(this.node.serviceHc).onSnapshot((snapshot) => {
        this.hcList = [];
        // this.uidList = [];
        snapshot.forEach((snapshot) => {

          let doc = this.setPendencyHc(snapshot.data());

          // sort data to adm (coordenador)
          if (auth.canEdit([str.editReport])) {
            this.hcList.push(doc as Service);
          } else {
            // console.log(doc, 'doc');
            // this.calcAge
            let fullName = auth.user.fullName + ' - ' + auth.user.proCode;
            if (doc.professional == fullName) {
              this.hcList.push(doc as Service);
            }
          }
          // this.uidList.push(doc.person.uid);
        });
      }));

  }

  //Get Services to dash fsp BY SECTOR
  getFspBySector(): void {
    this.unsubList.push(
      this.crud.getServiceBySector(this.node.serviceFsp).onSnapshot((snapshot) => {
        this.fspList = [];
        snapshot.forEach((doc) => {
          this.fspList.push(doc.data() as Service);
        });
      }));
  }

  //Get Services to dash router BY SECTOR
  getRouteBySector(): void {
    this.unsubList.push(
      this.crud.getServiceRoute(this.node.serviceRoute).onSnapshot((snapshot) => {
        this.routeList = [];
        snapshot.forEach((doc) => {
          this.routeList.push(doc.data() as Service);
        });
      }));
  }

  //get service visit to dashboard
  getVisitBySector(): void {
    this.unsubList.push(
      this.crud.getServiceBySector(this.node.serviceVisit).onSnapshot((snapshot) => {
        this.visitList = [];
        let data: any = { text: this.text.selectDistrict, value: '' };
        this.districtsVisit = [data];
        let district: { [key: string]: number } = {};
        snapshot.forEach((doc) => {
          let s = doc.data() as Service;
          this.visitList.push(s);
        });

        let promises: any[] = [];
        this.visitList.forEach(s => {
          promises.push(this.getAddress(s.familyUid).then(() => {
            s.address = this.address;
            if (s.address && s.address.district) {
              district[s.address.district] = district[s.address.district] ? (district[s.address.district] + 1) : 1;
            }
          }));
        });

        Promise.all(promises).then(() => {
          for (let key in district) {
            let value = district[key];
            let data: any = { text: key + ' (' + value + ')', value: key };
            this.districtsVisit.push(data);
          }
        });

      }));
  }

  //get service schedule to dashboard
  getScheduleBySector(): void {
    this.unsubList.push(
      this.crud.getServiceBySector(this.node.serviceSchedule).onSnapshot((snapshot) => {
        this.schedulesList = [];
        // let data: any = { text: this.text.selectDistric, value: '' };
        this.districtsSchedule = [];
        this.districtsSchedule.push({ text: this.text.selectDistrict, value: '' } as Data);
        let district: { [key: string]: number } = {};
        snapshot.forEach((doc) => {
          let s = doc.data() as Service;
          this.schedulesList.push(s);
        });

        let promises: any[] = [];
        this.schedulesList.forEach(s => {
          promises.push(this.getAddress(s.familyUid).then(() => {
            s.address = this.address;
            if (s.address && s.address.district) {
              district[s.address.district] = district[s.address.district] ? (district[s.address.district] + 1) : 1;
            }
          }));
        });

        Promise.all(promises).then(() => {
          for (let key in district) {
            let value = district[key];
            let data: any = { text: key + ' (' + value + ')', value: key };
            this.districtsSchedule.push(data);
          }
        });
      }));
  }

  //getting notes
  getNotes(limit?: number): void {
    console.log("GET NOTES !!!!");
    this.unsubList.push(
      this.crud.getNotes(this.familyUid, limit).onSnapshot((snapshot) => {
        this.notes = []
        snapshot.forEach((doc) => {
          this.notes.push(doc.data() as Note)
        });
        if (this.list && (this.action == this.str.editNote)) this.list = this.notes;
      }));
  }

  //get family address
  getFamilyAddress(uid: string): void {
    this.address = {} as Address;
    this.unsubList.push(
      this.crud.getAddress(uid).onSnapshot((snapshot) => {

        if (!snapshot.empty) {
          let data = snapshot.docs[0].data();
          delete data.writeAt;
          this.address = data as Address;
        }

      }));

  }

  getPerson(uid: string): Promise<void> {
    return this.crud.getPerson(uid).get()
      .then((snapshot) => {
        // this.doc.person = {};
        console.log('GETTING PERSON', uid, snapshot.ref, snapshot.data());

        if (snapshot.exists) {
          let person = snapshot.data() as Person;
          this.doc.person = person;
        }

      });
  }

  getFamilyPerson(doc: any): Promise<void> {
    return this.crud.getFamilyPerson(doc).get()
      .then((snapshot) => {
        // this.doc.person = {};
        console.log('GETTING PERSON', doc.person.uid, snapshot.ref, snapshot.data());

        if (snapshot.exists) {
          let person = snapshot.data() as Person;
          this.doc.person = person;
        }

      });
  }

  getPersonList(uid: string): Promise<void> {
    // if(!this.people) this.people = [];
    return this.crud.getPerson(uid).get()
      .then((snapshot) => {
        if (!this.people) this.people = [];
        if (snapshot.exists) {
          this.people.push(snapshot.data() as Person);
          console.log(snapshot.data());
        }
      });
  }

  getAddress(uid: string, isPromise?: boolean): Promise<void> {
    let promise = this.crud.getAddress(uid).get()
      .then((snapshot) => {
        this.address = {} as Address;
        if (!snapshot.empty) {
          let address = snapshot.docs[0].data() as Address;
          this.address = address;
        }
      });
    // if (mise) return promise;
    return promise;
  }

  //Get family members and address
  private getFamily(uid: string): void {
    this.address = {};
    this.oldFamilyId = "";
    this.unsubList.push(
      this.crud.getPeople(this.familyUid).onSnapshot((snapshot) => {
        //getting all family members
        this.people = [];
        snapshot.forEach((doc) => {
          if (doc.exists) {
            let person: Person = doc.data() as Person;
            delete person.writeAt;

            if(person.oldFamilyId) this.oldFamilyId = person.oldFamilyId;
            
            person.age = this.calcAge(JSON.stringify(person.birthday));

            if (person.kinship != 11) {
              this.people.push(person);
            } else {
              this.people.unshift(person);
            }
          }
        })
      }));
    //get family address
    this.getFamilyAddress(this.familyUid);

  }
  // Get a list of services --------------------- END

  //Get list district -------------------------------- START

  listDistrict(code: string): Promise<void> {
    return this.crud.listDistrict(code)
      .get()
      .then((snapshot) => {
        this.districts = [];
        snapshot.forEach((doc) => {
          this.districts.push(doc.data() as District);
        });
      });
  }

  //Get list district -------------------------------- END

  //CRUD - User functions in CRUCD ---------------------------------- END

  //Router actions --------------------------------------------------- START
  //Router to meeting list scfv
  routerToScfvMeetList(doc: any): void {
    this.meeting = [];
    let node = this.node.coreMeeting;
    this.listMeeting(doc, node, this.str.coreUid);
    this.router.navigate(['/dashboard/frequency/meet']);
  }

  // Get people and meeting to groups report
  getMeetingData(doc: Service, nodePeople: string,
    nodeMeeting: string, page: string, prop: string) {
    this.meeting = [];
    let promises = [];
    this.people = [];
    //

    this.listPeopleData(doc.uid, nodePeople).then(() => {
      let members = this.members;
      console.log(`member`, members);

      if (this.hasMembers) {
        this.listPeople(members).then(() => {

          this.listMeetingReport(doc, nodeMeeting, prop).then(() => {
            this.generateReportService();
            if (this.pages && this.pages.length) {
              // this.router.navigate(['/dashboard/' + page]);

              this.getCity(this.auth.user.city.code, false, true).then(() => {
                this.printService.frequency(this.pages, this.docPrint.city);
              });

            } else {
              this.alertFeedback(this.text.noData);
            }
          });
          Promise.all(promises).then(() => {
            this.setMeetData(doc);
          });
        });
      } else {
        this.alertFeedback(this.text.noMembers);
      }
    });
  }
  //router To Report Scfv
  routerToScfvReport(doc: any): void {
    this.getMeetingData(doc, this.node.corePeople, this.node.coreMeeting,
      'frequency/report-scfv', this.str.coreUid);
  }

  //router To Report Group
  routerToGroupReport(doc: any): void {
    this.getMeetingData(doc, this.node.groupPeople, this.node.groupMeeting, 'group/report-group', this.str.groupUid);
  }

  //Router To Meet List
  routerToMeetList(doc: any, node: string): void {
    this.meeting = [];
    //let node: string = this.node.groupMeeting;
    this.listMeeting(doc, node, this.str.groupUid);
    this.router.navigate(['/dashboard/group/meet']);
  }

  //Router to family_Detail
  routerToFamily(uid: string): void {
    // if(uid) {
    //   this.router.navigate(['/dashboard/family-detail', uid])
    // } else {
    //   let link = '/dashboard/family-detail/' + this.familyUid
    this.router.navigate(['/dashboard/family-detail', uid])
    if (this.modalRef) {
      this.modalRef.hide();
    }
    //   }
    // }
    console.log("routerToFamily", uid);

  }

  routerToDashboard(): void {
    this.router.navigate(['/dashboard/content']);
  }

  routerToHome(): void {
    this.router.navigate(['/']);
  }

  routerToFamilyDataList(template: TemplateRef<any>, action: string, list: any,
    title: string, model: string, isNested?: boolean): void {
    if (this.modalRef) { this.modalRef.hide(); }
    if (!this.checkResponsible()) { this.alertFeedback(this.text.noResponsible); return };

    if (!template || !action || !list || !title || !model || !this.familyUid) { this.routerToDashboard(); return }

    this.modal = template;
    this.action = action;
    this.list = list;
    this.getDataByAction(action);
    this.title = title;
    this.model = model;
    this.isNested = isNested;
    this.initDoc({}, action);
    console.log("routerToFamilyDataList", 'action', action, 'people', this.people);

    this.router.navigate(['/dashboard/family-data-list', this.familyUid]);

  }
  //Router actions --------------------------------------------------- END

  //Utilitaries ------------------------------------------------------ START
  cloneDeep(obj: any): any {
    return _.cloneDeep(obj);
  }

  setPendencyHc(doc: any): Service {
    // let doc = data as Service;
    console.log('hc', doc.person.name);

    let today = new Date();

    let m = 1;
    let t = 1;

    if (doc.updateAt && doc.person.birthday) {

      let writeAt = doc.writeAt.toDate();
      let changeDate = doc.monthUpdate ? this.toDate(doc.monthUpdate) : writeAt;
      let birthDate = this.toDate(doc.person.birthday);
      let quartDate = doc.quartUpdate ? this.toDate(doc.quartUpdate) : writeAt;

      let changeGapMonth = this.calcDaysElapsed(changeDate, today) / 30.4;
      let birthGapMonth = this.calcDaysElapsed(birthDate, today) / 30.4;
      let quartGapMonth = this.calcDaysElapsed(quartDate, today) / 30.4;

      if (changeGapMonth >= 1) {

        m += Math.floor(changeGapMonth);
      }

      let quart = Math.floor(quartGapMonth / 3);
      if (quart >= 3) {
        t += quart;
      }

    }
    doc.month = m;
    doc.quart = t;
    console.log('month', m, ' t ', t);
    return doc;
  }

  hasMembers(members: any[]): boolean {
    for (let m of Object.keys(members)) {
      if (members[m]) return true;
    }
    return false;
  }

  // Set people and meet data
  setMeetData(doc: any) {
    if (!doc.coreUid && !doc.groupUid) {
      let meet = {};
      this.people.forEach((p) => {
        p.present = true;
        meet[p.uid] = true;
      });
      this.doc.meet = meet;
    } else {
      this.people.forEach((p) => {
        p.present = doc.meet[p.uid] ? true : false;
      });
    }
  }

  findDistrictInDataList(query: string, list: any): void {
    this.listSearch = [];
    if (!query) {
      list.forEach((doc: any, index: number) => {
        if (index <= 6) {
          this.listSearch.push(doc);
        }
      });
    } else {
      console.log('list', list);
      list.forEach((doc) => {
        if (doc.name.toLowerCase().indexOf(query.toLowerCase()) >= 0) {
          if (this.listSearch.length <= 6) this.listSearch.push(doc);
        }
      });
    }
  }
  //
  findInDataset(query: string, array: any) {
    // console.log(query);
    this.listUser = [];
    // let count: number = 0;
    this.users.forEach((user: User, index: number) => {
      // count++;
      if (!query) {
        if (index <= 6) {
          this.listUser.push(user);
        }
      } else {
        if (user.fullName.toLowerCase().indexOf(query.toLowerCase()) >= 0) {
          if (this.listUser.length <= 6) this.listUser.push(user);
          //  console.log(user);
        }
      }
    });
    // //set professionalUid
    console.log('findInDataset');

    // this.isOk = this.users.findIndex(i => i.fullName == query) >= 0;

  }

  // finding inside collections that contain text propety
  findInDataset2(query: string, array: any[]): void {
    //console.log(query, array);

    let listSearch: any = [];
    array.forEach((e: any, index: number) => {
      // count++;
      if (!query) {
        if (index <= 6) {
          listSearch.push(e);
        }
      } else {
        if (e.text.toLowerCase().indexOf(query.toLowerCase()) >= 0) {
          if (listSearch.length <= 6) listSearch.push(e);
          //  console.log(user);
        }
      }
    });
    this.listSearch = listSearch;
    // this.valid[0] = listSearch.findIndex(i => i.text == query) >= 0;
  }

  // finding inside collections that contain text propety
  findInDataset3(query: string, array: any[], index: number, prop: string): void {
    console.log(query, array);

    let listSearch: any = [];
    array.forEach((e: any, index: number) => {
      // count++;
      if (!query) {
        if (index <= 6) {
          listSearch.push(e);
        }
      } else {
        if (e[prop].toLowerCase().indexOf(query.toLowerCase()) >= 0) {
          if (listSearch.length <= 6) listSearch.push(e);
          //  console.log(user);
        }
      }
    });
    this.listSearch2[index] = listSearch;
  }

  //Register a new user
  createUserAccount(doc: any, action: string): void {
    // console.log(doc);

    this.auth.createUser(doc).subscribe((result: any) => {
      if (result.code == 'success') {

        console.log("createUser", result.code);
      }

    });
  }

  //Sign in a user
  signIn() {
    let doc = this.doc;
    this.feedback = null;
    this.auth.signIn(doc.email, doc.password).then(() => {
      this.unsubList = [];
      // this.getTextService();
      this.routerToDashboard();
    }).catch((error) => {
      this.feedback = this.text.accessInformationsError;
    });
  }

  updatePass(pass: string): void {
    console.log('pass', pass);

    this.auth.updatePass(pass).then(() => {
      // Update successful.
      this.alertFeedback(this.text.operationSuccess);
    }).catch((error) => {
      // An error happened.
      this.alertError(this.text.signInAgain)
      //console.log('error', error);

    });
  }

  //filter service demand by district
  filterByDistrict(query: string, services: Service[], action: string): void {

    let str = this.str;
    if (action == str.editServiceVisit) {
      this.listQueryVisit = [];
      this.queryVisit = true;
      console.log(".editServiceVisit");
      services.forEach((s) => {
        if (s.address && s.address.district == query) this.listQueryVisit.push(s);
      });
      if (!query) this.queryVisit = false;
    }
    if (action == str.editServiceSchedule) {
      this.listQuerySchedule = [];
      this.querySchedule = true;
      services.forEach((s) => {
        if (s.address && s.address.district == query) this.listQuerySchedule.push(s);
      });

      if (!query) this.querySchedule = false;
    }

  }

  checkResponsible(): boolean {
    return this.people && (this.people.findIndex(i => i.kinship == 11) >= 0);
  }

  checkKinship(k: string): void {
    if (k == this.text.responsibleFamiliar
      && this.people.findIndex(i => i.kinship == 11) >= 0) {
      this.isOk = false;
      this.alertFeedback(this.text.hasResponsible);
    } else {
      this.isOk = true;
    }
  }

  checkSortPeople(): boolean {
    return this.sortPeople && this.sortPeople.length > 0;
  }

  checkNis(doc: any, pos?: number): void {
    if (this.familyUid) {
      if (!doc.nis) {
        this.valid[pos] = false;
        // this.nisOk = false;
        this.alertFeedback(this.text.noNis);
      } else {
        // this.nisOk = true;
        this.valid[pos] = true;
      }
    } else {
      this.valid[pos] = true;
    }
  }

  checkProfessional(doc: any, pos: number): void {
    this.valid[pos] = doc.professional ? true : false;
    // if (doc.professional) this.valid[2] = true;
  }

  checkDocPerson(docNum: string, docType: string): void {
    if (docNum && docNum.length == 11) {

      this.crud.searchPeople(docNum)
        .subscribe((data: Elastic) => {
          this.isOk = true;
          console.log(data.hits.hits);
          data.hits.hits.forEach((item) => {
            if (docType == this.text.cpf && item._source.cpf == docNum) {
              this.isOk = false;
            }
            if (docType == this.text.nis && item._source.nis == docNum) {
              this.isOk = false;
            }
          });
          if (!this.isOk) {
            this.alertFeedback(this.text.registeredDoc);
            return;
          }
        }, err => {
          console.error(err);
          if (err.status == 500 && this.auth.user.roles.oneAboveAll) this.isOk = true;
        });
    } else {
      this.isOk = false;
    }
  }

  checkCpf(doc: string): boolean {
    return doc && doc.length == 11;
  }

  checkNis2(doc: string): boolean {
    return doc && doc.length == 11;
  }

  checkDocPerson2(docNum: string, docType: string, pos: number): void {
    console.log(JSON.stringify(this.valid));
    console.log(this.valid, docNum, docType);

    if (docNum && docNum.length == 11) {
      this.valid[pos] = true;

      this.crud.searchPeople(docNum)
        .subscribe((data: Elastic) => {
          data.hits.hits.forEach((item) => {
            if (docType == this.text.cpf && item._source.cpf == docNum) {
              this.valid[pos] = false;
            }
            if (docType == this.text.nis && item._source.nis == docNum) {
              this.valid[pos] = false;
            }
          });
          if (!this.valid[pos]) {
            this.alertFeedback(this.text.registeredDoc);
            return;
          }
          console.log(this.valid[pos]);
        }, err => {
          console.error(err);
        });
    } else {
      this.valid[pos] = false;
    }
  }

  checkEmailUser(email: string) {
    let check = new RegExp(/^[^\s@]+@[^\s@]+\.[^\s@]+$/).test(email);
    if (check) {
      this.feedback = null;
      this.auth.fetchSignInForEmail(email).then((methods) => {
        let register: boolean = methods.length > 0;
        this.isOk = !register;
        if (register) {
          this.alertFeedback(this.text.registeredEmail);
        }
      });
    } else {
      this.feedback = this.text.invalidEmail;
    }
  }

  checkStatusServiceType2(doc: any, arr: Service[], pos: number): boolean {
    //console.log(doc);

    let check = arr.findIndex(i => i.person.uid == doc.person.uid && i.status == 1
      && i.type == doc.type) >= 0;
    this.valid[pos] = !check;
    console.log(check);

    if (check) this.alertFeedback(this.text.registeredOnService);
    return check;
  }

  checkStatusServiceType(doc: any, arr: Service[], act: string, isChange?: boolean) {
    console.log(doc);

    let index = -1;
    if (act == this.str.editServiceOld) {
      console.log(doc);

      index = arr.findIndex(i => i.person.uid == doc.person.uid && i.status == 1
        && i.type == doc.type);
    }
    if (index >= 0) {
      this.isOk = false;
      // this.valid[pos] = false;
      this.alertFeedback(this.text.registeredOnService);
    } else {
      this.isOk = true;
      // this.valid[pos] = true;
      if ((isChange && act == this.str.editServiceOld) || !doc.uid && doc.person) this.checkNis(doc.person);
    }
  }

  checkStatusOnService(doc: any, arr: Service[], template?: TemplateRef<any>, checkNis?: boolean) {
    // console.log("checkDoc", doc);
    // console.log("arr", arr);
    let index = arr.findIndex(i => i.person.uid == doc.person.uid && i.status == 1);

    if (index >= 0) {
      let msg = this.text.registeredOnService;
      this.feedback = msg;
      this.isOk = false;
      this.alertFeedback(msg);
    } else {
      this.feedback = null;
      this.isOk = true;
    }
    // checkNIs
    if (doc.person && checkNis) this.checkNis(this.doc.person);
    if (template) this.setModalRef(template);
  }

  checkStatusOnService2(doc: any, arr: Service[], pos: number): boolean {
    // console.log("checkDoc", doc);
    // console.log("arr", arr);
    if (this.familyUid) {

      let index = arr.findIndex(i => i.person.uid == doc.person.uid && i.status == 1);

      if (index >= 0) {
        let msg = this.text.registeredOnService;
        this.feedback = msg;
        this.valid[pos] = false;
        this.alertFeedback(msg);
        return true;
      } else {
        this.feedback = null;
        this.valid[pos] = true;
        console.log(this.valid);

        return false;
      }
    } else {
      this.valid[pos] = true;
      return true;

    }
    // if (template) this.setModalRef(template);
  }

  grantStatusGroup(doc: any): boolean {
    // if a document alright was writed
    if (doc.uid) {
      let $doc = this.$doc;
      return this.scfvList.findIndex(i => i.core.uid == $doc.core.uid
        && i.person.uid == $doc.person.uid && i.status == 1) >= 0;
    }
    return false;
  }

  checkStatusOnCore(doc: any, pos?: number): boolean {
    console.log(doc, 'coreDoc');

    let index = this.scfvList.findIndex(i => i.core.uid == doc.core.uid
      && i.person.uid == doc.person.uid && i.status == 1);
    console.log(index, 'indexCheck');

    this.feedback = null;
    let check = index >= 0;
    if (check) {
      this.feedback = this.text.registeredOnCore;
      this.valid[pos] = false;
      this.alertFeedback(this.text.registeredOnCore);

    } else {
      this.valid[pos] = true;
    }

    return check;
    // if (doc.uid) {
    //   let $doc = this.$doc;
    //   this.isOk = this.scfvList.findIndex(i => i.core.uid == $doc.core.uid
    //     && i.person.uid == $doc.person.uid && i.status == 1) >= 0;
    // }

  }

  checkStatusOnGroup(doc: any, pos: number): boolean {
    let index = this.groupsList.findIndex(i => i.group.uid == doc.group.uid
      && i.person.uid == doc.person.uid && i.status == 1);

    this.feedback = null;
    let check = index >= 0;
    if (check) {
      this.feedback = this.text.registeredOnGroup;

      this.valid[pos] = false;
      this.alertFeedback(this.text.registeredOnGroup);
    } else {
      this.valid[pos] = true;
    }
    return check;

  }

  checkDistrict(query: string, array: any[], pos: number): boolean {
    let check = array.findIndex(i => i.name == query) >= 0;
    this.valid[pos] = check;
    return check;
  }

  setMapDisabled(doc: Service, check: boolean): void {
    let map: { [key: string]: boolean } = {};
    map[doc.person.uid] = !check;
    this.doc.map = map;
    console.log('disabled', this.doc, !check);

  }

  setMapGroup(doc: Service, prop: string): any {
    let map: { [key: string]: boolean } = {};
    map[doc.person.uid] = true;
    this.doc.map = map;

    // set Last Map
    if (doc.uid) {
      doc = this.setLastMap(doc, prop);
    }

    this.doc = doc;
    console.log(this.doc);

    return map;
  }

  hideProfessional(pos: number, check: boolean): void {
    if (!check) {
      this._doc.professional = {};
      this.doc.professional = {};
      this._doc.listUser = [];
    }
    this.valid[pos] = !check;
  }

  //Clean professional
  cleanProfessional(pos: number): void {
    this._doc.professional = {};
    this.doc.professional = {};
    this.valid[pos] = this._doc.listUser.length > 0 ? true : false;
    console.log(this.valid[pos], this._doc.listUser.length);

  }

  // setProfessionalInfo
  setProfessional(name: string, pos: number): void {
    let email = name.split(' - ')[1];
    let p = this.listUser.find(i => i.email == email);
    this.valid[pos] = false;

    if (p && p.uid) {
      this.valid[pos] = true;
      this._doc.professional = { uid: p.uid, info: name };
    } else {
      this._doc.professional = { uid: '', info: '' };

    }
  }

  setRedField(valid: boolean) {
    let color = valid ? '#28a745' : '#dc3545';
    let styles = {
        'border-color': color,
        'box-shadow': color
    };

    // .test:focus {
    //   border: 1px solid  red;
    //   box-shadow: 0 0 0 .2rem green
    // }
    return styles;

  }

  checkProfissional(doc: any): boolean {
    return doc && doc.professional && doc.professional.uid && doc.professional.info;
  }

  setCoreByIndex(data: Core): void {
    this.doc.core = { uid: data.uid, name: data.name };
  }

  setGroupByIndex(data: Group): void {
    this.doc.group = { uid: data.uid, name: data.name };
  }

  setDocByIndex(i: number, act: string, list: any): void {
    this.feedback = null;
    let str = this.str;
    let data = list[i];
    let doc = this.doc;

    if (act == str.group) {
      // doc.name = data.name;
      // doc.index = i;
      doc.group.name = data.name;
      doc.group.uid = data.uid;
    }

    if (act == str.personGroup) {
      doc.person = data;
      this.checkNis(data);
    }

    if (act == str.personCore) {
      doc.person = data;
      // doc.map = this.setMapGroup(data);
      this.checkStatusOnCore(doc);
      this.checkNis(data);
    }


    if (act == str.core) {
      doc.core.uid = data.uid;
      doc.core.name = data.name;
      this.checkStatusOnCore(doc);
    }
  }

  setNestedDoc(doc: any, action: string, form?: boolean) {
    if (form) {
      let data = _.cloneDeep(doc);
      ///get family uid of modal open
      // if (!data.familyUid) data.familyUid - this.doc.familyUid;

      let str = this.str;
      if (action == this.str.esRouteAnswer) {
        if (!data.isAnswer) {
          data.isAnswer = true;
          data.status = 9;
          data.sector = data.user.subSector;
          data.node += '-' + this.node.city;
          delete data.uid;
          this.writeDoc(this.str.editServiceRoute, doc);
          // } else {
          //   data.active = false;
        }
      }

      if (this.modalRef && (this.action == this.str.esFspNotes
        || action == this.str.esHcNotes)) { this.modalRef.hide(); }

      this.writeDoc(action, data);
    } else {
      this.onCancel();
    }
    console.log("nesteddoc", doc, action);

  }

  //Set doc
  setDoc(action: string): void {
    console.log(this._doc);

    let _doc = this._doc;
    let doc = this.doc;
    let merge = false;
    let str = this.str;
    let text = this.text;
    console.log('setDocaction', action, 'doc', doc);

    if (doc.uid) {

      if (_doc) {
        console.log("_DOC true", _doc);

        merge = JSON.stringify(this.$_doc) !== JSON.stringify(_doc);
        console.log("MERGE", merge);
      }

      let docChange = JSON.stringify(this.$doc) !== JSON.stringify(doc);

      let canUpdate = true;
      if (doc.writeAt) {
        canUpdate = this.calcDaysElapsed(this.doc.writeAt.toDate(), new Date()) <= 3;
      }

      //data that there no need check days elapsed

      if (!canUpdate && docChange) {
        this.alertFeedback(this.text.threeDaysElasped);
      }

      if (canUpdate) {
        //this.doc = doc;
      }

      if (merge) {
        //this.doc = { ...this.$doc, ..._doc };
        // doc = { ...this.$doc, ..._doc };
        doc = { ...this.doc, ..._doc };
        docChange = true;
        //Avoid edit data just because this.merge is true
      }

      console.log("SET DOC", doc, "DocChange", docChange);

      if (docChange && (canUpdate || merge)) {
        this.writeDoc(action, doc);
      }

    } else {
      console.log("here doc not uid", action);
      if (action == str.editLocation) doc = { ...doc, ..._doc };
      if (action == str.editServiceReference) doc = { ...doc, ..._doc };
      if (action == str.editServiceInterview) doc = { ...doc, ..._doc };
      if (action == str.editServiceHc) doc = { ...doc, ..._doc };
      if (action == str.editServiceAttendance) doc = { ...doc, ..._doc };
      if (action == str.editAddress) doc = { ...doc, ..._doc };
      console.log("SET DOC", doc);

      this.writeDoc(action, doc);

    }

  }
  // Set last data to groups
  setLastMap(doc: Service, prop: string): any {
    //let doc = this.doc;
    delete doc[prop].lastUid;
    delete doc.lastMap;
    console.log(JSON.stringify(this.$doc.map), JSON.stringify(doc.map));
    console.log(this.$doc[prop].uid, doc[prop].uid);

    if (JSON.stringify(this.$doc.map) != JSON.stringify(doc.map) || this.$doc[prop].uid != doc[prop].uid) {
      console.log('test', doc.lastMap);
      doc[prop].lastUid = this.$doc[prop].uid;
      // doc.lastMap = this.setLastMap(doc,);
      let map = this.$doc.map;
      Object.keys(map).forEach((key) => {
        map[key] = false;
      });
      doc.lastMap = map;
    }

    console.log("setLastMap", this.doc);

    return doc;
  }

  //Getting today date from firebase
  today(): firebase.firestore.Timestamp {
    return firebase.firestore.Timestamp.now();
  }

  //Alerts --------------------------------------------------- START
  alertFeedback(msg: string): void {
    // this.alerts = []
    this.alerts.push({
      type: this.str.feedback,
      msg: msg,
      timeout: 5000
    });
  }

  alertError(msg: string): void {
    // this.alerts = []
    this.alerts.push({
      type: this.str.feedback,
      msg: msg,
      timeout: 5000
    });
  }

  alertProcessing(code: number): void {
    //  this.alerts = []
    this.alerts.push({
      type: this.str.process,
      msg: this.text.processing,
      code: code
    })
  }

  onAlertClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }
  //Alerts --------------------------------------------------- END

  //BackHistory
  backHistory() {
    this.location.back();
  }

  //Initaliazing page group meeting
  onInitGroupMeetReport(): void {
    if (!this.pages) this.router.navigate(['/dashboard/group']);
  }

  //Initaliazing page group meeting
  onInitGroupMeet(): void {
    if (!this.meeting) this.router.navigate(['/dashboard/group']);
  }

  //Initaliazing page meeting
  onInitMeetList(): void {

  }

  //Initializing  page frequency
  onInitFrequency(): void {
    this.doc = {};

    this.cores = [];
    if (!this.auth.user) {
      this.subscription = this.auth.userData.subscribe((user: User) => {
        this.auth.user = user;

        if (user) {
          this.subscription.unsubscribe();
          if (this.auth.canEdit([this.str.esScfv], true)) this.listCoresToCity(this.val.LIMIT_TWENTY);
        }
      });
      this.subscription.unsubscribe;
    } else {
      if (this.auth.canEdit([this.str.esScfv], true)) this.listCoresToCity(this.val.LIMIT_TWENTY);
    }
  }

  // init user profile
  onInitUserProfile(): void {
    this.doc = {};

    if (!this.auth.user) {
      this.subscription = this.auth.userData.subscribe((user: User) => {
        this.auth.user = user;

        if (user) {
          this.subscription.unsubscribe();
          // if (this.auth.canEdit([this.str.esScfv], true)) this.listCoresToCity(this.val.LIMIT_TWENTY);
        }
      });
      this.subscription.unsubscribe;
    } else {
      // if (this.auth.canEdit([this.str.esScfv], true)) this.listCoresToCity(this.val.LIMIT_TWENTY);
    }
  }

  //Initializing dashboard
  onInitDashboard(): void {
    if (location.pathname == '/dashboard') this.router.navigate(['/dashboard/content']);
    //this.routerToDashboard();
  }

  onInitDashboardContent(): void {
    // this.getTextService();

    this.doc = {};
    this.hide = [];

    this.routeList = [];
    this.visitList = [];
    this.schedulesList = [];
    this.demandList = [];
    this.hcList = [];

    this.fspList = [];
    this.address = null;
    this.people = null;

    if (!this.auth.user) {
      this.subscription = this.auth.userData.subscribe((user: User) => {
        this.auth.user = user;
        if (user) {
          this.getServiceToDashContent();
          this.subscription.unsubscribe();
        }
      });
      this.subscription.unsubscribe;
    } else {
      this.getServiceToDashContent();
    }
    //this.writeTextService();
    // this.writeArrayService ();
  }

  onInitGroup(): void {
    this.onAction = this.str.editGroup;
    this.doc = {};

    this.groups = [];
    if (!this.auth.user) {
      this.subscription = this.auth.userData.subscribe((user: User) => {
        this.auth.user = user;

        if (user) {
          this.subscription.unsubscribe();
          if (this.auth.canEdit([this.str.esGroup], true)) this.getGroupsToContent(this.val.LIMIT_TWENTY);
        }
      });
      this.subscription.unsubscribe;
    } else {
      if (this.auth.canEdit([this.str.esGroup], true)) this.getGroupsToContent(this.val.LIMIT_TWENTY);
    }
  }


  //Initializing Date-Report
  onInitReport(): void {
    // this.doc = {};
    this.list = this.arr.analyticalReportType;
    let doc: any = {};
    doc.startDate = new Date(2014, 1, 1);
    doc.endDate = new Date();
    doc.report = this.list[0];

    doc.filter = [];
    this.loading = false;
    this.doc = doc;
  }

  //Initializing edit-user
  onInitEditUser(): void {
    this.users = [];
    this.onAction = this.str.editUser;
  }


  //Initializing edit-user
  onInitCity(): void {
    this.doc = {};
    this.doc.address = {};
    // this.initDoc(null, this.str.editCity);

    if (!this.auth.user) {
      this.subscription = this.auth.userData.subscribe((user: User) => {
        this.auth.user = user;
        if (user) {
          this.subscription.unsubscribe();
          if (this.auth.canEdit([this.str.editCity], true)) this.getCity(this.auth.user.city.code, true);
        }
      });
      this.subscription.unsubscribe;
    } else {
      if (this.auth.canEdit([this.str.editCity], true)) this.getCity(this.auth.user.city.code, true);
    }
  }

  //Init page content main list  
  onInitPageContent(action: string): void {
    this.list = [];
    this.doc = {};
    console.log('onit day');

    if (!this.auth.user) {
      this.subscription = this.auth.userData.subscribe((user: User) => {
        this.auth.user = user;

        if (user) {
          this.subscription.unsubscribe();
          if (this.auth.canEdit([this.str[action]], true)) this.getListToPageContent(action);
        }
      });
      this.subscription.unsubscribe;
    } else {
      if (this.auth.canEdit([this.str[action]], true)) this.getListToPageContent(action);
    }
  }

  getListToPageContent(action: string) : void {
    let str = this.str;

    switch(action) {
      case str.editDistrict:
        //        
        this.getDistrictsToContent(this.val.LIMIT_TWENTY)
        break;
      case str.editPsf:
        //
        this.getDistrictsToContent(this.val.LIMIT_TWENTY)
        break;
      default:
        //
        console.log('getListToPageContent', action);
        
    }
  }

  //Initializing edit-family
  onInitEditFamily() {
    //this.setRequired();
    //this.districts = [];
    //this._doc = {};
    /*  this.listSearch2 = [];
     this.InitListSearch(0, this.arr.states); */
    //this.doc = { address: { city: {}, state: 0 }, district: {} }
    this.doc = {};
    this.valid = [false, false];


    // this.districts = [];
    if (!this.auth.user) {
      this.subscription = this.auth.userData.subscribe((user: User) => {
        this.auth.user = user;

        if (user) {
          this.subscription.unsubscribe();
        }
        this.initDoc({}, this.str.editFamily);
      });
      this.subscription.unsubscribe;

    } else {
      this.initDoc({}, this.str.editFamily);
    }

  }

  InitListSearch(index: number, array: any[]): void {
    let arr = [];
    for (let i = 0; i < array.length; i++) {
      arr.push(array[i]);
      if (i == 5) break;
    }
    if (!this.listSearch2) this.listSearch2 = [];
    this.listSearch2[index] = arr;
  }

  //Initializing family-data-list
  onInitFamilyDataList(route: ActivatedRoute) {
    if (!this.modal || !this.onAction || !this.list || !this.title || !this.model || !this.familyUid
      || !this.people) {
      this.getPageParams(route, this.str.familyDataList);

    }


    //getLocationPage
  }

  //Initializing family-detail var
  onInitFamilyDetail() {

    this.onAction = this.str.familyDetail;
    // this.action = null;
    //init arrays
    this.alerts = [];
    this.routeList = [];
    this.visitList = [];
    this.benefitList = [];
    this.referencesList = [];
    this.attendanceList = [];
    this.schedulesList = [];
    this.fspList = [];
    this.hcList = [];
    this.oldServiceList = [];
    this.notificationList = [];
    this.termList = [];
    this.demandList = [];
    this.acessuasList = [];
    this.groupsList = [];
    this.aepetiList = [];
    this.conditionList = [];
    this.complaintList = [];
    this.pendencyList = [];
    this.interviewList = [];
    this.fpgbList = [];
    this.milkList = [];
    this.scfvList = [];
    this.notes = [];



    if (!this.auth.user) {
      this.subscription = this.auth.userData.subscribe((user: User) => {
        this.auth.user = user

        if (user) {
          this.subscription.unsubscribe();
          this.getServices(this.val.LIMIT_TWO);
          this.getNotes(this.val.LIMIT_FOUR);
        }
      })
    } else {
      this.getServices(this.val.LIMIT_TWO);
      this.getNotes(this.val.LIMIT_FOUR);
    }

    this.getFamily(this.familyUid)

  }

  //get list of groups to edit-group
  getDistrictsToContent(limit?: number): void {
    this.unsubList.push(
      this.crud.listDistrict(this.auth.user.city.code, limit).onSnapshot((snapshot) => {
        //this.districts = [];
        this.list = [];
        snapshot.forEach((doc) => {
          //this.districts.push(doc.data() as District);
          this.list.push(doc.data() as District);
        });
      }));
  }

  //get list of groups to edit-group
  getGroupsToContent(limit?: number): void {
    this.unsubList.push(
      this.crud.listGroup(limit).onSnapshot((snapshot) => {
        this.groups = [];
        snapshot.forEach((doc) => {
          this.groups.push(doc.data() as Group);
        });
      }));
  }

  //Get lists of services to dashboard content
  getServiceToDashContent(): void {
    let auth = this.auth;
    let str = this.str;
    if (auth.canEdit([str.esRoute])) this.getRouteBySector();
    if (auth.canEdit([str.esFsp])) this.getFspBySector();
    if (auth.canEdit([str.esVisit])) this.getVisitBySector();
    if (auth.canEdit([str.esSchedule])) this.getScheduleBySector();
    if (auth.canEdit([str.esDemand])) this.getDemandBySector();
    if (auth.canEdit([str.esHc])) this.getHcBySector();
  }

  //Get lists of services
  getServices(limit: number): void {
    this.getHappyChild();
    this.getScheduleList();
    this.getFspList();
    this.getGroups();
    this.getRoute();
    this.getBenefit();
    this.getReference();
    this.getVisit();
    this.getOldServiceList();
    this.getNotification();
    this.getTerm();
    this.getDemand();
    this.getAcessuas();
    this.getAttendance();
    this.getAepeti();
    this.getCondition();
    this.getComplaint();
    this.getPendency();
    this.getInterview();
    this.getFpgb();
    this.getMilk();
    this.getSCFV();
  }

  //Get lists of services
  getDataByAction(action: string): void {
    let str = this.str;
    if (action == str.esFspNotes) this.listNotesFsp(this.doc);
    if (action == str.esHcNotes) this.listNotesHc(this.doc);
  }

  //Initialise home
  onInitHome(): void {
    this.doc = {};

  }

  initDoc2(action: string, doc: any): any {
    if (doc) {
      return doc;
    } else {
      let _doc = {};
      if (action == 'oi') {

      }
    }
  }

  //Initializing propety in a document
  initDoc(data: any, act?: string): void {
    console.log('INITDOC');
    let doc: any = {};
    doc.city = this.auth.user.city;
    if (data && data.uid) {
      doc = _.cloneDeep(data);
      console.log('Lodash', doc);
    }

    let text = this.text;
    let arr = this.arr;
    let node = this.node;
    let str = this.str;
    //  let user: User = this.auth.user;
    let action = act;
    //if(act) action = act;

    let familyUid = this.familyUid;
    let people = this.people;

    // //Init _DOC
    // if (action == str.editLocationEditFamily) {
    //   this._doc = { ...doc };
    //   // this.setGeolocation()
    //   console.log('UID', this._doc.familyUid);
    //   return;
    // }

    if (action == str.esFspNote || action == str.esFspNotes
      || action == str.esHcNote || action == str.esHcNotes) {
      // if (this.isNested) {
      console.log('note: ', doc);
      if (action == str.esFspNote || action == str.esFspNotes) {
        this._doc.note = data.status ? { node: node.fspNotes, familyUid: doc.familyUid, parentUid: doc.uid } : doc;

      }

      if (action == str.esHcNote || action == str.esHcNotes) {
        this._doc.note = data.status ? { node: node.hcNotes, familyUid: doc.familyUid, parentUid: doc.uid } : doc;

      }

      // this._doc.note = data.status ? { familyUid: doc.familyUid, parentUid: doc.uid } : doc;
      console.log('notes', this._doc);

      return;
    }

    if (action == str.esNoteFinds) {
      this._doc = {};
      delete data.uid;
      data.node = node.visitNotes
      data.text = '';
      data.date = new Date();
      this.doc.note = data;
      return;
    }
    //Init DOC
    this.$doc = {};
    this.doc = {};

    console.log("ACTION", action);

    //keep doc
    if (action == str.editCity) {
      doc.address = {};
      doc.node = node.cities;
      doc.code = this.auth.user.city.code;
    }

    if (action == str.newCore) {
      if (!doc.uid) {
        doc.node = node.cores;
      }

      // doc.city = this.auth.user.city;
    }

    if (action == str.editGroup) {
      // doc.city = this.auth.user.city;
      if (!doc.uid) {
        doc.node = node.groups;
        doc.status = 0;
        doc.pickedDate = new Date();
      }
    }

    if (action == str.editGroupMeet) {
      if (!doc.groupUid) {
        doc.node = node.groupMeeting;
        doc.pickedDate = new Date();
        doc.meet = {};
        doc.groupUid = doc.uid;
        doc.status = 7;
        delete doc.uid;
        delete doc.writeAt;
        delete doc.updateAt;
      } else {

      }
    }

    if (action == str.editCoreMeet) {
      if (!doc.coreUid) {
        doc.node = node.coreMeeting;
        doc.pickedDate = new Date();
        doc.meet = {};
        doc.coreUid = doc.uid;
        doc.status = 7;
        delete doc.uid;
        delete doc.writeAt;
        delete doc.updateAt;
      } else {

      }
    }

    if (action == str.editUser) {
      if (!doc) { doc = {} }
      if (!doc.uid) {
        doc.node = node.users;
        doc.sector = arr.sectorType[0].index;
        this.list = [];
        this.list.push(doc.sector);
        doc.subSector = doc.sector;
        // doc.city = {};
        // doc.city.name = this.auth.user.city.name;
        // doc.city.code = this.auth.user.city.code;
        doc.roles = {};
        doc.active == true;
      }
    }

    if (action == str.editFamily) {
      console.log('onInitEDitFamily');
      doc.node = node.families;
      if (!doc) { doc = {} }

      doc = { deleted: false, gender: 0, kinship: 11 };

      // this.listSearch = [];
    }

    if (action == str.district) {
      if (!doc.uid) {
        doc.node = node.districts;
        // doc.city = this.auth.user.city;
      }
    }

    if (action == str.editLocation) {
      this._doc = {};

      console.log('LOCATION.DOC', doc);

      let data = { ...doc };
      doc = {};
      doc.node = node.address;
      doc.address = { ...data }
      this._doc.address = { ...data };

    }

    if (action == str.editAddress) {
      this._doc = {};
      if (!doc.uid) {
        doc.node = node.address;
        doc.familyUid = familyUid;
        doc.state = arr.states[0].index;
        doc.district = {};
      } else {

      }

      this.listSearch = arr.states;
    }

    if (action == str.editNote) {
      doc.node = node.familyNotes;
      doc.familyUid = familyUid;
      // doc.city = this.auth.user.city;
      //doc.user = this.auth.user;
    }

    if (action == str.editPerson) {
      if (!doc) { doc = {} }
      this._doc = {};
      if (!doc.uid) {
        doc.node = node.people;
        doc.deleted = false;
        doc.familyUid = familyUid;
        doc.kinship = 0;
        doc.addressUid = this.address.uid;
        doc.gender = 0;
        this.setMaxDate(new Date());
        this.setMinDate(new Date(1900, 1, 1));
      } else {
      }
    }

    if (action == str.editServiceRoute) {
      this.doc = {};
      this._doc = {};
      let st: any[] = _.cloneDeep(arr.sectorType);
      // console.log('route', arr.sectorType.findIndex);
      // remove user sector
      if (!doc.uid) {
        //Remove later - just to report
        let person = people.find(i => i.kinship == 11);
        doc.responsible = { uid: person.uid, nis: person.nis, cpf: person.cpf, name: person.name };

        doc.familyUid = familyUid;
        doc.node = this.str.serviceRoute;
        doc.active = true;
        doc.status = 0;
        doc.filename = text.chooseFile;
        //Take off the same subsector

        st.splice(arr.sectorType.findIndex(i => i.index == this.auth.user.subSector), 1);
        doc.sector = arr.sectorType[0].index;

        // doc.city = this.auth.user.city;
      } else {
        // this._doc = { familyUid: doc.familyUid, node: doc.node };
      }


      this._arr = {
        sectorType: st
      }
      // arr.sectorType.splice(arr.sectorType.findIndex(i => i.index == this.auth.user.subSector), 1);

    }

    if (action == str.editServiceVisit) {
      this.doc = {};
      this._doc = {};
      this.nestedList = [];
      // this.valid[0] = true;

      if (!doc.uid) {
        doc.familyUid = familyUid;
        doc.sector = this.auth.user.subSector;
        doc.node = this.str.serviceVisit;
        doc.modality = arr.modalityVisit[0].index;
        doc.pickedDate = new Date();
        doc.active = true;
        doc.status = 0;
        doc.district = this.address.district ? this.address.district : '';
        let person = people.find(i => i.kinship == 11);
        doc.responsible = { name: person.name, uid: person.uid, nis: person.nis, cpf: person.cpf };
        doc.type = arr.visitType[0].index;
        doc.reason = arr.visitReasons[0].index;
        doc.incomeSources = [];
        //doc.person = people[0];
        doc.person = { name: people[0].name, uid: people[0].uid }; //responsible
        // doc.city = this.auth.user.city;
        doc.listUser = [];
        doc.professional = {};
        this._doc.professional = {};
      } else {
        this._doc = { listUser: doc.listUser, professional: {} };

        doc.note = {
          familyUid: doc.familyUid, date: new Date(),
          parentUid: doc.uid, city: this.auth.user.city
        };
        if (this.familyUid) doc.address = this.address;

      }

    }

    if (action == str.editServiceGroup) {
      this.doc = {};
      this._doc = {};

      if (!doc.uid) {
        doc.familyUid = familyUid;
        doc.addressUid = this.address.uid;
        doc.node = this.str.serviceGroup;
        doc.status = 1;
        doc.person = people[0];
        //set map
        let map: { [key: string]: boolean } = {};
        map[doc.person.uid] = true;
        doc.map = map;
        // doc.city = this.auth.user.city;
      } else {


      }

    }

    if (action == str.editServiceBenefit) {
      this.doc = {};
      this._doc = {};
      // this.setNode(node.serviceBenefit);
      if (!doc.uid) {
        let person = people.find(i => i.kinship == 11);
        doc.responsible = { uid: person.uid, nis: person.nis, cpf: person.cpf, name: person.name };
        doc.address = this.address;

        doc.familyUid = familyUid;
        doc.node = this.str.serviceBenefit;
        doc.status = 0;
        doc.requestDate = new Date();
        doc.benefit = arr.benefitType[0].index;
        // doc.city = this.auth.user.city;
      } else {

      }

    }

    if (action == str.editServiceReference) {
      this.doc = {};
      this._doc = {};
      //this.hide = [];
      this.people.forEach(p => {
        let obj = {
          benefitRef: arr.bpcType[0].index,
          deficiencies: [],
          extraInformations: []
        };

        if (!doc[p.uid]) this._doc[p.uid] = obj;
        if (doc[p.uid]) this._doc[p.uid] = _.cloneDeep(doc[p.uid]);

      });

      console.log('REF _DOC', this._doc, 'DOC', doc);


      if (!doc.uid) {
        //Remove later - just to report
        let person = people.find(i => i.kinship == 11);
        doc.responsible = { uid: person.uid, nis: person.nis, cpf: person.cpf, name: person.name };
        doc.address = { district: this.address.district };

        doc.familyUid = familyUid;
        doc.node = this.str.serviceReference;
        this.setMaxDate(new Date());
        this.setMinDate(new Date(1900, 1, 1));
        doc.pickedDate = new Date();
        doc.status = 1;
        doc.extraInformations = [];
        doc.benefit = arr.bpcType[0].index;
        doc.deficiency = arr.deficiencyType[0].index;
        // doc.city = this.auth.user.city;
        this._doc.extraInformations = [];

        //output in report
        doc.person = people.find(i => i.kinship == 11);
        doc.address = { district: this.address.district };
      } else {
        this._doc['extraInformations'] = doc.extraInformations;


        // this.people.forEach(p => {
        //   //this._doc[p.uid] = doc[p.uid];
        // });

      }

    }

    if (action == str.editServiceAttendance) {
      this.doc = {};
      this._doc = {};


      this.nestedList = arr.specInputTypeOne;
      this.nestedList2 = arr.specOutputTypeOne;
      if (!doc.uid) {
        //remove later - just to report
        let person = people.find(i => i.kinship == 11);
        doc.responsible = { uid: person.uid, nis: person.nis, cpf: person.cpf };

        doc.familyUid = familyUid;
        doc.node = this.str.serviceAttendance;
        doc.status = 3;
        doc.pickedDate = new Date();
        doc.person = people[0];
        doc.input = new IO();
        doc.output = new IO();
        doc.extraInput = [];
        doc.extraOutput = [];
        doc.input.detail = arr.specInputTypeOne[0].index;
        doc.output.detail = arr.specOutputTypeOne[0].index;
        doc.input.array = 'specInputTypeOne';
        doc.output.array = 'specOutputTypeOne';
        doc.type = arr.attendanceType[0].index;
        doc.accessForm = arr.accessType[0].index;
        doc.input.type = arr.inputType[0].index;
        doc.output.type = arr.outputType[0].index;
        // doc.city = this.auth.user.city;
        doc.listUser = [];
        doc.professional = {};
        this._doc.professional = {};

      } else {
        this.nestedList = arr[doc.input.array];
        this.nestedList2 = arr[doc.output.array];


      }
      this._doc = { listUser: doc.listUser, professional: {} };
    }

    if (action == str.editServiceSchedule) {
      this.doc = {};;
      // this.setNode(node.serviceSchedule);
      if (!doc.uid) {
        doc.familyUid = familyUid;
        doc.node = this.str.serviceSchedule;
        doc.pickedDate = new Date();
        doc.active = true;
        doc.status = 0;
        doc.index = 0;
        doc.district = this.address.district ? this.address.district : '';
        doc.person = people[0];
        doc.type = arr.scheduleAttendanceType[0].index;
        doc.sector = this.auth.user.subSector;
        //doc.address = this.address;
        // doc.city = this.auth.user.city;
      } else {
        this._doc = {};
        if (this.familyUid) doc.address = this.address;
        // if (!this.address) this.getAddress(doc.familyUid);
      }
    }

    if (action == str.editServiceFsp) {
      this.doc = {};
      this._doc = {};
      this.setNode(node.serviceFsp)
      if (!doc.uid) {
        //Remove later - just to report
        let person = people.find(i => i.kinship == 11);
        doc.responsible = { uid: person.uid, nis: person.nis, cpf: person.cpf, name: person.name };
        doc.address = { district: this.address.district };

        let user = this.auth.user;
        let _doc = this.referencesList.find(i => i.status == 1
          && i.user.subSector == user.subSector);
        if (_doc) doc.referenceUid = _doc.uid;
        doc.familyUid = familyUid;
        doc.person = people.find(i => i.kinship == 11);
        doc.node = this.str.serviceFsp;
        doc.status = 1;
        doc.pickedDate = new Date();
        doc.active = true;
        doc.sector = user.subSector;
        // doc.city = user.city;
      } else {
        this.minDate = new Date(JSON.parse(JSON.stringify(doc.pickedDate)));
        this._doc.note = { familyUid: doc.familyUid, parentUid: doc.uid };
        //this.nestedList = [];
      }
    }

    if (action == str.editServiceHc) {
      this.doc = {};
      this._doc = {};
      // this._doc.professional = {};

      // this.setNode(node.serviceHc)

      if (!doc.uid) {
        //Remove later - just to report
        let person = people.find(i => i.kinship == 11);
        doc.responsible = { uid: person.uid, nis: person.nis, cpf: person.cpf, name: person.name };
        doc.address = { district: this.address.district };

        let user = this.auth.user;
        doc.familyUid = familyUid;
        doc.active = true;
        doc.address = { uid: this.address.uid };
        // doc.sector = user.subSector;
        doc.node = this.str.serviceHc;
        doc.status = 1;
        doc.accessType = arr.accessType[0].index;

        this.sortPersonToHc();
        doc.person = this.sortPeople[0];

        // active this filter after
        // this.setFilterList(action, doc.person.uid);
        // console.log('filterList', this.filterList);
        // doc.target = this.filterList[0][0].index;
        // doc.rangeAge = this.filterList[1][0].index;

        doc.target = arr.targetHc[0].index;
        doc.rangeAge = arr.rangeAgeHc[0].index;
        // doc.city = user.city;
        // doc.note = this.getMonth() + ' - ' + this.text.visitFail;
        // doc.plan = {
        //   rangeAge: arr.rangeAgePlan[0].index,
        //   activities: [],
        //   month: arr.monthOfYear[0].index
        // };
        doc.professional = {};
        this._doc.professional = {};
      } else {
        if (this.familyUid) {
          this.sortPersonToHc();
          // this.setFilterList(action, doc.person.uid);
        } else {
          // here all filters are nothing happen
          this.getPerson(doc.person.uid);
          // this.filterList = [arr.targetHc, arr.rangeAgeHc];
          // console.log(this.filterList, 'filterList');


        }
        this._doc.note = { familyUid: familyUid, parentUid: doc.uid };
      }

    }

    if (action == str.editServiceOld) {
      this.doc = {};
      if (!doc.uid) {
        doc.familyUid = familyUid;
        doc.node = this.str.serviceOld;
        doc.status = 1;
        doc.type = arr.serviceType[0].index;
        this.sortOldPerson();
        doc.person = this.sortPeople[0];
        // doc.city = this.auth.user.city;
      } else {
        this.sortOldPerson();
        this._doc = {};
      }
    }

    if (action == str.editServiceNotification) {
      this.doc = {};
      if (!doc.uid) {
        doc.node = this.str.serviceNotification;
        this.setMinDate(new Date());
        doc.pickedDate = new Date();
        doc.familyUid = familyUid;
        doc.status = 7;
        // doc.city = this.auth.user.city;
      } else {
        this._doc = {};
      }
    }

    if (action == str.editServiceTerm) {
      this.doc = {};
      if (!doc.uid) {
        doc.familyUid = familyUid;
        doc.node = this.str.serviceTerm;
        doc.status = 7;
        // doc.city = this.auth.user.city;
      } else {
        this._doc = {};
      }
    }

    if (action == str.editServiceDemand) {
      this.doc = {};
      if (!doc.uid) {
        doc.familyUid = familyUid;
        doc.node = this.str.serviceDemand;
        doc.status = 0;
        doc.active = true;
        doc.person = people[0];
        doc.pickedDate = new Date();
        doc.input = new IO();
        doc.output = new IO();
        doc.input.type = arr.childInputType[0].index;
        doc.output.type = arr.childOutputType[0].index;
        doc.rangeAge = arr.childRangeAge[0].index;
        doc.gender = arr.gender[0].index;
        doc.input.detail = arr.childSpecInputType[0].index;
        doc.input.array = 'childSpecInputType';
        doc.output.detail = arr.childSpecOutputType[0].index;
        doc.output.array = 'childSpecOutputType';
        // doc.city = this.auth.user.city;
      } else {
        this._doc = {};
      }
    }

    if (action == str.editServiceAcessuas) {
      this.doc = {};
      if (!doc.uid) {
        doc.familyUid = familyUid;
        // doc.city = this.auth.user.city;
        doc.node = this.str.serviceAcessuas;
        doc.status = 1;
        doc.person = people[0];
        doc.extraInformations = [];
        doc.income = arr.incomeType[0].index;
        doc.local = arr.locationType[0].index;
        doc.shift = arr.shiftType[0].index;
        doc.schooling = arr.schoolingType[0].index;
        doc.situation = arr.situationType[0].index;
        doc.accessForm = arr.accessType[0].index;
      } else {
        this._doc = {};
      }

    }

    if (action == str.editServiceAepeti) {
      this.doc = {};
      this._doc = {};
      if (!doc.uid) {
        //Remove later - just to report
        let person = people.find(i => i.kinship == 11);
        doc.responsible = { uid: person.uid, nis: person.nis, cpf: person.cpf, name: person.name };
        doc.address = { district: this.address.district };

        doc.familyUid = familyUid;
        doc.node = this.str.serviceAepeti;
        doc.status = 1;
        doc.person = people[0];
        doc.accessForm = arr.accessFormAEPETI[0].index;
        doc.register = arr.yesNoType[0].index;
        doc.include = arr.yesNoType[0].index;
        doc.workActivity = arr.workActivityType[0].index;
        doc.activity = arr.activityType[0].index;
        doc.period = arr.periodType[0].index;
        doc.schooling = arr.schoolingType[0].index;
        doc.shiftWork = arr.shiftWork[0].index;
        doc.school = {};
      } else {
      }

    }

    if (action == str.editServiceCondition) {
      this.doc = {};
      if (!doc.uid) {
        //Remove later - just to report
        let person = people.find(i => i.kinship == 11);
        doc.responsible = { uid: person.uid, nis: person.nis, cpf: person.cpf, name: person.name };
        doc.address = { district: this.address.district };

        doc.familyUid = familyUid;
        // doc.city = this.auth.user.city;
        doc.node = this.str.serviceCondition;
        doc.status = 0;
        doc.person = people[0];
        doc.type = arr.conditionType[0].index;
        doc.resource = arr.resourceType[0].index;
        doc.effect = arr.yesNoType[0].index;
        doc.local = {};
      } else {
        this._doc = {};
      }
    }

    if (action == str.editServiceComplaint) {
      this.doc = {};
      this._doc = {};
      if (!doc.uid) {
        //Remove later - just to report
        let person = people.find(i => i.kinship == 11);
        doc.responsible = { uid: person.uid, nis: person.nis, cpf: person.cpf, name: person.name };

        // doc.city = this.auth.user.city;
        doc.familyUid = familyUid;
        doc.node = this.str.serviceComplaint;
        doc.status = 0;
      } else {

      }
    }

    if (action == str.editServicePendency) {
      this.doc = {};
      if (!doc.uid) {
        //Remove later - just to report
        let person = people.find(i => i.kinship == 11);
        doc.responsible = { uid: person.uid, nis: person.nis, cpf: person.cpf, name: person.name };

        // doc.city = this.auth.user.city;
        doc.familyUid = familyUid;
        doc.node = this.str.servicePendency;
        doc.status = 0;
      } else {
        this._doc = {};
      }
    }

    if (action == str.editServiceInterview) {
      this.doc = {};
      this._doc = {};
      if (!doc.uid) {
        //Remove later - just to report
        let person = people.find(i => i.kinship == 11);
        doc.responsible = { uid: person.uid, nis: person.nis, cpf: person.cpf, name: person.name };

        doc.familyUid = familyUid;
        // doc.city = this.auth.user.city;
        doc.node = this.str.serviceInterview;
        doc.pickedDate = new Date();
        doc.status = 7;
        doc.type = arr.interviewType[0].index;
        doc.modality = arr.modalityType[0].index;
        doc.goal = arr.goalType[0].index;
        doc.professional = {};
        this._doc.professional = {};
      } else {

      }

    }
    if (action == str.editServiceFpgb) {
      this.doc = {};
      this._doc = { professional: {} };
      if (!doc.uid) {
        //Remove later
        let person = people.find(i => i.kinship == 11);
        doc.responsible = { uid: person.uid, nis: person.nis, cpf: person.cpf, name: person.name };

        doc.familyUid = familyUid;
        // doc.city = this.auth.user.city;
        doc.node = this.str.serviceFpgb;
        doc.status = 0;
        doc.type = arr.requestType[0].index;
        doc.professional = {};
      } else {

      }

    }
    if (action == str.editServiceMilk) {
      this.doc = {};
      this._doc = {};
      if (!doc.uid) {
        let person = people.find(i => i.kinship == 11);
        doc.responsible = { uid: person.uid, nis: person.nis, cpf: person.cpf, name: person.name };

        doc.familyUid = familyUid;
        // doc.city = this.auth.user.city;
        doc.node = this.str.serviceMilk;
        doc.status = 1;
        doc.pickedDate = new Date();
        this.sortPersonMilk();
        doc.person = this.sortPeople[0];
        //add kinship
        if (doc.person) doc.kinship = doc.person.kinship;
        doc.school = {};
      } else {
        this.sortPersonMilk();
      }

    }
    if (action == str.editServiceScfv) {
      // initscfv
      this.doc = {};
      this._doc = {};

      if (!doc.uid) {
        doc.familyUid = familyUid;
        // doc.city = this.auth.user.city;
        doc.node = this.str.serviceSCFV;
        doc.status = 1;
        doc.person = people[0];
        let person = people.find(i => i.kinship == 11);
        doc.responsible = { uid: person.uid, nis: person.nis, cpf: person.cpf };
        //set map
        let map: { [key: string]: boolean } = {};
        map[doc.person.uid] = true;
        doc.map = map;
        doc.shift = arr.shiftType[0].index;
        doc.situation = arr.situationType[0].index;
        doc.accessForm = arr.accessForm[0].index;
      } else {

      }
    }

    //find person in sortPeople
    if (this.sortPeople && doc.person) {
      this.validPerson = this.sortPeople.findIndex(i => i.uid == doc.person.uid) >= 0;
      console.log(this.validPerson, 'validPerson');
    }

    if (this._doc) {
      this.$_doc = _.cloneDeep(this._doc);
    }
    this.$doc = _.cloneDeep(doc);
    // console.log('DOC', doc);
    // console.log(this.doc, this.$doc);

    this.doc = _.cloneDeep(doc);
  }

  //Calculate how much days pass by a date to other
  calcDaysElapsed(date: Date, date2: Date): number {
    let timeDiff = Math.abs(date.getTime() - date2.getTime())
    console.log('DaysElapsed', Math.floor((timeDiff / (1000 * 3600 * 24))));
    return Math.floor((timeDiff / (1000 * 3600 * 24)))
  }

  //add data to a map
  updateMap(uid: string, map: { [key: string]: boolean }): void {
    if (!map[uid]) {
      map[uid] = true;
    } else {
      map[uid] = false;
    }
    console.log(map);

  }

  hasRoles(roles: Roles, value: string): boolean {
    return roles[value];
  }
  // upDate roles for one user
  updateRoles(role: string, checked: boolean): void {
    // console.log(event);
    if (checked) {
      this.doc.roles[role] = true;
    } else {
      delete this.doc.roles[role];
    }
    console.log(this.doc.roles);

  }

  //Adding data to a array in a list of Checkbox
  updateArr2(data: any, array: any[]): void {
    console.log(data, array);

    if (!array) array = [];
    if (data) {
      let index: number = array.findIndex(i => i == data.index);
      if (index >= 0) {
        array.splice(index, 1);
      } else {
        array.push(data.index);
      }
      console.log(array, this._doc);
      data = '';

      this.isRequired = array ? true : false;

      console.log(array, this._doc);
      data = '';

      this.isRequired = array ? true : false;
    }

  }

  // Adding data to a array in a list of Checkbox
  updateArr(index: number, array: any[]): void {
    console.log(index);

    if (!array) array = [];
    if (index >= 0) {
      // let findindex: number = ;
      let i = array.findIndex(i => i == index);
      if (i >= 0) {
        array.splice(i, 1);
      } else {
        array.push(index);
      }
      console.log(array, this._doc);
      // i = '';

      // this.isRequired = array ? true : false;
    }

  }

  // Adding data to a array in a list of Checkbox
  updateArrByUid(doc: any, array: any[]): void {
    if (!array) array = [];
    if (doc && doc.uid) {
      let findindex: number = array.findIndex(i => i.uid == doc.uid);
      if (findindex >= 0) {
        array.splice(findindex, 1);
      } else {
        array.push(doc);
      }
      console.log(array, this._doc);
      // i = '';
    }

  }

  // setValidByPos(pos: number, b: boolean): void {
  //   this.valid[pos] = b;
  // }

  showLog(value: any): void {
    console.log("showLOG: ", value, this.doc, this._doc);
  }

  //Show a field after select a option in a Select
  showOption(index: number, option: string): void {

    let show: boolean = index == 1 ? true : false;
    let str = this.str;
    let doc = this.doc;
    let arr = this.arr;

    if (option == str.registerAepeti) {
      doc.frequency = 0;
      doc.schoolPeriod = 0;
    }

    if (option == str.socialAepeti) {
      doc.socialProgram = 0;
    }

    if (option == str.activityAepeti) {
      doc.moneyDestiny = 0;
      doc.incomeType = 0;
    }
  }

  //Show a field after select a option in a Select // IO object
  showSpecIn(index: number, array: any[], option: string) {
    this.doc.extraInput = []
    let doc = this.doc
    let arr = this.arr
    let str = this.str
    doc.hasInputOne = false;
    doc.hasInputTwo = false;
    this.doc.extraInput = [];

    if (option == str.attendance) {
      if (index == 0) {
        this.nestedList = arr.specInputTypeOne;
        doc.input.array = 'specInputTypeOne';
      }
      if (index == 1) {
        this.nestedList = arr.specInputTypeTwo;
        doc.input.array = 'specInputTypeTwo';
      }

      doc.input.detail = parseInt(this.nestedList[0].index);
    }

    if (option == str.demand) {
      if (index == 0) {
        doc.hasInputOne = true;
        doc.input.array = 'childSpecInputType';
        doc.input.detail = arr.childSpecInputType[0].index;
      }
    }

  }

  //Show a field after select a option in a Select // IO object
  showSpecOut(index: number, array: string[], option: string) {
    this.doc.extraOutput = [];
    let doc = this.doc;
    let arr = this.arr;
    let str = this.str;

    if (option == str.attendance) {

      if (index == 0) {
        this.nestedList2 = arr.specOutputTypeOne;
        doc.output.array = 'specOutputTypeOne';
      }
      if (index == 1) {
        this.nestedList2 = arr.specOutputTypeTwo;
        doc.output.array = 'specOutputTypeTwo';
      }
      if (index == 2) this.nestedList2 = [];
      if (index == 3) {
        this.nestedList2 = arr.specOutputTypeThree;
        doc.output.array = 'specOutputTypeThree';
      }
      if (index == 4) {
        this.nestedList2 = arr.specOutputTypeFour;
        doc.output.array = 'specOutputTypeFour';
      }
      if (this.nestedList2.length) doc.output.detail = parseInt(this.nestedList2[0].index);
    }

    if (option == str.demand) {
      if (index == 0) {
        doc.output.array = 'childSpecOutputType';
        doc.output.detail = arr.childSpecOutputType[0].index;
      }
      if (index == 2) {
        doc.output.array = 'childSpecOutputTypeTwo';
        doc.output.detail = arr.childSpecOutputTypeTwo[0].index;
      }
    }
  }

  //Set location in Document
  onChoseLocation(event: any): void {
    this.alertFeedback(this.text.newLocationChose);
    this._doc.address.coords._lat = event.coords.lat;
    this._doc.address.coords._long = event.coords.lng;

    console.log(this.address);

  }

  toDate(date: any): Date {
    return new Date(JSON.parse(JSON.stringify(date)));
  }

  //Calc days of life
  calcMonths(stringDate: Date): number {
    const date = new Date(JSON.parse(JSON.stringify(stringDate)));
    var timeDiff = Math.abs(Date.now() - date.getTime());
    let months = Math.floor((timeDiff / (1000 * 3600 * 24) / 30.4));
    return months;
  }

  //Calc days of life
  calcDays(stringDate: Date): number {
    const date = new Date(JSON.parse(JSON.stringify(stringDate)));
    var timeDiff = Math.abs(Date.now() - date.getTime());
    let days = Math.floor((timeDiff / (1000 * 3600 * 24)));
    return days;
  }

  //Calc age
  calcAge(s: string): number {
    const date = new Date(JSON.parse(s));
    var timeDiff = Math.abs(Date.now() - date.getTime());
    let age = Math.floor((timeDiff / (1000 * 3600 * 24)) / 365);
    return age;
  }

  //Sort people to Milk service
  sortPersonMilk(): void {
    this.sortPeople = [];
    this.people.forEach((item) => {
      let age: number = this.calcAge(JSON.stringify(item.birthday));
      if (age >= 2 && age <= 64) {
        this.sortPeople.push(item);
      }
    });
    if (this.sortPeople.length == 0) {
      this.alertFeedback(this.text.haveNoneAble);
    }
  }

  //Sort old people
  sortOldPerson(): void {
    this.sortPeople = [];
    this.people.forEach((item) => {
      if (this.calcAge(JSON.stringify(item.birthday)) >= 60) {
        this.sortPeople.push(item);
      }
    });
    if (this.sortPeople.length == 0) {
      // this.sortPeople.push({} as Person);
      this.alertFeedback(this.text.haveNoneAble);
    }
  }

  //Sort people to Happy child
  sortPersonToHc(): void {
    this.sortPeople = [];
    this.people.forEach((item) => {
      // let ref = this.referencesList.find(i => i.user.subSector == this.auth.user.subSector
      //   && i.status == 1);
      if (this.calcAge(JSON.stringify(item.birthday)) <= 6 || item.gender == 1) {
        this.sortPeople.push(item);
      }
    });
    if (this.sortPeople.length == 0) {
      this.alertFeedback(this.text.haveNoneAble);
    }
  }

  //Used to checked a checkbox
  indexOf2(data: any, array: any): boolean {
    //Initialize var requiqued to writed data
    this.isRequired = array.length < 1 ? true : false;
    let check = array.findIndex(i => i == data.index) != -1;
    // console.log('indexOf', check, data, array);

    return check;

  }

  //Used to checked a checkbox
  indexOf(data: string, array: string): boolean {
    //Initialize var requiqued to writed data
    this.isRequired = array.length < 1 ? true : false
    return array.indexOf(data) != -1
  }

  //Set person in Document
  setPersonByIndex2(person: Person): void {
    console.log('person', person);

    this.doc.person = person;
    // let person: any = { uid: doc.person.uid, name: doc.person.name };
    // doc.person = person;
    console.log(this.doc);

  }

  setPersonByIndex(index: number, people: Person[], act?: string): void {
    let person = people[index];
    this.doc.person = person;
    console.log("person", person);

    if (act) {
      let str = this.str;
      if (person) this.checkNis(person);

      let doc = this.doc;
      let arr: any;

      if (act == str.hc) {
        this.setFilterList(act, person.uid);
        this.doc.target = this.filterList[0][0].index;
        this.doc.rangeAge = this.filterList[1][0].index;
        arr = this.hcList;
      }
      if (act == str.old) arr = this.oldServiceList;
      if (act == str.aepeti) arr = this.aepetiList;
      if (act == str.acessuas) arr = this.acessuasList;
      if (act == str.milk) arr = this.milkList;
      this.checkStatusOnService(doc, arr);
      // }
    }
  }

  //Set a file in Document
  setFile(prop: string, file: File): void {
    this.doc[prop] = file;
    console.log(this.doc);
    // this.isOk = false;
    if (!file.type.match("image.*")) {
      this.alertFeedback(this.text.selectImage);
    } else if (file.size >= 2 * 1024 * 1024) {
      this.alertFeedback(this.text.imageSize);
    } else {
      // this.isOk = true;
    }
  }

  //Set a file in Document
  detectFiles(file: File): void {
    this.doc.file = file;
    console.log(file);
    if (!file.type.match("image.*")) {
      this.alertFeedback(this.text.selectImage);
      this.isOk = false;
    } else if (file.size >= 2 * 1024 * 1024) {
      this.alertFeedback(this.text.imageSize);
      this.isOk = false;
    } else {
      this.isOk = true;
      this.doc.filename = file.name;
    }

  }

  // indentify family search type CPF NIS Name
  setSearchFamily(query: string): void {
    this.query = query;
    // this.feedback = null;
    if (Number(query)) {
      let text = this.text;
      let len: number = query.length;
      if (len <= 10) {
        this.feedback = text.plus + ' ' + (11 - len) + ': ' + text.cpf + '/' + text.nis;
      }
      if (len == 11) {
        this.feedback = text.cpf + '/' + text.nis;
      }
    }
  }

  //Print All
  print(): void {
    window.print();
  }

  //show visit plan of hc
  sheetHcPlan(doc: Service, address: Address, template: TemplateRef<any>) {
    if (this.modalRef) this.modalRef.hide();
    doc.address = address;
    doc.time = new Date();
    this.getCity(doc.city.code, false, true);
    console.log(doc);

    this.docPrint = doc;
    this.setModalPrint(template);
  }

  //report people groups(groups and cores)
  sheetListPeopleGroup(doc: any, node: string, template: TemplateRef<any>): void {
    if (this.modalRef) this.modalRef.hide();
    doc.time = new Date();
    this.docPrint = doc;
    console.log(this.docPrint, node);
    this.people = [];
    this.getCity(doc.city.code, false, true).then(() => {
    });

    this.pages = [];
    // let node = this.node.groups;
    this.listPeopleData(doc.uid, node).then(() => {
      let members = this.members;
      console.log(members);

      if (this.hasMembers(members)) {
        this.setModalPrint(template);
        this.listPeople(members).then(() => {
          this.setAddresToPeople().then(() => {
            let page = [];
            let pageNum = 1;

            this.people.forEach((p, index) => {
              page.push(p);

              if (pageNum == 1 && page.length == 22
                || pageNum > 1 && page.length == 27
                || index == this.people.length - 1) {
                this.pages.push(_.cloneDeep(page));
                page = [];
                pageNum++;
              }
            });
            console.log(this.pages);
          });
        });
      } else {
        this.alertFeedback(this.text.noMembers);
      }
    });
  }

  //Sheet demand/happy child
  sheetDemand(doc: Service, address: Address, template: TemplateRef<any>): void {
    if (this.modalRef) this.modalRef.hide();
    doc.time = new Date();
    doc.address = address;
    this.docPrint = doc;
    this.getCity(doc.city.code, false, true).then(() => {
    });
    this.setModalPrint(template);
  }

  //Sheet family
  sheetFamily(template: TemplateRef<any>) {
    this.docPrint = {};
    let code = this.auth.user.city.code;
    this.getCity(code, false, true).then(() => {
      this.docPrint.time = new Date();
      this.setModalPrint(template);
    });
  }

  //Set sheet visit
  sheetVisit(doc: Service, address: Address, template: TemplateRef<any>): void {
    if (this.modalRef) this.modalRef.hide();
    doc.time = new Date();
    doc.address = address;
    this.docPrint = doc;
    this.getCity(doc.city.code, false, true).then(() => {
      console.log('print', this.docPrint);

      this.setModalPrint(template);
    });
    // this.router.navigate(['/print/sheet-visit']);
  }

  //generate group report
  generateGroupReport(doc: any): void {
    this.time = new Date;
    doc.page = this.text.group;
    this.doc = doc;
    this.people = [];
    this.meeting = [];
    let node = this.node.groups;
    // this.listPeople(doc.uid, node).then(() => {
    //   for (let i = 0; i < 30; i++) {
    //     this.people.push(_.cloneDeep(this.people[0]));
    //   }
    //   this.listMeetingReport(doc, node).then(() => {
    //     this.generateReportService();
    //     this.router.navigate(['/report-meeting']);
    //   });
    // });
  }
  //Generate user meeting list
  generateUserMeeting(valid: boolean, doc: any): void {

    doc = {};
    this.doc.title = 'asdasdas';
    this.doc.amount = 40;
    this.doc.date = new Date();

    if (true) {
      let amount = parseInt(this.doc.amount) + 1;
      this.pages = [];
      let page = [];
      if (amount > 13) {
        for (let i = 0; i < 13; i++) {
          page.push('line');
          amount--;
        }
        this.pages.push(page);
      }

      let countPages = Math.ceil(amount / 16);
      for (let i = 0; i < countPages; i++) {
        page = [];
        while (amount > 0) {
          amount--;
          page.push('line - 2');
          if (page.length == 16 || amount == 0) {
            this.pages.push(page);
            break;
          }
        }
      }
      console.log('count', countPages, 'generate', this.pages);

      if (this.modalRef) this.modalRef.hide();
      this.router.navigate(['/meeting-list']);
    }
  }

  //getMonth of the year
  getMonth(date?: Date): string {
    if (!date) date = new Date();
    return this.arr.monthOfYear[date.getMonth()].text;
  }

  // add notes - sysLog
  saveNote(doc: any, act: string, checked?: boolean) {
    let str = this.str;
    // act.saveNote(service, str.editServiceHc, $event.target.checked
    console.log(checked, new Date().getMonth());
    let month = this.getMonth();
    if (act == str.editServiceHc && checked) {
      let note = this.setNote(doc, this.text.visitComplete, month);
      console.log(note);

      this.crud.writeNoteService(note, this.node.hcNotes).then(() => {

      });
      doc.note = { text: note.text, month: this.arr.monthOfYear.findIndex(i => i.text == note.month) };
      this.writeDoc(str.editServiceHc, doc);
    }
  }

  setNote(doc: any, text: string, month?: string): any {
    let user = {
      fullName: this.auth.user.fullName, uid: this.auth.user.uid,
      subSector: this.auth.user.subSector
    };

    return {
      familyUid: doc.familyUid,
      city: this.auth.user.city,
      parentUid: doc.uid,
      log: true,
      month: month,
      updateAt: new Date(),
      text: text,
      user: user
    };
  }

  filterArray(array: any[], type: string, id: string) {
    let str = this.str;
    console.log('array', array);
    let results: any[] = [];
    results = results.concat(array);

    if (type == str.rangeAgeHc) {

      let person = this.people.find(i => i.uid == id);
      let months = this.calcMonths(person.birthday);
      console.log('months', months);

      if (months <= 36) results.splice(1, 1);
      if (months > 37 && months <= 72) results.splice(0, 1);
      console.log(results);
      return results;

    }

    if (type == str.targetHc) {
      console.log('person.gender', this.people, this.doc.person);
      let person = this.people.find(i => i.uid == id);

      if (person.gender != 1) results.splice(results.findIndex(i => i.index == 0), 1);
      let ref = this.referencesList.find(i => i.user.subSector == this.auth.user.subSector
        && i.status == 1);

      if (ref) {

        person = ref[id];
        if (person) {
          if (person.benefitRef > 0) {
            results.splice(results.findIndex(i => i.index == 2), 1);
            results.splice(results.findIndex(i => i.index == 4), 1);
          }
          if (person.extraInformations.length && person.extraInformations.findIndex(i => i == 3) > 0) {
            results.splice(results.findIndex(i => i.index == 3), 1);
            let index = results.findIndex(i => i.index == 4);
            if (index >= 0) results.splice(index, 1);
          }
        }

      }

      console.log('array', results);
      return results;
    }

  }

  setFilterList(action: string, id: string): void {
    let filterList: any[] = [];
    let arr = this.arr;
    let str = this.str;
    if (action == str.editServiceHc || action == str.hc) {
      console.log('here');

      filterList[0] = this.filterArray(arr.targetHc, this.str.targetHc, id);
      filterList[1] = this.filterArray(arr.rangeAgeHc, this.str.rangeAgeHc, id);
    }
    console.log(JSON.stringify(filterList));

    this.filterList = filterList;
  }
  //Utilitaries ------------------------------------------------------ END

  //setState to address
  setStateOnAddress(val: string): void {
    let index = this.arr.states.findIndex(i => val == i.id);
    this._doc.state = index;
  }

  checkStateOnAddress(val: string, pos: number): boolean {
    let index = this.arr.states.findIndex(i => val == i.id);

    let check = index >= 0 ? true : false;
    this.valid[pos] = check;
    console.log('checkStateOnAddress', val, index, this.doc);

    return check;
  }

  hasIndexOf(val: string, arr: any[], prop: string): boolean {
    return arr.findIndex(i => val.toLowerCase() == String(i[prop]).toLowerCase()) > -1;
  }

  showEvent(event: any) {
    console.log(event);

  }

  parseInt(value: any): number {
    return parseInt(value);
  }

  //Define The Kind of Report
  setReportType(type: string): void {
    let text = this.text;
    let arr = this.arr;

    if (type == text.analytical) {
      this.action = text.analytical;
      this.list = arr.analyticalReportType;
    }
    if (type == text.synthetic) {
      this.action = text.synthetic;
      this.list = arr.syntheticReportType;

    }
    let doc: any = {};
    doc.startDate = new Date(2014, 1, 1);
    doc.endDate = new Date();
    doc.report = this.list[0];
    this.doc = doc;
  }
  //generate group report
  generateReportService(): void {
    let text = this.text;
    let pages = [];
    let page = [];

    this.meeting.forEach((m, i: number) => {
      page.push(m);
      if (page.length == 9) {
        pages.push(page);
        page = [];
      }

      if ((i + 1) == this.meeting.length && this.meeting.length != 9) {
        console.log('meet-', m);
        pages.push(page);
      }
    });

    console.log('lengt', pages);

    this.pages = [];
    page = [];
    pages.forEach((meeting: any, i: number) => {
      let hasDates: boolean = false;
      this.people.forEach((p) => {

        p.frequency = [];
        if (!hasDates) p.dates = [];

        for (let m of meeting) {
          if (!hasDates) p.dates.push(m.pickedDate);
          if (m.meet[p.uid] === true) {
            p.frequency.push(text.yes);
          } else if (m.meet[p.uid] === false) {
            p.frequency.push(text.no);
          } else {
            p.frequency.push(text.x);
          }
        }

        hasDates = true;
        page.push(_.cloneDeep(p));

        if ((this.people.indexOf(p) + 1) == this.people.length) {
          this.pages.push(page);
          page = [];
        }
      });

    });
    console.log('pages', this.pages);

  }

  //Login actions --------------------------------------------------- START
  signInWithEmailAndPass(): void {
    //Impplement
    console.log("DATAS", this.doc);
  }

  signOut(): void {
    this.auth.signOut();
    this.unsubList.forEach(unsub => {
      unsub();
    });
  }

  signInWithGoogle() {
    this.auth.googleSignIn().then(() => {
      //sucess signIn
    });
  }
  //Login actions --------------------------------------------------- END

  //Menu manage - Vini`s code
  openMenu() {
    if (this.openMenuInt == 0) {
      (document.querySelector('.navbar-sv') as HTMLElement).style.height = '400px';
      (document.querySelector('.content-sv') as HTMLElement).style.marginTop = '400px';
      (document.querySelector('.navbtnL-sv') as HTMLElement).style.display = 'block';
      this.openMenuInt = 1
    } else {
      (document.querySelector('.navbar-sv') as HTMLElement).style.height = '90px';
      (document.querySelector('.content-sv') as HTMLElement).style.marginTop = '90px';
      (document.querySelector('.navbtnL-sv') as HTMLElement).style.display = 'none';
      this.openMenuInt = 0
    }
  }

  //Getting page params
  getPageParams(route: ActivatedRoute, action: string): void {
    route.params.subscribe(params => {
      let str = this.str;

      let uid = params['uid'];
      this.familyUid = uid;

      if (action == str.familyDetail
        || action == str.feedback) {
        this.onInitFamilyDetail();
      }

      if (action == str.familyDataList) {
        this.routerToFamily(uid);
      }

    });
  }

  logStatus() {
    console.log("onACtion", this.onAction, "STATUS_DOC", this._doc);
  }

  onSubmit(valid: boolean, action?: string, merge?: boolean): void {
    if (valid) {
      //----
      this.onConfirm(action, merge);
    } else {
      this.alertFeedback(this.text.noneAction);
    }
  }

  onOk(action: string): void {
    this.writeDoc(action, this.doc2);
  }

  //Modal manager ------------------------------- START
  onConfirm(action?: string, merge?: boolean): void {
    if (this.modalRef) { this.modalRef.hide(); }
    if (action != null) {
      this.merge = merge;
    }

    console.log("action", action, "onACtion", this.onAction, "Action", action, "MERGE", merge, "STATUS_DOC", this._doc);
    console.log('my doc', this.doc);
    console.log('my_doc', this._doc);

    this.setDoc(action);
  }

  onCancel(): void {
    console.log('cancel');
    console.log(this.valid);

    if (this.modalRef) { this.modalRef.hide(); }
  }

  confirmDialog(template: TemplateRef<any>): void {
    this.dialogRef = this.modalService.show(template);
  }

  onConfirmDialog(): void {
    console.log('onConfirmDialog', this.doc, this.onAction);

    this.setDoc(this.onAction);

    //move to action service
    this.dialogRef.hide()
    if (this.modalRef) { this.modalRef.hide(); }
  }

  onCancelDialog(): void {
    this.dialogRef.hide()
  }

  //modalFilter
  modalFilter(action: string, doc: any): boolean {
    let str = this.str;
    let text = this.text;
    console.log("Filter modal");

    if (!doc) {
      if (action == str.editServiceScfv || action == str.editServiceHc) {
        if (this.referencesList.findIndex(i => i.status == 1
          && i.user.subSector == 6) < 0) {
          this.alertFeedback(text.needHaveRefOnCras);
          return true;
        }
      }
      //c
      if (action == str.editServiceReference) {
        if (this.referencesList.findIndex(i => i.status == 1
          && i.user.subSector == this.auth.user.subSector) >= 0) {
          this.alertFeedback(text.haveOne + text.reference);
          return true;
        }
      }

      if (action == str.editServiceFsp) {
        if (this.referencesList.findIndex(i => i.status == 1
          && i.user.subSector == this.auth.user.subSector) < 0) {
          this.alertFeedback(text.needAddRerefence);
          console.log("REF----------------------");
          return true;

        }
        if (this.fspList.findIndex(i => i.status == 1
          && i.user.subSector == this.auth.user.subSector) >= 0) {
          this.alertFeedback(text.haveOne + text.fsp);
          console.log("FSP----------------------");

          return true;
        }
      }
    } else {
      // check subSector
      console.log(action);

      if (doc.user.subSector != this.auth.user.subSector
        && action != str.editServiceRoute
        && action != str.editPerson
        && action != str.editNote
        && action != str.editLocation
        && action != str.editUser
        && action != str.editGroupMeet
        && action != str.editCoreMeet
        && action != str.editAddress) {
        this.alertFeedback(this.text.differentSubSector);
        return true;
      }
    }
    return false;
  }

  // Get list of people to groups
  getMembersGroup(doc: any, uid: string, node: string, template?: TemplateRef<any>): void {
    this.people = [];
    this.listPeopleData(uid, node).then(() => {
      let members = this.members;
      console.log(`member`, members);

      if (this.hasMembers(members)) {
        this.setModalRef(template);
        this.listPeople(members).then(() => {
          // this.setModalRef(template);
          this.setMeetData(doc);
        });
      } else {
        this.alertFeedback(this.text.noMembers);
      }
    });
  }

  setListUser(template?: TemplateRef<any>, defaultValue?: boolean): void {
    console.log('get user', this.auth.user.city.code, this.auth.user.subSector);

    this.getUsers(this.auth.user.city.code, this.auth.user.subSector).then(() => {

      if (!defaultValue) this.listUser = [];
      this.users.forEach((user: User, index: number) => {
        if (user.proCode) user.fullName += ' - ' + user.email;
        if (index <= 6) {
          this.listUser.push(user);
        }
      });
      if (template) this.setModalRef(template);
    });
  }

  //Call print modal
  private setModalPrint(template: TemplateRef<any>) {
    this.modalPrint = this.modalService.show(template, Object.assign({}, { class: 'modal-lg' }));
  }

  //Call standart modal
  private setModalRef(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  setValid(): void {
    this.valid = new Array(10).fill(false);
  }

  checkValid(act: string) {
    let str = this.str;
    let doc = this.doc;
    if (act == str.editPerson && doc.uid) {
      this.valid[0] = doc.cpf.length == 11 || doc.nis.length == 11 ? true : false;
      this.valid[1] = doc.cpf.length == 11 || doc.nis.length == 11 ? true : false;
    }
  }

  checkAddress(): boolean {
    let b = this.address && this.address.uid;
    if (!b) this.alertFeedback(this.text.noAddress);
    return b;
  }

  //Open a modals
  openModal(template: TemplateRef<any>, action: string, doc: any) {
    this.setValid();

    this.hide = [];
    if (this.modalFilter(action, doc)) {
      return;
    }
    //set sortPeople
    this.sortPeople = [];
    //setfeedback
    this.feedback = null;
    // setisOk
    // this.changeStatus = true;
    this.isOk = true;
    let str = this.str;

    //Set MERGE
    this.merge = false;

    console.log("DOC before ", doc, action);

    this.initDoc(doc, action);
    console.log("DOC AFTER ", this.doc);

    //setting person info
    if (this.doc.uid && this.doc.person && this.people && doc && doc.person) {
      let person = this.people.find(i => i.uid == doc.person.uid);
      if (person) this.doc.person = person;
    }

    if (action == str.editLocation) {
      //get Locations
      console.log('HERE');

      if (!this.doc.address.coords) {
        //act._doc.address.coords._lat
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(
            (position) => {
              this._doc.address = { ...this.doc.address };
              this._doc.address.coords = {};
              this._doc.address.coords._lat = position.coords.latitude;
              this._doc.address.coords._long = position.coords.longitude;
              this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'modal-lg' }));
            }
          )
        }
      } else {
        this.modalRef = this.modalService.show(template, Object.assign({}, { class: 'modal-lg' }))
      }
    } else if (action == str.editServiceMilk) {
      if (this.doc.person) {
        this.checkNis(this.doc.person, 0);
        let check = this.checkStatusOnService2(this.doc, this.milkList, 1);

        if (doc && doc.uid) this.valid[1] = check;
      }

      if (this.checkSortPeople() || doc.person) this.setModalRef(template);
    } else if (action == str.editServiceAepeti) {
      if (this.doc.person) {
        this.checkNis(this.doc.person, 0);
        let check = this.checkStatusOnService2(this.doc, this.aepetiList, 1);
        if (doc && doc.uid) this.valid[1] = check;
      }

      this.setModalRef(template);



    } else if (action == str.editServiceAcessuas) {
      if (doc) this.setModalRef(template);
      if (!this.doc.uid) {
        this.checkStatusOnService(this.doc, this.acessuasList, template, true);
      }
    } else if (action == str.editServiceScfv) {
      if (this.doc.person) this.checkNis(this.doc.person, 0);
      this.listCores().then(() => {
        if (this.cores.length) {
          if (!doc) {
            this.doc.core = { uid: this.cores[0].uid, name: this.cores[0].name };
          }

          let check = this.checkStatusOnCore(this.doc, 1);
          if (doc && doc.uid) this.valid[1] = check;

          // this.checkStatusOnCore(this.doc, 1);
          this.setModalRef(template);
        } else {
          this.alertFeedback(this.text.noCores);
        }
      });
    } else if (action == str.editServiceGroup) {
      if (this.doc.person) this.checkNis(this.doc.person, 0);

      this.listGroup().then(() => {
        if (this.groups.length) {
          if (!doc) this.doc.group = { uid: this.groups[0].uid, name: this.groups[0].name };

          let check = this.checkStatusOnGroup(this.doc, 1);
          if (doc && doc.uid) this.valid[1] = check;

          if (this.checkAddress()) this.setModalRef(template);
        } else {
          this.alertFeedback(this.text.noGroups);
        }
      });

    } else if (action == str.editServiceFsp) {
      if (doc) this.listNotesFsp(doc, this.val.LIMIT_TWO);
      this.setModalRef(template);

    } else if (action == str.editServiceVisit) {
      if (this.doc.status == 0) this.valid[0] = true;
      if (doc) {
        this.listNotesFindsVisit(doc);
      }
      this.setListUser();

      if (this.checkAddress()) this.setModalRef(template);


    } else if (action == str.editGroupMeet) {
      console.log('HERE');
      let uid = doc.groupUid ? doc.groupUid : doc.uid;
      this.getMembersGroup(doc, uid, this.node.groupPeople, template);

    } else if (action == str.editCoreMeet) {
      let uid = doc.coreUid ? doc.coreUid : doc.uid;
      this.getMembersGroup(doc, uid, this.node.corePeople, template);

    } else if (action == str.editServiceHc) {
      //nmodalhc

      this.checkNis(this.doc.person, 0);

      let check = this.checkStatusOnService2(this.doc, this.hcList, 1);
      this.valid[1] = doc && doc.uid ? check : !check;


      if (doc) {
        this.checkProfessional(doc, 2);
        this.listNotesHc(doc, this.val.LIMIT_TWO);
        // if (doc.uid) this.setOk();
        if (!this.familyUid) this.getAddress(doc.familyUid);
      }

      this.setListUser();
      if (this.checkSortPeople() || doc.person) this.setModalRef(template);

    } else if (action == str.editServiceOld) {
      if (this.doc.person) {
        this.checkNis(this.doc.person, 0);
        let check = this.checkStatusServiceType2(this.doc, this.oldServiceList, 1);
        if (doc && doc.uid) this.valid[1] = check;

        if (this.checkSortPeople() || doc.person) this.setModalRef(template);
      }

    } else if (action == str.editServiceInterview) {
      if (doc && doc.uid) this.valid[0] = this.checkProfissional(doc);
      this.setListUser();
      this.setModalRef(template);
    } else if (action == str.editServiceFpgb) {
      if (doc) this.setListUser();
      this.valid[0] = true;
      this.setModalRef(template);
    } else if (action == str.editServiceCondition) {
      if (this.doc.person) this.checkNis(this.doc.person, 0);
      this.setModalRef(template);
    } else if (action == str.editServiceAttendance) {
      if (doc) this.checkProfessional(doc, 0);
      this.setListUser();
      this.setModalRef(template);
    } else if (action == str.editServiceSchedule) {
      if (this.checkAddress()) this.setModalRef(template);

    } else if (action == str.editAddress) {
      // this.listDistrict(this.auth.user.city.code).then(() => {
      //   // if (!this.districts.length) {
      //   //   this.alertFeedback(this.text.noDistrict);
      //   // } else {
      //   //   this.InitListSearch(0, this.districts);
      //   // }
      // });
      //this.checkDistrict(this.doc.district, this.districts, 1);
      this.checkStateOnAddress(this.arr.states[this.doc.state].id, 0);
      this.setModalRef(template);
    } else {
      this.setModalRef(template);
      this.checkValid(action);
    }
  }

  //Modal manager ------------------------------- END
}
