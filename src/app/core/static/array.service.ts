import { Injectable } from '@angular/core';
import { Role } from 'src/app/model/role';

@Injectable({
  providedIn: 'root'
})
export class ArrayService {


  //Array to all app
  //none, tech, adm, query, coord, coord-psb, coord-pse, adv-cmas, root(set by firebase)
  readonly reasonsToSetOff: any[] = [{ index: 0, text: 'Mudanca de cidade' },
  { index: 1, text: 'Óbito' },
  { index: 2, text: 'A pedido' },
  { index: 3, text: 'Fora dos critérios' }];

  readonly accessLevel: any[] = [{ index: 0, text: 'Nenhum' },
  { index: 1, text: 'Técnico' },
  { index: 2, text: 'Administrativo' },
  { index: 3, text: 'Consulta' },
  { index: 4, text: 'Coordenador' },
  { index: 5, text: 'Coordenador PSB' },
  { index: 6, text: 'Coordenador PSE' },
  { index: 7, text: 'Conselho CMAS' }];

  readonly sectorType: any[] = [{ index: 0, text: 'ACA-República' },
  { index: 1, text: 'ACESSUAS' },
  { index: 2, text: 'AEPETI' },
  { index: 3, text: 'Cadastro único' },
  { index: 4, text: 'Centro POP' },
  { index: 5, text: 'Conselho tutelar' },
  { index: 6, text: 'CRAS' },
  { index: 7, text: 'CREAS' },
  { index: 8, text: 'Criança feliz' },
  { index: 9, text: 'PAA Leite' },
  { index: 10, text: 'Residência Inclusiva' },
  { index: 11, text: 'SAAF' },
  { index: 12, text: 'SCFV' },
  { index: 13, text: 'Secretaria' }];


  readonly kinshipType: any[] = [{ index: 0, text: 'Filho(a)' },
  { index: 1, text: 'Enteado(a)' },
  { index: 2, text: 'Irmão(ã)' },
  { index: 3, text: 'Sobrinho(a)' },
  { index: 4, text: 'Companheiro(a)' },
  { index: 5, text: 'Conjugue' },
  { index: 6, text: 'Neto/Bisneto(a)' },
  { index: 7, text: 'Avó/Avô' },
  { index: 8, text: 'Pai/Mãe' },
  { index: 9, text: 'Primo(a)' },
  { index: 10, text: 'Tio(a)' },
  { index: 11, text: 'Responsável familiar' },
  { index: 12, text: 'Outro' }];

  readonly visitType: any[] = [{ index: 0, text: 'Domiciliar' },
  { index: 1, text: 'Institucional' }];

  readonly visitReasons: any[] = [{ index: 0, text: 'Acompanhamento/Família/Indivíduo' },
  { index: 1, text: 'Análise de padrão socioeconomico' },
  { index: 2, text: 'Averiguação' },
  { index: 3, text: 'Busca ativa/Abordagem social' },
  { index: 4, text: 'Direitos violagos' },
  { index: 5, text: 'Inserção bolsa família' },
  { index: 6, text: 'Inserção BPC' },
  { index: 7, text: 'Orientação/Outros' },
  { index: 8, text: 'Pessoas em situação de rua' },
  { index: 9, text: 'Transferência de município' },
  { index: 10, text: 'Verificação de composição familiar' }];

  readonly incomeSources: any[] = [{ index: 0, text: 'Aposentadoria/Pensão' },
  { index: 1, text: 'Servidor público' },
  { index: 2, text: 'BPC' },
  { index: 3, text: 'Empregado CTPS assinada' },
  { index: 4, text: 'Empregado sem CTPS assinada' },
  { index: 5, text: 'Trabalhador doméstico' },
  { index: 6, text: 'Trabalhador autônomo' },
  { index: 7, text: 'Outro' }];

  readonly benefitType: any[] = [{ index: 0, text: 'Benefício alimentação' },
  { index: 1, text: 'Benefício documento(taxas)' },
  { index: 2, text: 'Benefício funeral' },
  { index: 3, text: 'Benefício moradia' },
  { index: 4, text: 'Benefício natalidade' },
  { index: 5, text: 'Benefício viagem' },
  { index: 6, text: 'Calamidade pública' },
  { index: 7, text: 'Outros' },
  { index: 8, text: 'Vulnerabilidade temporaria' }];


  readonly bpcType: any[] = [{ index: 0, text: 'Não recebe' },
  { index: 1, text: 'BPC Idoso' },
  { index: 2, text: 'BPC Deficiente' },
  { index: 3, text: 'BPC Deficiente/Escola' }];

  readonly deficiencyType: any[] = [{ index: 0, text: 'Não possue deficiência' },
  { index: 1, text: 'Cegueira' },
  { index: 2, text: 'Baixa visão' },
  { index: 3, text: 'Surdez severa' },
  { index: 4, text: 'Surdez leve' },
  { index: 5, text: 'Deficiência física' },
  { index: 6, text: 'Deficiência mental' },
  { index: 7, text: 'Sindrome de down' },
  { index: 8, text: 'Transtorno mental/intelectual' },
  { index: 9, text: 'Microcefalia' },
  { index: 10, text: 'Deficiência multipla' },
  { index: 11, text: 'Outra' }];

  readonly extraInformations: any[] = [{ index: 0, text: 'Perfil para MCMV' },
  { index: 1, text: 'Recebe Bolsa Família' },
  { index: 2, text: 'Família em extrema pobreza' },
  { index: 3, text: 'Família indígena' },
  { index: 4, text: 'Família quilombola' },
  { index: 5, text: 'Família cigana' },
  { index: 6, text: 'Família ribeirinha' },
  { index: 7, text: 'Idoso em serviço de acolhimento' },
  { index: 8, text: 'Recebe Bolsa Família Municipal' },
  { index: 9, text: 'Família em vulnerabilidade social' },
  { index: 10, text: 'Outros benefícios municipais' },
  { index: 11, text: 'Vítima de calamidade pública' },
  { index: 12, text: 'Família/ em situação de rua' },
  { index: 13, text: 'Criança/adolescente em serviço de acolhimento' },
  { index: 14, text: 'Indivíduo/Família em risco social' }];

  readonly extraInfosPerson: any[] = [{ index: 0, text: 'Medidas protetivas/judicial' },
  { index: 1, text: 'Situação de trabalho infantil' },
  { index: 2, text: 'Risco de trabalho infantil' },
  { index: 3, text: 'Idoso em serviço de acolhimento' },
  { index: 4, text: 'Recebe Bolsa Família Municipal' },
  { index: 5, text: 'Outros benefícios municipais' },
  { index: 6, text: 'Crianças com microcefalia' },
  { index: 7, text: 'Gravidez na adolescência' },
  { index: 8, text: 'Gravidez na infância' },
  { index: 9, text: 'Indivíduo faz abusivo de álcool' },
  { index: 10, text: 'Cumprimento de MSE-LA' },
  { index: 11, text: 'Cumprimento de MSE-PSC' },
  { index: 12, text: 'Criança/adolescente com autismo' },
  { index: 13, text: 'Adulto com autismo' }];

  readonly attendanceType: any[] = [{ index: 0, text: 'Atendimento' },
  { index: 1, text: 'Visita domiciliar' }];

  readonly accessType: any[] = [{ index: 0, text: 'Demanda expontânea' },
  { index: 1, text: 'Busca ativa' },
  { index: 2, text: 'Encaminhamendo da rede socioassistencial' },
  { index: 3, text: 'Encaminhamento de outras politicas setoriais' },
  { index: 4, text: 'Família em acompanhamento' }];

  readonly inputType: any[] = [{ index: 0, text: 'Risco social' },
  { index: 1, text: 'Vunerabilidade social' }];

  readonly specInputTypeOne: any[] = [{ index: 0, text: 'Abandono criança' },
  { index: 1, text: 'Abandono idoso' },
  { index: 2, text: 'Abuso sexual' },
  { index: 3, text: 'Apropriação indebita do cartão' },
  { index: 4, text: 'Risco em razão da própria conduta' },
  { index: 5, text: 'Discriminação' },
  { index: 6, text: 'Exploração sexual' },
  { index: 7, text: 'Gravidez na adolescencia' },
  { index: 8, text: 'Famílias em situação de rua' },
  { index: 9, text: 'LGBT' },
  { index: 10, text: 'Mendicancia' },
  { index: 11, text: 'Maus tratos a criança/adolescente' },
  { index: 12, text: 'Maus tratos a deficiente' },
  { index: 13, text: 'Maus tratos idoso' },
  { index: 14, text: 'Negligencia criança/adolecente' },
  { index: 15, text: 'Negligencia deficiente' },
  { index: 16, text: 'Negligencia idoso' },
  { index: 17, text: 'Perda do poder familiar' },
  { index: 18, text: 'Supenção do poder familiar' },
  { index: 19, text: 'Trabalho infantil' },
  { index: 20, text: 'Trabalho escravo' },
  { index: 21, text: 'Tráfico de pessoas' },
  { index: 22, text: 'Uso de álcool/drogas' },
  { index: 23, text: 'Violência criança/adolescente' },
  { index: 24, text: 'Violência contra idoso' },
  { index: 25, text: 'Violência institucional' },
  { index: 26, text: 'Violência intrafamiliar' },
  { index: 27, text: 'Violência contra mulher' },
  { index: 28, text: 'Violência psicologica' },
  { index: 29, text: 'Violência sexual adulto' },
  { index: 30, text: 'Violência sexual criança/adolescente' },
  { index: 31, text: 'Violência sexal idoso' },
  { index: 32, text: 'Outros' }];

  readonly specInputTypeTwo: any[] = [{ index: 0, text: 'Abandono ou isolamento' },
  { index: 1, text: 'BF bloqueado ou suspenso' },
  { index: 2, text: 'Evasão do SCFV' },
  { index: 3, text: 'Famílias desc/condicionalidades' },
  { index: 4, text: 'Famílias S/ Acesso a serviço público' },
  { index: 5, text: 'Individuos em situação de rua' },
  { index: 6, text: 'IND/ Famílias em situação de pobreza' },
  { index: 7, text: 'IND/ Famílias em extrema pobreza' },
  { index: 8, text: 'Individuos pobres sem NIS' },
  { index: 9, text: 'Medida socio educativas' },
  { index: 10, text: 'Não possue residência' },
  { index: 11, text: 'Sem acesso a política de saúde' },
  { index: 12, text: 'Sem acesso a política de educação' },
  { index: 13, text: 'Sem acesso a justiça' },
  { index: 14, text: 'Sem acesso alimentação digna' },
  { index: 15, text: 'Outros' }];

  readonly outputType: any[] = [{ index: 0, text: 'Benefícios eventuais' },
  { index: 1, text: 'Demanda suprimida' },
  { index: 2, text: 'Desligamento' },
  { index: 3, text: 'Encaminhamento' },
  { index: 4, text: 'Outros' }];

  readonly specOutputTypeOne: any[] = [{ index: 0, text: 'Benefício alimentacao' },
  { index: 1, text: 'Benefício documento (taxas)' },
  { index: 2, text: 'Benefício funeral' },
  { index: 3, text: 'Benefício moradia' },
  { index: 4, text: 'Benefício natalidade' },
  { index: 5, text: 'Benefício viagem' },
  { index: 6, text: 'Calamidade pública' },
  { index: 7, text: 'Vulnerabilidade temporaria' },
  { index: 8, text: 'Outros' }];

  readonly specOutputTypeTwo: any[] = [{ index: 0, text: 'Atendimento/familia/individuo' },
  { index: 1, text: 'Benefícios eventuais' },
  { index: 2, text: 'Condicionalidade' },
  { index: 3, text: 'MSE' },
  { index: 4, text: 'Visitas domiciliares' },
  { index: 5, text: 'Outros' }];

  readonly specOutputTypeThree: any[] = [{ index: 0, text: 'Abrigo longa permanencia' },
  { index: 1, text: 'Assessoria juridica/municipal' },
  { index: 2, text: 'Adocão' },
  { index: 3, text: 'Agricultura' },
  { index: 4, text: 'Albergue' },
  { index: 5, text: 'Benefícios eventuais' },
  { index: 6, text: 'BPC deficiente' },
  { index: 7, text: 'BPC idoso' },
  { index: 8, text: 'BPC trabalho' },
  { index: 9, text: 'Cadastro único ou atualização' },
  { index: 10, text: 'Cadastro único/inclusão' },
  { index: 11, text: 'CAPS' },
  { index: 12, text: 'Carteira deficiente' },
  { index: 13, text: 'Carteira do idoso' },
  { index: 14, text: 'Cartorio' },
  { index: 15, text: 'Casa de passagem' },
  { index: 16, text: 'Centro dia' },
  { index: 17, text: 'Centro pop' },
  { index: 18, text: 'Conselho tutelar' },
  { index: 19, text: 'CREAS' },
  { index: 20, text: 'Defensoria pública/estadual' },
  { index: 21, text: 'Delegacia' },
  { index: 22, text: 'Delegacia da mulher' },
  { index: 23, text: 'Delegacia do idoso' },
  { index: 24, text: 'Documentação civil' },
  { index: 25, text: 'Educação' },
  { index: 26, text: 'Família extensa' },
  { index: 27, text: 'Família substituta' },
  { index: 28, text: 'Fórum' },
  { index: 29, text: 'Habitacão' },
  { index: 30, text: 'INSS' },
  { index: 31, text: 'Visando Benefício' },
  { index: 32, text: 'Ministério público' },
  { index: 33, text: 'Passe livre' },
  { index: 34, text: 'PPCAM' },
  { index: 35, text: 'Pronatec' },
  { index: 36, text: 'Reitegracão familiar' },
  { index: 37, text: 'República' },
  { index: 38, text: 'Residencia inclusiva' },
  { index: 39, text: 'Saúde' },
  { index: 40, text: 'Saúde bucal' },
  { index: 41, text: 'Saúde da familia' },
  { index: 42, text: 'Saúde mental' },
  { index: 43, text: 'Saúde NASF' },
  { index: 44, text: 'Saúde ortese/protese' },
  { index: 45, text: 'Saúde regulação' },
  { index: 46, text: 'SCFV' },
  { index: 47, text: 'Visitas domiciliares' }];

  readonly specOutputTypeFour: any[] = [{ index: 0, text: 'Atendimento individualizado' },
  { index: 1, text: 'Atendimento coletivo continuo' },
  { index: 2, text: 'Atendimento coletivo não continuo' },
  { index: 3, text: 'Bolsa família municipal' },
  { index: 4, text: 'Cadastramento/atualização cadastral' },
  { index: 5, text: 'Orientação pontual' },
  { index: 6, text: 'Acompanhamento de MSE' },
  { index: 7, text: 'Acompanhamento individual/coletivo' },
  { index: 8, text: 'Solicitação de benefício eventual' }];

  readonly scheduleAttendanceType: any[] = [{ index: 0, text: 'Assistente social' },
  { index: 1, text: 'Jurídico' },
  { index: 2, text: 'Psicosocial' },
  { index: 3, text: 'Psicológico' }];

  // object 
  readonly rangeAgeHc: any[] = [{ index: 0, text: '0 à 36 meses' },
  { index: 1, text: '36 à 72 meses'}];

  readonly rangeAgePlan: any[] = [{ index: 0, text: '0 à 3 meses' },
  { index: 1, text: '3 à 6 meses' },
  { index: 2, text: '6 à 9 meses' },
  { index: 3, text: '9 à 12 meses' },
  { index: 4, text: '12 à 18 meses' },
  { index: 5, text: '18 à 24 meses' },
  { index: 6, text: '24 à 36 meses' },
  { index: 7, text: '36 à 42  meses' },
  { index: 8, text: '42 à 48  meses' },
  { index: 9, text: '54 à 60 meses' },
  { index: 10, text: '60 à 64 meses' },
  { index: 11, text: '64 à 72 meses' }];

  readonly monthOfYear: any[] = [{ index: 0, text: 'Janeiro' },
  { index: 1, text: 'Fevereiro' },
  { index: 2, text: 'Março' },
  { index: 3, text: 'Abril' },
  { index: 4, text: 'Maio' },
  { index: 5, text: 'Junho' },
  { index: 6, text: 'Julho' },
  { index: 7, text: 'Agosto' },
  { index: 8, text: 'Setembro' },
  { index: 9, text: 'Outubro' },
  { index: 10, text: 'Novembro' },
  { index: 11, text: 'Dezembro' }];

  readonly monthPeriod: any[] = [{ index: 0, text: 'Mensal' },
  { index: 1, text: 'Trimestral' }];

  readonly targetHc: any[] = [{ index: 0, text: 'Gestante'},
  { index: 1, text: 'Cadastro único'},
  { index: 2, text: 'BPC'},
  { index: 3, text: 'Acolhimento'},
  { index: 4, text: 'BPC e acolhimento'}];


  readonly serviceType: any[] = [{ index: 0, text: 'Emissão carteira do idoso' },
  { index: 1, text: 'Emissão de declaração provisória' },
  { index: 2, text: 'Emissão de extrato de pagamento INSS' }];

  readonly childRangeAge: any[] = [{ index: 0, text: '0 - 12 anos' },
  { index: 1, text: '13 - 18 anos' }];

  readonly statusHcFilter: any[] = [{ index: 1, text: 'Ativos' },
  { index: 4, text: 'Desligados' }];

  readonly status: any[] = [{ index: 0, text: 'Pendente' },
  { index: 1, text: 'Ativo' },
  { index: 2, text: 'Atendido' },
  { index: 3, text: 'Realizado' },
  { index: 4, text: 'Desligado' },
  { index: 5, text: 'Resolvido' },
  { index: 6, text: 'Averiguado' },
  { index: 7, text: 'Registrado' },
  { index: 8, text: 'Cancelado' },
  { index: 9, text: 'Respondido' },
  { index: 10, text: 'Concedido' },
  { index: 11, text: 'Analizando' }];

  readonly gender: any[] = [{ index: 0, text: 'Masculino' },
  { index: 1, text: 'Feminino' }];

  readonly childInputType: any[] = [{ index: 0, text: 'Risco social' }];

  readonly childSpecInputType: any[] = [{ index: 0, text: 'Abandono de incapaz' },
  { index: 1, text: 'Abuso' },
  { index: 2, text: 'Cri./Adol. em situação de rua' },
  { index: 3, text: 'Discriminação' },
  { index: 4, text: 'Estupro' },
  { index: 5, text: 'Exploração sexual' },
  { index: 6, text: 'Gravidez na adolescencia' },
  { index: 7, text: 'LGBTS' },
  { index: 8, text: 'Maus tratos' },
  { index: 9, text: 'Negligencia' },
  { index: 10, text: 'Perda do poder familiar' },
  { index: 11, text: 'Requisição de documentos' },
  { index: 12, text: 'Trabalho infantil' },
  { index: 13, text: 'Uso de Álcool/Drogas' },
  { index: 14, text: 'Violência Criança/Adolescente' }];

  readonly childOutputType: any[] = [{ index: 0, text: 'Demanda suprimida' },
  { index: 1, text: 'Desligamento' },
  { index: 2, text: 'Encaminhamento' }];

  readonly childSpecOutputType: any[] = [{ index: 0, text: 'Abandono de incapaz' },
  { index: 1, text: 'Abuso' },
  { index: 2, text: 'Cri./Adol. em situação de rua' },
  { index: 3, text: 'Discriminação' },
  { index: 4, text: 'Estupro' },
  { index: 5, text: 'Exploração sexual' },
  { index: 6, text: 'Gravidez na adolescencia' },
  { index: 7, text: 'LGBTS' },
  { index: 8, text: 'Maus tratos' },
  { index: 9, text: 'Negligencia' },
  { index: 10, text: 'Perda do poder familiar' },
  { index: 11, text: 'Requisição de documentos' },
  { index: 12, text: 'Trabalho infantil' },
  { index: 13, text: 'Uso de Álcool/Drogas' },
  { index: 14, text: 'Violência Criança/Adolescente' }];

  readonly childSpecOutputTypeTwo: any[] = [{ index: 0, text: 'Abrigo longa permanência' },
  { index: 1, text: 'Assessoria juridica/Municipal' },
  { index: 2, text: 'Adoção' },
  { index: 3, text: 'Agricultura' },
  { index: 4, text: 'Albergue' },
  { index: 5, text: 'Benefícios eventuais' },
  { index: 6, text: 'BPC' },
  { index: 7, text: 'BPC trabalho' },
  { index: 8, text: 'Cadastro único' },
  { index: 9, text: 'CAPS' },
  { index: 10, text: 'Cartório/RC' },
  { index: 11, text: 'Casa de passagem' },
  { index: 12, text: 'Centro dia' },
  { index: 13, text: 'Centro POP' },
  { index: 14, text: 'CRAS' },
  { index: 15, text: 'CREAS' },
  { index: 16, text: 'Defensoria Pública/Estadual' },
  { index: 17, text: 'Delegacia da mulher' },
  { index: 18, text: 'Delegacia do idoso' },
  { index: 19, text: 'Documento civil' },
  { index: 20, text: 'Educação' },
  { index: 21, text: 'Família extensa' },
  { index: 22, text: 'Família substituta' },
  { index: 23, text: 'Fórum' },
  { index: 24, text: 'Habitação' },
  { index: 25, text: 'INSS visando benefício' },
  { index: 26, text: 'Ministério público' },
  { index: 27, text: 'PPCAM' },
  { index: 28, text: 'PRONATEC' },
  { index: 29, text: 'Reintegração familiar' },
  { index: 30, text: 'República' },
  { index: 31, text: 'Residência inclusiva' },
  { index: 32, text: 'Saúde' },
  { index: 33, text: 'Saúde bucal' },
  { index: 34, text: 'Saúde da família' },
  { index: 35, text: 'Saúde mental' },
  { index: 36, text: 'Saúde NASF' },
  { index: 37, text: 'Saúde Órtese/Prótese' },
  { index: 38, text: 'Saúde regulação' },
  { index: 39, text: 'SCFV' },
  { index: 40, text: 'Visitas domiciliares' }];

  readonly states: any[] = [{ index: 0, id: 'AC', text: 'Acre' },
  { index: 1, id: 'AL', text: 'Alagoas' },
  { index: 2, id: 'AP', text: 'Amapá' },
  { index: 3, id: 'AM', text: 'Amazonas' },
  { index: 4, id: 'BA', text: 'Bahia' },
  { index: 5, id: 'CE', text: 'Ceará' },
  { index: 6, id: 'DF', text: 'Distrito Federal' },
  { index: 7, id: 'ES', text: 'Espírito Santo' },
  { index: 8, id: 'GO', text: 'Goiás' },
  { index: 9, id: 'MA', text: 'Maranhão' },
  { index: 10, id: 'MT', text: 'Mato Grosso' },
  { index: 11, id: 'MS', text: 'Mato Grosso do Sul' },
  { index: 12, id: 'MG', text: 'Minas Gerais' },
  { index: 13, id: 'PA', text: 'Pará' },
  { index: 14, id: 'PB', text: 'Paraíba' },
  { index: 15, id: 'PR', text: 'Paraná' },
  { index: 16, id: 'PE', text: 'Pernambuco' },
  { index: 17, id: 'PI', text: 'Piauí' },
  { index: 18, id: 'RJ', text: 'Rio de Janeiro' },
  { index: 19, id: 'RN', text: 'Rio Grande do Norte' },
  { index: 20, id: 'RS', text: 'Rio Grande do Sul' },
  { index: 21, id: 'RO', text: 'Rondônia' },
  { index: 22, id: 'RR', text: 'Roraima' },
  { index: 23, id: 'SC', text: 'Santa Catarina' },
  { index: 24, id: 'SP', text: 'São Paulo' },
  { index: 25, id: 'SE', text: 'Sergipe' },
  { index: 26, id: 'TO', text: 'Tocantins' }];


  readonly incomeType: any[] = [{ index: 0, text: 'Até meio salário minímo' },
  { index: 1, text: 'Até 1 salário minímo' },
  { index: 2, text: 'Até 1 salário minímo e meio' },
  { index: 3, text: 'Até 2 salários minímos' }];

  readonly locationType: any[] = [{ index: 0, text: 'CETEP' },
  { index: 1, text: 'INFOCENTRO' },
  { index: 2, text: 'SENAI' }];

  readonly shiftType: any[] = [{ index: 0, text: 'Matutino' },
  { index: 1, text: 'Vespertino' },
  { index: 2, text: 'Noturno' }];

  readonly shiftWork: any[] = [{ index: 0, text: 'Integral' },
  { index: 1, text: 'Matutino' },
  { index: 2, text: 'Vespertino' },
  { index: 3, text: 'Noturno' }];

  readonly schoolingType: any[] = [{ index: 0, text: 'Não possue' },
  { index: 1, text: 'Nível médio completo' },
  { index: 2, text: 'Nível médio incompleto' },
  { index: 3, text: 'Nível fundamental completo' },
  { index: 4, text: 'Nível fundamental incompleto' },
  { index: 5, text: 'Nunca estudou' },
  { index: 6, text: 'Analfabeto' }];

  readonly situationType: any[] = [{ index: 0, text: 'Inscrito no cadastro único' },
  { index: 1, text: 'Beneficiário do PBF' },
  { index: 2, text: 'Beneficiário BPC deficiente' },
  { index: 3, text: 'Situação de extrema pobreza' },
  { index: 4, text: 'Egressos do projoven e SCFV' },
  { index: 5, text: 'Egressos do MSE' },
  { index: 6, text: 'Trabalho Infantil' },
  { index: 7, text: 'Situação de rua' },
  { index: 8, text: 'Criança em acolhimento provisório' },
  { index: 9, text: 'Egressos de serviço de acolhimento' },
  { index: 10, text: 'Território de Risco/Tráfico de drogas' },
  { index: 11, text: 'Egressos do sistema penal' },
  { index: 12, text: 'Retirada do trabalho escravo' },
  { index: 13, text: 'Violência contra a mulher' },
  { index: 14, text: 'Outra' }];

  readonly accessForm: any[] = [
    { index: 0, text: 'Busca ativa' },
    { index: 1, text: 'Demanda espontânea' },
    { index: 2, text: 'Encaminhamento/Rede/Outra p setorial' },
    { index: 3, text: 'Encaminhamento/Rede/Socioassistencial' }];

  readonly infoPlus: any[] = [{ index: 0, text: 'Perfil para MCMV' },
  { index: 1, text: 'Medidas protetivas/judicial' },
  { index: 2, text: 'Recebe bolsa família' },
  { index: 3, text: 'Recebe benefício de transferência de renda  municipal' },
  { index: 4, text: 'Outros benefícios municipais' },
  { index: 5, text: 'Vítima de calamidade pública' },
  { index: 6, text: 'Indivíduo ou família com direito violado' }];

  readonly accessFormAEPETI: any[] = [{ index: 0, text: 'Forma de acesso' },
  { index: 1, text: 'Busca ativa' },
  { index: 2, text: 'Demanda espontânea' },
  { index: 3, text: 'Encaminhamento/Rede/Outra setorial' },
  { index: 4, text: 'Encaminhamento/Rede/Socioassistencial' },
  { index: 5, text: 'Ministério público' },
  { index: 6, text: 'Poder judiciário' },
  { index: 7, text: 'Outro' },
  { index: 8, text: 'TAC' }];

  readonly yesNoType: any[] = [{ index: 0, text: 'Não' },
  { index: 1, text: 'Sim' }];

  readonly workActivityType: any[] = [{ index: 0, text: 'Constução civil' },
  { index: 1, text: 'Carregador de feira' },
  { index: 2, text: 'Feiras livres' },
  { index: 3, text: 'Garçom' },
  { index: 4, text: 'Insdustria de transformação' },
  { index: 5, text: 'Lava jato' },
  { index: 6, text: 'Oficina' },
  { index: 7, text: 'Outros' },
  { index: 8, text: 'Pescas' },
  { index: 9, text: 'Serralheiria' },
  { index: 10, text: 'Trabalho de Lavouras' },
  { index: 11, text: 'Trabalho penoso' },
  { index: 12, text: 'Trabalho domésticos' },
  { index: 13, text: 'Trabalho escravo' },
  { index: 14, text: 'Vendedor de picolé' }];

  readonly activityType: any[] = [{ index: 0, text: 'Trabalho não remunerado' },
  { index: 1, text: 'Trabalho remunerado' }];

  readonly periodType: any[] = [{ index: 0, text: 'Até 06 meses' },
  { index: 1, text: 'Até 1 ano' },
  { index: 2, text: 'Até 2 anos' },
  { index: 3, text: 'Mais de 2 anos' }];

  readonly moneyDestinyType: any[] = [{ index: 0, text: 'Auto sustento' },
  { index: 1, text: 'Complemento familiar' }];

  readonly incomeTypeAEPETI: any[] = [{ index: 0, text: 'Até meio salário mínimo' },
  { index: 1, text: 'Até 1 salário mínimo' },
  { index: 2, text: 'Até 2 salário mínimo' }];

  readonly socialProgram: any[] = [{ index: 0, text: 'Bolsa família' },
  { index: 1, text: 'SCFV' },
  { index: 2, text: 'Outro' }];

  readonly conditionType: any[] = [{ index: 0, text: 'Educação' },
  { index: 1, text: 'Saúde' }];

  readonly interviewType: any[] = [{ index: 0, text: 'Manual' },
  { index: 1, text: 'Online' }];

  readonly modalityType: any[] = [{ index: 0, text: 'Atualização' },
  { index: 1, text: 'Inclusão' },
  { index: 2, text: 'Exclusão' },
  { index: 3, text: 'Transferência' }];

  readonly modalityVisit: any[] = [{ index: 0, text: 'PAIF' },
  { index: 1, text: 'SCFV' },
  { index: 2, text: 'Idosos e PCD' },
  { index: 3, text: 'Programa primeira infancia no SUAS' },
  { index: 4, text: 'BPC na escola' },
  { index: 5, text: 'Descumprimento de condicionalidade' }];

  readonly goalType: any[] = [{ index: 0, text: 'Benefício municipal' },
  { index: 1, text: 'Benefício eventual' },
  { index: 2, text: 'Bolsa estiagem' },
  { index: 3, text: 'Bolsa família' },
  { index: 4, text: 'BPC' },
  { index: 5, text: 'Carteira do idoso' },
  { index: 6, text: 'ID jovem' },
  { index: 7, text: 'Isenção em concursos' },
  { index: 8, text: 'Leite fome zero' },
  { index: 9, text: 'Obter/Validar NIS' },
  { index: 10, text: 'Outros benefícios do MDS' },
  { index: 11, text: 'Passe Livre' },
  { index: 12, text: 'Programa MCMV' },
  { index: 13, text: 'PRONATEC' },
  { index: 14, text: 'Seguro safra' },
  { index: 15, text: 'Sem finalidade específica' },
  { index: 16, text: 'Tarifa social' },
  { index: 17, text: 'Telefona popular' }];

  readonly requestType: any[] = [{ index: 0, text: 'Bloqueio de benefícios' },
  { index: 1, text: 'Desbloqueio de benefícios' },
  { index: 2, text: 'Cancelamento de benefícios' },
  { index: 3, text: 'Reversão de cancelamento' },
  { index: 4, text: 'Reversão de suspensão' }];

  readonly resourceType: any[] = [{ index: 0, text: 'Indeferido' },
  { index: 1, text: 'Deferido' },
  { index: 2, text: 'Sem recurso' }];

  readonly attendanceReport: any[] = [{ index: -1, text: 'Todos' },
  { index: 0, text: 'Meus atendimentos' }];

  readonly roles2: Role[] = [{ level: 'thanos', text: 'Administrador' },
   { level: 'editCity', text: 'Cidade' },
   { level: 'editUser', text: 'Usuários' },
   { level: 'editSms', text: 'SMS' },
   { level: 'editReport', text: 'Relatório' },
   { level: 'editDistrict', text: 'Bairros' },
   { level: 'editContact', text: 'Contato' },
   { level: 'editNote', text: 'Anotações' },
   { level: 'editFamily', text: 'Família' },
   { level: 'esGroup', text: 'Grupos' },
   { level: 'esVisit', text: 'Visita' },
   { level: 'esBenefit', text: 'Benefícios eventuais' },
   { level: 'esReference', text: 'Referenciamento' },
   { level: 'esAttendance', text: 'Atendimentos' },
   { level: 'esSchedule', text: 'Atendimentos Agendados' },
   { level: 'esFsp', text: 'PAF' },
   { level: 'esRoute', text: 'Encaminhamento' },
   { level: 'esHc', text: 'Criança feliz' },
   { level: 'esOld', text: 'Serviço ao idoso' },
   { level: 'esNotification', text: 'Notificação' },
   { level: 'esTerm', text: 'Termo' },
   { level: 'esDemand', text: 'Demanda' },
   { level: 'esAcessuas', text: 'ACESSUAS' },
   { level: 'esAepeti', text: 'AEPETI' },
   { level: 'esCondition', text: 'Condicionalidade' },
   { level: 'esComplaint', text: 'Denúncia' },
   { level: 'esInterview', text: 'Entrevista' },
   { level: 'esFpgb', text: 'FPGB' },
   { level: 'esMilk', text: 'PAA leite' },
   { level: 'esScfv', text: 'SCFV' },
   { level: 'esPendancy', text: 'Pendência' }];

     //implement later
  /*  {text: 'Mensagens', index: 'sms', action: 'editSms'},
   {text: 'BPC', index: '', action: ''},
   {text: 'Perfil para BPC', index: '', action: ''},
   {text: '$Pesquisa', index: '', action: 'esFsp'}, 
   { text: 'Notificações', index: 'service-notification', action: 'esNotification' },
   { text: 'Equipe', index: 'users', action: 'editUser' },*/
   readonly analyticalReportType: any[] = [{ text: '$Atendimentos', index: 'service-attendance', action: 'esAttendance' },
   { text: 'Acompanhamento familiar', index: 'service-fsp', action: 'esFsp' },
   { text: '$Criança feliz', index: 'service-hc', action: 'esHc' },
   { text: 'Condicionalidades', index: 'service-condition', action: 'esCondition' },
   { text: 'Serviço ao idoso', index: 'service-old', action: 'esOld' },
   { text: 'Encaminhamento', index: 'service-route', action: 'esRoute' },
   { text: '$Benefícios eventuais', index: 'service-benefit', action: 'esBenefit' },
   { text: 'Referenciamento', index: 'service-reference', action: 'esReference' },
   { text: 'Visitas', index: 'service-visit', action: 'esVisit' },
   { text: '$ACESSUAS', index: 'service-acessuas', action: 'esAcessuas' },
   { text: 'AEPETI', index: 'service-aepeti', action: 'esAepeti' },
   { text: 'Denuncias', index: 'service-complaint', action: 'esComplaint' },
   { text: '$Entrevistas', index: 'service-interview', action: 'esInterview' },
   { text: 'FPGB', index: 'service-fpgb', action: 'esFpgb' },
   { text: 'Pendencias', index: 'service-pendency', action: 'esPendency' },
   { text: 'PAA Leite', index: 'service-milk', action: 'esMilk' },
   { text: '$SCFV', index: 'service-scfv', action: 'esScfv' }];

  readonly syntheticReportType: any[] = [{ text: 'Condicionalidades', index: 'service-condiction', action: 'esCondition' },
  { text: 'Serviço ao idoso', index: 'service-old', action: 'esOld' },
  { text: 'Encaminhamento', index: 'service-route', action: 'esRoute' },
  { text: 'Mensagens', index: 'sms', action: 'edtiSms' },
  { text: 'Benefícios eventuais', index: 'service-benefit', action: 'esBenefit' },
  { text: 'BPC', index: '', action: '' },
  { text: 'Pesquisa', index: '', action: '' },
  { text: 'Referenciamento', index: 'service-reference', action: 'esReference' },
  { text: 'Perfil para BPC', index: '', action: '' },
  { text: 'Visitas', index: 'service-visit', action: 'esVisit' },
  { text: 'Base RMA CRAS', index: '', action: '' },
  { text: 'Base SIACOF CRAS', index: '', action: '' },
  { text: 'Base novo SIACOF CRAS', index: '', action: '' },
  { text: 'Cursos de preferência', index: '', action: '' },
  { text: 'ACESSUAS', index: 'service-acessuas', action: 'esAcessuas' },
  { text: 'AEPETI', index: 'service-aepeti', action: 'esAepeti' },
  { text: 'Denuncias', index: 'service-complaint', action: 'esComplaint' },
  { text: 'Entrevistas', index: 'service-interview', action: 'esInterview' },
  { text: 'FPGB', index: 'service-fpgb', action: 'esFpgb' },
  { text: 'Pendencias', index: 'service-pendency', action: 'esPendency' },
  { text: 'Notificações', index: 'service-notification', action: 'esNotification' },
  { text: 'PAA Leite', index: 'service-milk', action: 'esMilk' },
  { text: 'SCFV', index: 'service-scfv', action: 'esScfv' },
  { text: 'Vigilância', index: '', action: '' }];

  readonly searchReport: any[] = [{ text: 'Perfil para MCMV', value: 'MCMV', index: 'service-reference' },
  { text: 'Medidas protetivas/judicial', value: 'judicial', index: 'people' },
  { text: 'Recebe Bolsa Família', value: 'bolsa', index: 'people' },
  { text: 'Recebe Bolsa Família municipal', value: 'municipal', index: 'people' },
  { text: 'Outros benefícios municipais', value: 'benefícios', index: 'people' },
  { text: 'Vítima de calamidade pública', value: 'calamidade', index: 'service-reference' },
  { text: 'Criança/adolescente em risco de trabalho infantil', value: 'risco|infantil', index: 'people' },
  { text: 'Criança/adolescente em situação de trabalho infantil', value: 'situação|infantil', index: 'people' },
  { text: 'Família em extrema pobreza', value: 'pobreza', index: 'service-reference' },
  { text: 'Crianças com microcefalia', value: 'microcefalia', index: 'people' },
  { text: 'Criança/adolescente em serviço de acolhimento', value: 'adolescente|acolhimento', index: 'service-reference' },
  { text: 'Família/Pessoa em situação de rua', value: 'rua', index: 'service-reference' },
  { text: 'Família quilombola', value: 'quilombola', index: 'service-reference' },
  { text: 'Família ribeirinha', value: 'ribeirinha', index: 'service-reference' },
  { text: 'Família cigana', value: 'cigana', index: 'service-reference' },
  { text: 'Família indígena', value: 'indígena', index: 'service-reference' },
  { text: 'Gravidez na adolescencia', value: 'gravidez|adolescencia', index: 'people' },
  { text: 'Indivíduo/Família-Membro faz abusivo de álcool', value: 'álcool', index: 'people' },
  { text: 'Gravidez na infância', value: 'gravidez|infância', index: 'people' },
  { text: 'Idoso em serviço de acolhimento', value: 'idoso|acolhimento', index: 'people' },
  { text: 'Cumprimimento de MSE-LA', value: 'mse|la', index: 'people' },
  { text: 'Cumprimimento de MSE-PSC', value: 'psc', index: 'people' },
  { text: 'Indivíduo/Família em vulnerabilidade social', value: 'vulnerabilidade', index: 'service-reference' },
  { text: 'Indivíduo/Família em risco social', value: 'família|risco', index: 'service-reference' },
  { text: 'Criança/adolescente com autismo', value: 'adolescente|autismo', index: 'people' },
  { text: 'Adulto com autismo', value: 'adulto|autismo', index: 'people' }];
  // don't have this items
  //  {text: 'Perda do poder familiar', value: 'perda', index: ''},
  //  {text: 'Família acolhedora', value: 'acolhedora', index: ''}];
  //  {text: 'Indivíduo ou família com direito violado', value: 'violado', index: ''},


  constructor() { }
}
