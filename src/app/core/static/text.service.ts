import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class TextService {
  /***************************************************/
  /***************************************************/
  /* SITE */
  readonly titleSite: string = "SIMVIS"
  readonly sectorBD: string = "Centro de referência de assistência social (Deixar pegando do Banco)"
  readonly sector: string = "Setor"
  readonly login: string = "Login"
  readonly logout: string = "Sair"
  readonly user: string = "Usuário";
  readonly pass: string = "Senha";
  readonly rememberMe: string = "Lembre-me";
  readonly errorNumber: string = "404";
  readonly pageNotFound: string = "Página não encontrada";
  readonly typeSomething: string = "Digite algo..."
  readonly operationSuccess: string = "Opereção realizada com sucesso!";
  readonly signInAgain: string ='Saia e entre novamente para mudar a senha'
  readonly registeredOnCore: string = 'Já cadastrado no núcleo';
  readonly registeredOnGroup: string = 'Já cadastrado no grupo';
  readonly registeredOnService: string = 'Úsuario já registrado no serviço';
  readonly operationError: string = "Erro ao realizar operação!"
  readonly confirmUpdates: string = "Confirmar alteração?"
  readonly responsibleFamiliar: string = 'Responsável familiar'
  readonly noAddress: string = 'Endereço não cadastro';
  readonly noResultFoundToSearch: string = 'Sem resultados para essa pesquisa'
  readonly withoutDatas: string = 'Sem dados cadastrados'
  readonly members: string = "Membros"
  readonly userDataLoggedIn: string = "Dados do usuário logado"
  readonly plus: string = 'mais';
  readonly differentSubSector = 'Dados de outro sub-sector';
  readonly month: string = 'Mês';
  readonly quart: string = 'Tri';
  readonly rangeAge: string = 'Faixa etária'; 
  readonly status: string = 'Estado';
  readonly defaultOption: string = 'Selecione uma opção';
  readonly noneAction: string = 'Nenhuma ação realizada!';
  readonly noResponsible: string = 'Resposável familiar não cadastrado!';
  readonly noSearchResult: string = 'Não há resultados para esssa pesquisa!';
  readonly noData: string = 'Não há dados!';


  /* Home */
  readonly accessInformationsError: string = "Úsuario ou senha incorretos"
  /***************************************************/
  /***************************************************/
  /* edit group meet */

  readonly meetingHappendAt = 'Reunião aconteceu em';
  /***********************;****************************/
  /***************************************************/
  /* Several Buttons */
  readonly pendant: string = "Pendente"
  readonly active: string = 'Ativo';
  readonly answer: string = 'Resposta';
  readonly toAnswer: string = 'Responder';
  readonly asked: string = "Respondido"
  readonly released: string = 'Lançado';
  readonly finished: string = 'Finalizado';
  readonly off: string = "Desligado"
  readonly granted: string = "Concedido"
  readonly setOff: string = "Desligar"
  readonly canceled: string = "Cancelado(a)"
  readonly cancel: string = "Cancelar";
  readonly ok: string = "Ok";
  readonly clear: string = "Limpar";
  readonly new: string = "Novo";
  readonly selectLocal: string = "Selecionar localização";
  readonly selectDistrict: string = "Selecione um Bairro";
  readonly back: string = "Voltar";
  readonly completed: string = 'Realizado(a)';
  readonly print: string = 'Imprimir';

  /***************************************************/
  /***************************************************/
  /* NavBar */
  readonly benefitNav: string = "Benefícios eventuais"
  readonly milkNav: string = "PPA Leite"
  readonly fspNav: string = "PAF"
  readonly scheduleNav: string = "Agendar"

  readonly people: string = "Pessoas"
  readonly recently: string = "Visto recentemente"


  /***************************************************/
  /***************************************************/
  /* Fields Verification */
  readonly requiredField: string = "*Campo obrigatório"
  readonly invalidPass: string = "Senha inválida"
  readonly invalidUser: string = "Usuário inválido"
  readonly alreadyLogin: string = "Já está logado"
  readonly loginGoogle: string = "Entrar com google"

  /***************************************************/
  /***************************************************/
  /* Dabshboard */
  readonly schedule: string = "Agenda"
  readonly homesVisit: string = " Visita domiciliares"
  readonly report: string = "Relatório"
  readonly analytical: string = "Analítico"
  readonly synthetic: string = "Sintético"
  readonly vigilance: string = "Vigilância"
  readonly file: string = "Arquivo"
  readonly sms: string = "SMS"
  readonly meeting: string = "Reunião"
  readonly infoCity: string = "Informações da Cidade"

  /* Relatório */
  readonly generateReport: string = "Gerar relatório"
  readonly startDate: string = "Data inicial"
  readonly endDate: string = "Data final"

  /* SMS */
  readonly employees: string = "Funcionários"
  readonly family: string = "Família"
  readonly message: string = "Mensagem"
  readonly send: string = "Enviar"
  
  /* Distric */
  readonly newDistrict: string = 'Novo bairro';
  readonly noDistrict: string = 'Não há bairros cadastrados';
  readonly existingDistrict: string = "Bairros cadastrados"
  readonly listDistrict: string = 'Lista de Bairros';

  /* PSF */
  readonly newPsf: string = 'Novo PSF';
  readonly noPsf: string = 'Não há PSF cadastrados';
  readonly existingPsf: string = "PSF cadastrados"
  readonly listPsf: string = 'Lista de PSF';
  readonly psfText: string = 'PSF';
  

  /* Group */
  readonly newGroup: string = "Novo grupo"
  readonly createGroup: string = "Criar grupo"
  readonly existingGroups: string = "Grupos existentes"
  readonly meetingList: string = "Lista de presença";
  readonly listPeople: string = 'Lista de pessoas';
  readonly reportScfv: string = "Relatório"
  readonly meetDate: string = 'Data da reunião';

  /* Users */
  readonly users: string = "Usuários"
  readonly team: string = "Equipe"
  readonly search: string = "Buscar"
  readonly addUser: string = "Adicionar usuário"
  readonly inBlankForFullList: string = "Em branco para lista completa"
  readonly results: string = "Resultados"
  readonly accessLevel: string = 'Nível de acesso';
  readonly accessDontCreated: string = "Informacões de acesso não cadastradas";
  readonly changePassword: string = "Mudar senha";


  /* ADD USER */
  readonly professionalRegistry: string = "Registro profissional"
  readonly subSector: string = "Sub setor"
  readonly workload: string = "Carga horária"
  readonly email: string = "Email"
  readonly emailAddress: string = "Endereço de e-mail"
  readonly function: string = "Função"
  readonly weekly: string = "Semanalmente"
  readonly daily: string = "Diariamente"
  readonly remove: string = "Remover"
  readonly registeredEmail: string = 'Email já cadastrado';
  readonly invalidEmail: string = 'Email inválido';
  readonly resetPassword: string = 'Nova senha';
  readonly activities: string = 'Atividades';
  readonly cityCode: string = 'Código da cidade';

  /* Info City */
  readonly organName: string = "Nome do Órgão";
  readonly organLogo: string = "Logo do Órgão";
  readonly prefectureLogo: string = "Logo da Prefeitura";
  readonly cnpj: string = "CNPJ";
  readonly ibgeCod: string = "Código do IBGE";

  /* Meeting */
  readonly meetingName: string = "Nome da reunião"
  readonly meetingDate: string = "Data da reunião"
  readonly numberOfParticipants: string = "Número de participantes"
  readonly generatePdf: string = "Gerar PDF"

  /* Route */
  readonly downloadFile: string = "Baixar arquivo"
  readonly cpfOfRF: string = "CPF do RF"
  readonly sus: string = "SUS"
  readonly selectImage: string = "Selecione um arquivo do tipo imagem";
  readonly imageSize: string = "Seleciona uma imagem com menos 2MB";

  /* Contact */
  readonly contact: string = "Contato"
  readonly houseType: string = "Tipo de Casa"
  readonly employee: string = "Funcionário"

  /* Frequency */
  readonly core: string = "Núcleo"
  readonly newCore: string = "Novo núcleo"
  readonly listUsers: string = "Listar usuários"
  readonly listCores: string = 'Lista de núcleos';
  readonly sendFrequency: string = "Enviar frequência"
  readonly LastFrequencyReleasedBy: string = "Última frequência lançada por:"
  readonly local: string = "Local"

  /***************************************************/
  /***************************************************/
  /* Family Detail */
  readonly scheduleAttendanceFD: string = "Atendimentos agendados"
  readonly newAddress: string = "Nova endereço";
  readonly hcProgram: string = "Programa criança feliz"
  readonly references: string = "Referenciamentos"
  readonly routes: string = "Encaminhamentos"
  readonly notificationFD: string = "Notificação - conselho tutelar"
  readonly termFD: string = "Termo de declaração - conselho tutelar"
  readonly demandFD: string = "Demanda - conselho tutelar"
  readonly visits: string = "Visitas"
  readonly milkFD: string = "Leite fome zero"
  readonly attendanceFD: string = "Atendimentos"
  readonly noNotes: string = "Ainda não foram feitas anotações"
  readonly processing: string = "Processando...";
  readonly threeDaysElasped: string = "Informações não podem mais serem alteradas";
  readonly sendNote: string = "Enviar nota"


  /***************************************************/
  /***************************************************/
  /* MAPS */
  readonly familyLocal: string = "Localicação da família"
  readonly newLocal: string = "Nova localicação"
  readonly newLocationChose: string = 'Nova localicação definida!'

  /* Repeated on Some Services */
  readonly member: string = "Membro"
  readonly schooling: string = "Escolaridade"
  readonly access: string = "Forma de acesso"
  readonly detail: string = "Detalhe"
  readonly type: string = "Tipo"
  readonly school: string = "Escola"
  readonly gender: string = "Sexo"
  readonly ref: string = "Referência"
  readonly number: string = "Número"
  readonly cep: string = "CEP"
  readonly district: string = "Bairro"
  readonly address: string = "Endereço"
  readonly city: string = "Cidade"
  readonly state: string = "Estado"
  readonly date: string = "Data"
  readonly name: string = "Nome"
  readonly familyResponsable: string = "Responsável familiar"
  readonly offIn: string = "Desligado em:"
  readonly reviewedBy: string = "Revisado por:";
  readonly in: string = "em"
  readonly result: string = "Resultado"
  readonly proceeds: string = "Procede?"
  readonly proceed: string = "Proceder"
  readonly yes: string = "Sim"
  readonly no: string = "Não";
  readonly x: string = "X";
  readonly ascertained: string = "Averiguado"
  readonly noNis: string = "NIS não cadastrado";
  /* Acessuas */
  readonly acessuas: string = "Acessuas"
  readonly income: string = "Renda familiar bruta"
  readonly localAc: string = "Local das atividades"
  readonly shift: string = "Turno"
  readonly situation: string = "Situação"
  readonly course: string = "Curso";
  readonly infoC: string = "Informações complementares"

  /* Endereço */
  /* UEHUEHUEHEU it have everything on another page */

  /* AEPETI */
  readonly aepeti: string = "AEPETI"
  readonly matriculate: string = "Matriculado"
  readonly frequency: string = "Frequência"
  readonly isSocialProgram: string = "Participa de algum programa social"
  readonly socialProgram: string = "Programa social"
  readonly workActivity: string = "Atividade laboral"
  readonly activity: string = "Atividade"
  readonly moneyDestiny: string = "Destino do dinheiro"
  readonly incomeO: string = "Renda"
  readonly period: string = "Período";
  readonly currentSchool: string = "Escola atual"
  readonly shiftWork: string = "Turno que trabalha"
  readonly shiftStudy: string = "Turno que estuda"

  /* Attendance */
  readonly attendance: string = "Atendimento"
  readonly attendanceDate: string = "Data de atendimento"
  readonly attendanceType: string = "Tipo de atendimento"
  readonly attendedPerson: string = "Familiar antendido"
  readonly personGroup: string = "Grupo familiar"
  readonly input: string = "Entrada"
  readonly output: string = "Saída"
  readonly specifyAttendance: string = "Especificação do atendimento"

  /* Benefit  */
  readonly benefit: string = "Novo benefício"
  readonly benefitDate: string = "Data de concessão"
  readonly requestDate: string = "Data de solicitação"
  readonly benefitType: string = "Tipo de benefício"

  /* Complaint  */
  readonly complaints: string = "Denúncias"
  readonly complaint: string = "Denúncia"

  /* Condition */
  readonly condition: string = "Condicionalidade"
  readonly psf: string = "PSF - Programa da Saúde da Família"
  readonly effects: string = "Efeitos"
  readonly resource: string = "Recurso"

  /* Demand */
  readonly demand: string = "Demanda"
  readonly demandDate: string = "Data da demanda"
  readonly range: string = "Perfil da criança/adolescente"
  readonly responsableRange: string = "Responsável pela criança/adolescente"

  /* Family */
  readonly fullName: string = "Nome completo"
  readonly nickname: string = "Apelido"
  readonly birthday: string = "Nascimento"
  readonly phone: string = "Telefone"
  readonly nis: string = "NIS"
  readonly cpf: string = "CPF"
  readonly rg: string = "RG"
  readonly complement: string = "Complemento"
  readonly male: string = "Masculino"
  readonly female: string = "Feminino"
  readonly job: string = "Profissão"
  readonly age: string = "Idade";
  readonly sheet: string = 'Ficha';

  /* FPGB */
  readonly fpgb: string = "FPGB"
  readonly observation: string = "Observação"
  readonly request: string = "Solicitação"
  readonly fpgbNumber: string = "Número do FPGB"
  readonly dispatch: string = "Envio"
  readonly return: string = "Retorno"

  /* FSP */
  readonly fsp: string = "Programa acompanhamento familiar"
  readonly force: string = "Pontencialidades"
  readonly weakness: string = "Vunerabilidades"
  readonly objective: string = "Objetivos"
  readonly promise: string = "Compromissos"
  readonly nextAttendance: string = "Próximo atendimento";
  readonly needAddRerefence: string = 'É preciso fazer o Referenciamento antes de fazer o PAF';
  readonly needHaveRefOnCras: string = 'É preciso fazer o Rerenviamento no CRAS';

  /* HC */
  readonly hc: string = "Criança feliz"
  readonly usedObjects: string = "Matériais utilizados";
  readonly doActivity: string = "Atividade a ser realizada";
  readonly needs: string = "Necessidades da família";
  readonly haveNoneAble: string = 'Nenhum membro pode ser cadastrado nesse serviço';
  readonly listNotes: string = "Listar anotações"
  readonly visitPlan: string = "Plano de visita"

  /* Interview */
  readonly interview: string = "Entrevista"
  readonly interviewDate: string = "Data de entrevista"
  readonly goal: string = "Finalidade"
  readonly modality: string = "Modalidade"
  readonly interviewer: string = "Entrevistador"
  readonly add: string = "Adicionar";

  /* Milk */
  readonly milk: string = "Leite fome zero"
  readonly registrationDate: string = "Data de cadastro"
  readonly removeOfProgram: string = "Remover do programa"

  /* Note */
  readonly note: string = "Anotações"
  readonly aboutFamily: string = "Sobre a família"

  /* Notification */
  readonly notification: string = "Notificações"
  readonly bookNumber: string = "Nº caso/livro"
  readonly notificationNumber: string = "Nº notificação"

  /* Old */
  readonly old: string = "Serviço ao idoso"
  readonly noOld: string = "Não existe pessoa considerada idosa nessa família"
  readonly service: string = "Serviço"

  /* Person */
  readonly familyComposition: string = "Composição familiar"
  readonly kinship: string = 'Parentesco';
  readonly delete: string = 'Excluir';
  readonly deleted: string = 'Excluído';
  readonly warning: string = 'Aviso!';
  readonly warningDelPerson: string = 'Deseja realmente apagar esse membro da familia?';
  readonly hasResponsible: string = 'Responsável já cadastrado';
  readonly registeredDoc: string = 'documento já cadastrado';

  /* Pendency */
  readonly pendency: string = "Pendência";
  readonly solved: string = "Resolvido";

  /* Reference */
  readonly reference: string = "Referenciamento"
  readonly suasNumber: string = "Número pnt. SUAS"
  readonly referenceDate: string = "Data do referenciamento"
  readonly familyReferenceCourse: string = "Curso de referência da família";
  readonly familyReceives: string = "Família recebe BPC"
  readonly hasDeficiency: string = "Tem deficiência";
  readonly haveOne: string = 'Já há um ';

  /* Request */
  readonly requestPage: string = "TEM NADA AQUI AINDA"

  /* SCFV */
  readonly scfv: string = "SCFV";
  readonly noCores: string = 'Não ha núcleos cadastrados';

  /* ScheduleAttendance */
  readonly scheculeAttendance: string = "Agendar atendimento"

  /* Term */
  readonly term: string = "Termo de declaração";
  readonly registered: string = 'Registrado';

  /* Visit */
  readonly register: string = "Registrar";
  readonly visit: string = "Visita";
  readonly visitComplete: string = "Visita realizada";

  readonly visitFail: string = "Visita não realizada";
  readonly cancelVisit: string = "Cancelar Visita"
  readonly activeSearch: string = "Busca ativa"
  readonly percapita: string = "Renda percapita"
  readonly incomeSource: string = "Fonte de renda"
  readonly visitType: string = "Tipo da visita"
  readonly public: string = "Público"
  readonly visitor: string = "Visitador"
  readonly visitDate: string = "Data da visita"
  readonly professional: string = "Profissional"

  /* Route */
  readonly route: string = "Encaminhamento"
  readonly destiny: string = "Destino"
  readonly attachment: string = "Anexo"
  readonly reason = "Motivo"
  readonly chooseFile: string = "Escolher arquivo"
  readonly response: string = "Resposta"
  readonly inAnalyze: string = "Em análise"
  readonly attended: string = "Atendido"
  readonly openFamily: string = "Abrir familia"

  /* Group */
  readonly group: string = "Grupo"
  readonly groupName: string = "Nome do grupo"
  readonly noGroups: string = 'Não há grupos cadastrados';
  readonly noMembers: string = 'Não há pessoas cadastradas';
  readonly listGroups: string = 'Lista de grupos';
  readonly familyCode: string = 'Código familiar';
  /***************************************************/
  /***************************************************/

  constructor() { }

}
