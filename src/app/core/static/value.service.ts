import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValueService {
  readonly LIMIT_TWENTY: number = 20;
  readonly LIMIT_TWO: number = 2;
  readonly LIMIT_FOUR: number = 4;
  constructor() {}
}
