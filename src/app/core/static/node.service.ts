import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NodeService {
  readonly serviceAttendance: string = 'service-attendance'
  readonly notes: string = 'notes'
  readonly familyNotes: string = 'family-notes'
  readonly fspNotes: string = 'fsp-notes'
  readonly hcNotes: string = 'hc-notes'
  readonly visitNotes: string = 'visit-notes'
  readonly services: string = 'services';
  readonly families: string = 'families'
  readonly serviceRoute: string = 'service-route'
  readonly serviceSCFV: string = 'service-scfv'
  readonly serviceMilk: string = 'service-milk'
  readonly serviceFpgb: string = 'service-fpgb'
  readonly serviceInterview: string = 'service-interview'
  readonly serviceComplaint: string = 'service-complaint';
  readonly servicePendency: string = 'service-pendency';
  readonly serviceCondition: string = 'service-condition'
  readonly serviceAepeti: string = 'service-aepeti'
  readonly serviceAcessuas: string = 'service-acessuas'
  readonly serviceDemand: string = 'service-demand'
  readonly serviceTerm: string = 'service-term'
  readonly serviceNotification: string = 'service-notification'
  readonly serviceOld: string = 'service-old'
  readonly serviceVisit: string = 'service-visit'
  readonly serviceReference: string = 'service-reference'
  readonly serviceBenefit: string = 'service-benefit'
  readonly serviceGroup: string = 'service-group'
  readonly serviceFsp: string = 'service-fsp'
  readonly serviceSchedule: string = 'service-schedule';
  readonly serviceHc: string = 'service-hc';
  readonly location: string = 'location';
  readonly address: string = 'address';
  readonly people: string = 'people';
  readonly groups: string = 'groups';
  readonly groupPeople: string = 'group-people';
  readonly corePeople: string = 'core-people';
  //readonly group: string = 'group';
  readonly cores: string = 'cores';
  //readonly core: string = 'core';
  readonly cities: string = 'cities';
  readonly static: string = 'static';
  readonly text: string = 'text';
  readonly city: string = 'city';
  readonly districts: string = 'districts';
  readonly users: string = 'users';
  readonly meeting: string = 'meeting';
  readonly groupMeeting: string = 'group-meeting';
  readonly coreMeeting: string = 'core-meeting';
  
  constructor() { }
}
