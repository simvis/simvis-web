import { Component, OnInit } from '@angular/core';
import { ActionService } from '../core/util/action.service';
import { StringService } from '../core/static/string.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(public act: ActionService, 
    public str: StringService) {
  }

  ngOnInit() {
    this.act.onInitHome();
  }
}
