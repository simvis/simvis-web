import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActionService } from '../../core/util/action.service';
import { ArrayService } from '../../core/static/array.service';
import { StringService } from '../../core/static/string.service';
import { AlertComponent } from 'ngx-bootstrap';

@Component({
  selector: 'app-google-map',
  templateUrl: './google-map.component.html',
  styleUrls: ['./google-map.component.css']
})

export class GoogleMapComponent implements OnInit {

  @Output()
  readonly confirm = new EventEmitter<boolean>();  

  constructor(public act: ActionService, public arr: ArrayService, public str: StringService) {
  }

  ngOnInit() {
  
  }

  onSubmit(valid?: boolean): void {
    this.confirm.emit(true);
    // if(valid) {
    //   this.confirm.emit(true);
    // } else {
    //   this.act.onCancel();
    // }
  }

  
  
}
