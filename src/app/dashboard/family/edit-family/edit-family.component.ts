import { Component, OnInit } from '@angular/core';
import { TextService } from '../../../core/static/text.service';
import { ActionService } from '../../../core/util/action.service';
import { StringService } from '../../../core/static/string.service';
import { ArrayService } from 'src/app/core/static/array.service';

@Component({
  selector: 'app-edit-family',
  templateUrl: './edit-family.component.html',
  styleUrls: ['./edit-family.component.css']
})
export class EditFamilyComponent implements OnInit {

  constructor(public act: ActionService, 
              public text: TextService,
              public str: StringService,
              public arr: ArrayService) { 
  }

  ngOnInit() {
    this.act.onInitEditFamily();
  }

}
