import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TextService } from '../../../core/static/text.service';
import { StringService } from '../../../core/static/string.service';
import { ArrayService } from '../../../core/static/array.service';
import { ActionService } from '../../../core/util/action.service';

@Component({
  selector: 'app-edit-term',
  templateUrl: './edit-term.component.html',
  styleUrls: ['./edit-term.component.css']
})
export class EditTermComponent implements OnInit {

  @Output()
  readonly confirm = new EventEmitter<boolean>();

  constructor(public act: ActionService, public arr: ArrayService, public text: TextService, public str: StringService) { }

  ngOnInit() {
    //Delete after
  }

  onSubmit(valid: boolean): void {
    if(valid) {
      this.confirm.emit(true);
    } else {
      this.act.onCancel();
    }
  }
}
