import { Component, OnInit } from '@angular/core';
import { TextService } from '../../../core/static/text.service';
import { ActionService } from '../../../core/util/action.service';
import { ArrayService } from '../../../core/static/array.service';
import { StringService } from '../../../core/static/string.service';
import { AuthService } from 'src/app/core/auth.service';

@Component({
  selector: 'app-menu-family',
  templateUrl: './menu-family.component.html',
  styleUrls: ['./menu-family.component.css']
})
export class MenuFamilyComponent implements OnInit {

  constructor(public auth: AuthService, public act: ActionService, public arr: ArrayService, public text: TextService, public str: StringService) { }

  ngOnInit() {
  }

}
