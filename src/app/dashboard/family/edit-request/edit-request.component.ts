import { Component, OnInit } from '@angular/core';
import { TextService } from '../../../core/static/text.service';
import { ActionService } from 'src/app/core/util/action.service';

@Component({
  selector: 'app-edit-request',
  templateUrl: './edit-request.component.html',
  styleUrls: ['./edit-request.component.css']
})
export class EditRequestComponent implements OnInit {

  constructor(public text: TextService, public act: ActionService) { }

  ngOnInit() {
  }

}
