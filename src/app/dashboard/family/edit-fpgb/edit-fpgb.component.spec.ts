import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditFpgbComponent } from './edit-fpgb.component';

describe('EditFpgbComponent', () => {
  let component: EditFpgbComponent;
  let fixture: ComponentFixture<EditFpgbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditFpgbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditFpgbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
