import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { StringService } from '../../../core/static/string.service';
import { TextService } from '../../../core/static/text.service';
import { ArrayService } from '../../../core/static/array.service';
import { ActionService } from '../../../core/util/action.service';


@Component({
  selector: 'app-edit-attendance',
  templateUrl: './edit-attendance.component.html',
  styleUrls: ['./edit-attendance.component.css']
})
export class EditAttendanceComponent implements OnInit {

  @Output()
  readonly confirm = new EventEmitter<boolean>();

  constructor(public act: ActionService, public arr: ArrayService, public text: TextService, public str: StringService) { 

  }

  ngOnInit() {
  }
  
  onSubmit(valid: boolean): void {
    if(valid) {
      this.confirm.emit(true);
    } else {
      this.act.onCancel();
      this.act.alertFeedback(this.text.noneAction);

    }
  }

}