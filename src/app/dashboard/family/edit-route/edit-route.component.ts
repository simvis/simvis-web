import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TextService } from '../../../core/static/text.service';
import { ActionService } from '../../../core/util/action.service';
import { ArrayService } from '../../../core/static/array.service';
import { StringService } from '../../../core/static/string.service';
import { AuthService } from 'src/app/core/auth.service';

@Component({
  selector: 'app-edit-route',
  templateUrl: './edit-route.component.html',
  styleUrls: ['./edit-route.component.css']
})
export class EditRouteComponent implements OnInit {

  @Output()
  readonly confirm = new EventEmitter<boolean>();  
  
  constructor(public act: ActionService, 
              public auth: AuthService,
              public arr: ArrayService,
              public text: TextService, 
              public str: StringService) {

  }

  ngOnInit() {
  }

  onSubmit(valid: boolean): void {
    if(valid) {
      this.confirm.emit(true);
    } else {
      this.act.onCancel();
      this.act.alertFeedback(this.text.noneAction);

    }
  }

}
