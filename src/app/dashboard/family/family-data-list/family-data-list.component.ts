import { Component, OnInit } from '@angular/core';
import { ActionService } from '../../../core/util/action.service';
import { TextService } from '../../../core/static/text.service';
import { StringService } from '../../../core/static/string.service';
import { AuthService } from '../../../core/auth.service';
import { ActivatedRoute } from '@angular/router';
import { ArrayService } from 'src/app/core/static/array.service';

@Component({
  selector: 'app-family-data-list',
  templateUrl: './family-data-list.component.html',
  styleUrls: ['./family-data-list.component.css']
})
export class FamilyDataListComponent implements OnInit {

  constructor(public act: ActionService,
    public arr: ArrayService,
    public text: TextService,
    public str: StringService,
    public auth: AuthService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.act.onInitFamilyDataList(this.route);
  }

}
