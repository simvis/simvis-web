import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FamilyDataListComponent } from './family-data-list.component';

describe('FamilyDataListComponent', () => {
  let component: FamilyDataListComponent;
  let fixture: ComponentFixture<FamilyDataListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FamilyDataListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FamilyDataListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
