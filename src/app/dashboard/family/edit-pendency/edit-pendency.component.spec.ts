import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPendencyComponent } from './edit-pendency.component';

describe('EditPendencyComponent', () => {
  let component: EditPendencyComponent;
  let fixture: ComponentFixture<EditPendencyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPendencyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPendencyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
