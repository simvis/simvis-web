import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TextService } from '../../../core/static/text.service';
import { ActionService } from '../../../core/util/action.service';
import { ArrayService } from '../../../core/static/array.service';
import { StringService } from '../../../core/static/string.service';

@Component({
  selector: 'app-edit-pendency',
  templateUrl: './edit-pendency.component.html',
  styleUrls: ['./edit-pendency.component.css']
})
export class EditPendencyComponent implements OnInit {

  @Output()
  readonly confirm = new EventEmitter<boolean>();

  constructor(public act: ActionService, public arr: ArrayService, public text: TextService, public str: StringService) {

  }

  ngOnInit() {
  }

  onSubmit(valid: boolean): void {
    if(valid) {
      this.confirm.emit(true);
    } else {
      this.act.onCancel();
    }
  }
}
