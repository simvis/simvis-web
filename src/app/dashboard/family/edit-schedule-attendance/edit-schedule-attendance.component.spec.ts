import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditScheduleAttendanceComponent } from './edit-schedule-attendance.component';

describe('EditScheduleAttendanceComponent', () => {
  let component: EditScheduleAttendanceComponent;
  let fixture: ComponentFixture<EditScheduleAttendanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditScheduleAttendanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditScheduleAttendanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
