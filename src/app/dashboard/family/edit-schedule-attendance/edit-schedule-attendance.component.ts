import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Person } from '../../../model/person';
import { TextService } from '../../../core/static/text.service';
import { ActionService } from '../../../core/util/action.service';
import { ArrayService } from '../../../core/static/array.service';
import { StringService } from '../../../core/static/string.service';

@Component({
  selector: 'app-edit-schedule-attendance',
  templateUrl: './edit-schedule-attendance.component.html',
  styleUrls: ['./edit-schedule-attendance.component.css']
})
export class EditScheduleAttendanceComponent implements OnInit {

  @Output()
  readonly confirm = new EventEmitter<boolean>();  

  constructor(public act: ActionService, public arr: ArrayService, public text: TextService, public str: StringService) {
  }

  ngOnInit() {

  }

  onSubmit(valid: boolean): void {
    if(valid) {
      this.confirm.emit(true);
    } else {
      this.act.onCancel();
    }
  }
}
