import { Component, OnInit, Input } from '@angular/core';
import { Service } from '../../../../model/service';
import { TextService } from '../../../../core/static/text.service';
import { StringService } from '../../../../core/static/string.service';
import { ArrayService } from 'src/app/core/static/array.service';
import { ActionService } from 'src/app/core/util/action.service';

@Component({
  selector: 'app-item-service',
  templateUrl: './item-service.component.html',
  styleUrls: ['./item-service.component.css']
})
export class ItemServiceComponent implements OnInit {

  @Input()
  service: Service
  @Input()
  type: string
  
  constructor(public str: StringService,
    public act: ActionService, 
    public textService: TextService, 
    public arr: ArrayService) { }

  ngOnInit() {
  }

}
