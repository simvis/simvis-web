import { Component, OnInit, Input } from '@angular/core';
import { Note } from '../../../../model/note';
import { TextService } from '../../../../core/static/text.service';
import { ArrayService } from 'src/app/core/static/array.service';

@Component({
  selector: 'app-item-note',
  templateUrl: './item-note.component.html',
  styleUrls: ['./item-note.component.css']
})
export class ItemNoteComponent implements OnInit {

  @Input()
  note: Note

  constructor(public text: TextService, 
    public arr: ArrayService) { }

  ngOnInit() {
  }

}
