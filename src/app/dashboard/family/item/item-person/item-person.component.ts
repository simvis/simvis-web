import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Person } from '../../../../model/person';
import { ActionService } from 'src/app/core/util/action.service';
import { ArrayService } from 'src/app/core/static/array.service';
import { TextService } from 'src/app/core/static/text.service';

@Component({
  selector: 'app-item-person',
  templateUrl: './item-person.component.html',
  styleUrls: ['./item-person.component.css']
})
export class ItemPersonComponent implements OnInit {

  @Input()
  person: Person

  constructor(public act: ActionService, 
    public text: TextService,
    public arr: ArrayService) { }

  ngOnInit() {

  }

}
