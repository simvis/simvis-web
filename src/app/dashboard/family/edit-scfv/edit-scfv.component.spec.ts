import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditScfvComponent } from './edit-scfv.component';

describe('EditScfvComponent', () => {
  let component: EditScfvComponent;
  let fixture: ComponentFixture<EditScfvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditScfvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditScfvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
