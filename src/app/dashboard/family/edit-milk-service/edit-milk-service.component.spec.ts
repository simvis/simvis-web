import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMilkServiceComponent } from './edit-milk-service.component';

describe('EditMilkServiceComponent', () => {
  let component: EditMilkServiceComponent;
  let fixture: ComponentFixture<EditMilkServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditMilkServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMilkServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
