import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditOldServiceComponent } from './edit-old-service.component';

describe('EditOldServiceComponent', () => {
  let component: EditOldServiceComponent;
  let fixture: ComponentFixture<EditOldServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditOldServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditOldServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
