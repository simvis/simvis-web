import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TextService } from '../../../core/static/text.service';
import { ActionService } from '../../../core/util/action.service';
import { ArrayService } from '../../../core/static/array.service';
import { StringService } from '../../../core/static/string.service';

@Component({
  selector: 'app-edit-old-service',
  templateUrl: './edit-old-service.component.html',
  styleUrls: ['./edit-old-service.component.css']
})
export class EditOldServiceComponent implements OnInit {

  @Output()
  readonly confirm = new EventEmitter<boolean>();
  
  //olds: Person[];
 
  constructor(public act: ActionService, public arr: ArrayService, public text: TextService, public str: StringService) { }

  ngOnInit() {
  }

  onSubmit(valid: boolean): void {
    if(valid) {
      this.confirm.emit(true);
    } else {
      this.act.onCancel();
      this.act.alertFeedback(this.text.noneAction);
    }
  }
}
