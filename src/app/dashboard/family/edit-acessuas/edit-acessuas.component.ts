import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ActionService } from '../../../core/util/action.service';
import { ArrayService } from '../../../core/static/array.service';
import { TextService } from '../../../core/static/text.service';
import { StringService } from '../../../core/static/string.service';

@Component({
  selector: 'app-edit-acessuas',
  templateUrl: './edit-acessuas.component.html',
  styleUrls: ['./edit-acessuas.component.css']
})
export class EditAcessuasComponent implements OnInit {

  @Output()
  readonly confirm = new EventEmitter<boolean>();
  
  constructor(public act: ActionService, public arr: ArrayService, public text: TextService, public str: StringService) { }

  ngOnInit() {

  }

   onSubmit(valid: boolean): void {
    if(valid) {
      this.confirm.emit(true);
    } else {
      this.act.onCancel();
    }
  }

}
