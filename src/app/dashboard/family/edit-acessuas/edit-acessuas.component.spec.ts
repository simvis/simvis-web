import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAcessuasComponent } from './edit-acessuas.component';

describe('EditAcessuasComponent', () => {
  let component: EditAcessuasComponent;
  let fixture: ComponentFixture<EditAcessuasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAcessuasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAcessuasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
