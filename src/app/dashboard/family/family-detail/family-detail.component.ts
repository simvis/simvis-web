import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { ActionService } from '../../../core/util/action.service';
import { TextService } from '../../../core/static/text.service';
import { StringService } from '../../../core/static/string.service';
import { AuthService } from 'src/app/core/auth.service';
import { ArrayService } from 'src/app/core/static/array.service';

@Component({
  selector: 'app-family-detail',
  templateUrl: './family-detail.component.html',
  styleUrls: ['./family-detail.component.css']
})
export class FamilyDetailComponent implements OnInit {

  constructor(private route: ActivatedRoute,
    public arr: ArrayService,
    public act: ActionService,
    public text: TextService,
    public str: StringService,
    public auth: AuthService) { }

  ngOnInit() {
    this.act.getPageParams(this.route, this.str.familyDetail);
  }
}
