import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAepetiComponent } from './edit-aepeti.component';

describe('EditAepetiComponent', () => {
  let component: EditAepetiComponent;
  let fixture: ComponentFixture<EditAepetiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAepetiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAepetiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
