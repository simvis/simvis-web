import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditFspComponent } from './edit-fsp.component';

describe('EditFspComponent', () => {
  let component: EditFspComponent;
  let fixture: ComponentFixture<EditFspComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditFspComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditFspComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
