import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { StringService } from '../../../core/static/string.service';
import { ArrayService } from '../../../core/static/array.service';
import { TextService } from '../../../core/static/text.service';
import { ActionService } from '../../../core/util/action.service';

@Component({
  selector: 'app-edit-fsp',
  templateUrl: './edit-fsp.component.html',
  styleUrls: ['./edit-fsp.component.css']
})
export class EditFspComponent implements OnInit {
 
  @Output()
  readonly confirm = new EventEmitter<boolean>();
  
  constructor(public act: ActionService, public arr: ArrayService, public text: TextService, public str: StringService) { 

  }
  
  ngOnInit() {
  }
  
  onSubmit(valid: boolean): void {
    if(valid) {
      this.confirm.emit(true);
    } else {
      this.act.onCancel();
    }
  }
}
