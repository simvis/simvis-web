import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { StringService } from '../../../core/static/string.service';
import { TextService } from '../../../core/static/text.service';
import { ArrayService } from '../../../core/static/array.service';
import { ActionService } from '../../../core/util/action.service';
import { AuthService } from 'src/app/core/auth.service';

@Component({
  selector: 'app-edit-happy-child',
  templateUrl: './edit-happy-child.component.html',
  styleUrls: ['./edit-happy-child.component.css']
})
export class EditHappyChildComponent implements OnInit {

  @Output()
  readonly confirm = new EventEmitter<boolean>();  
  name = 'Nested template driven form';
  
  constructor(public act: ActionService,
    public text: TextService, 
    public arr: ArrayService, 
    public textService: TextService, 
    public str: StringService,
    public auth: AuthService) { }

  ngOnInit() {
  }


  onSubmit(valid: boolean): void {
    if(valid) {
      this.confirm.emit(true);
    } else {
      this.act.onCancel();
      this.act.alertFeedback(this.text.noneAction);
    }
  }

}
