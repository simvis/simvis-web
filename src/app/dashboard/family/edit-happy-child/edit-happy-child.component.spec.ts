import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditHappyChildComponent } from './edit-happy-child.component';

describe('EditHappyChildComponent', () => {
  let component: EditHappyChildComponent;
  let fixture: ComponentFixture<EditHappyChildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditHappyChildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditHappyChildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
