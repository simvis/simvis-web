import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TextService } from '../../../core/static/text.service';
import { ArrayService } from '../../../core/static/array.service';
import { ActionService } from '../../../core/util/action.service';
import { StringService } from '../../../core/static/string.service';

@Component({
  selector: 'app-edit-note',
  templateUrl: './edit-note.component.html',
  styleUrls: ['./edit-note.component.css']
})
export class EditNoteComponent implements OnInit {
  @Output()
  readonly confirm = new EventEmitter<boolean>();
  

  constructor(public act: ActionService, public arr: ArrayService, public text: TextService, public str: StringService) { }

  ngOnInit() {
  }

  onSubmit(valid: boolean): void {
    valid ? this.confirm.emit(true) : this.act.onCancel();
   /*  if(valid) {
      this.confirm.emit(true);
    } else {
      this.act.onCancel();
    } */
  }
}
