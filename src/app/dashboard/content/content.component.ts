import { Component, OnInit } from '@angular/core';
import { ActionService } from '../../core/util/action.service';
import { ArrayService } from '../../core/static/array.service';
import { StringService } from '../../core/static/string.service';
import { AuthService } from 'src/app/core/auth.service';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  constructor(public auth: AuthService, public act: ActionService, public arr: ArrayService, public str: StringService) { }

  ngOnInit() {

    this.act.onInitDashboardContent();
  }

}
