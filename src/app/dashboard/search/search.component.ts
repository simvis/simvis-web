import { Component, OnInit } from '@angular/core';
import { ActionService } from 'src/app/core/util/action.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  constructor(public act: ActionService) { }

  ngOnInit() {
  }

}
