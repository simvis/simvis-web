import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/auth.service';
import { ActionService } from '../../core/util/action.service';
import { StringService } from 'src/app/core/static/string.service';
import { ArrayService } from 'src/app/core/static/array.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  constructor(public act: ActionService, 
    public auth: AuthService,
    public str: StringService,
    public arr: ArrayService) {
    
  }

  ngOnInit() {
    // this.act.onInitUserProfile();
  
  }

}



