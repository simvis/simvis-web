import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ActionService } from '../../core/util/action.service';
import { ArrayService } from '../../core/static/array.service';
import { StringService } from '../../core/static/string.service';

@Component({
  selector: 'app-new-core',
  templateUrl: './new-core.component.html',
  styleUrls: ['./new-core.component.css']
})
export class NewCoreComponent implements OnInit {

  @Output()
  readonly confirm = new EventEmitter<boolean>();
  
  constructor(public act: ActionService, public arr: ArrayService, public str: StringService) { }

  ngOnInit() {

  }

  onSubmit(valid: boolean): void {
    if(valid) {
      this.confirm.emit(true);
    } else {
      this.act.onCancel();
    }
  }
}