import { Component, OnInit } from '@angular/core';
import { ActionService } from 'src/app/core/util/action.service';
import { ArrayService } from 'src/app/core/static/array.service';
import { TextService } from 'src/app/core/static/text.service';
import { StringService } from 'src/app/core/static/string.service';

@Component({
  selector: 'app-meet-scfv',
  templateUrl: './meet-scfv.component.html',
  styleUrls: ['./meet-scfv.component.css']
})
export class MeetScfvComponent implements OnInit {

  constructor(public act: ActionService, public arr: ArrayService, public text: TextService, public str: StringService) {

  }


  ngOnInit() {
  }

}
