import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetScfvComponent } from './meet-scfv.component';

describe('MeetScfvComponent', () => {
  let component: MeetScfvComponent;
  let fixture: ComponentFixture<MeetScfvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetScfvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetScfvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
