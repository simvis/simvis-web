import { Component, OnInit } from '@angular/core';
import { ActionService } from 'src/app/core/util/action.service';
import { TextService } from 'src/app/core/static/text.service';
import { StringService } from 'src/app/core/static/string.service';

@Component({
  selector: 'app-report-scfv',
  templateUrl: './report-scfv.component.html',
  styleUrls: ['./report-scfv.component.css']
})
export class ReportScfvComponent implements OnInit {

  constructor(public act: ActionService, public text: TextService, public str: StringService) {
    
    
  }

  ngOnInit() {
  }

}
