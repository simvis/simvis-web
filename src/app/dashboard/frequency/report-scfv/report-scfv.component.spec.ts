import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportScfvComponent } from './report-scfv.component';

describe('ReportScfvComponent', () => {
  let component: ReportScfvComponent;
  let fixture: ComponentFixture<ReportScfvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportScfvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportScfvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
