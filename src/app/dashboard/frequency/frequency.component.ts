import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ActionService } from '../../core/util/action.service';
import { ArrayService } from '../../core/static/array.service';
import { StringService } from '../../core/static/string.service';
import { NodeService } from 'src/app/core/static/node.service';

@Component({
  selector: 'app-frequency',
  templateUrl: './frequency.component.html',
  styleUrls: ['./frequency.component.css']
})
export class FrequencyComponent implements OnInit {

  @Output()
  readonly confirm = new EventEmitter<boolean>();

  constructor(public act: ActionService,
     public arr: ArrayService, 
     public str: StringService, 
     public node: NodeService) {

  }

  ngOnInit() {
    this.act.onInitFrequency();
  }

  onSubmit(valid: boolean): void {
    if(valid) {
      this.confirm.emit(true);
    } else {
      this.act.onCancel();
    }
  }
}
