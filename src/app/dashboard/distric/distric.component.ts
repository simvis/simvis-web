import { Component, OnInit } from '@angular/core';
import { ActionService } from '../../core/util/action.service';
import { ArrayService } from '../../core/static/array.service';
import { StringService } from '../../core/static/string.service';
import { AuthService } from '../../core/auth.service';

@Component({
  selector: 'app-distric',
  templateUrl: './distric.component.html',
  styleUrls: ['./distric.component.css']
})
export class DistricComponent implements OnInit {

  constructor(public auth: AuthService, public act: ActionService, public arr: ArrayService, public str: StringService) { }
  
  ngOnInit() {
    this.act.onInitPageContent(this.str.editDistrict);
  }

}
