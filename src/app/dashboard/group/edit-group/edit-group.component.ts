import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../core/auth.service';
import { Group } from '../../../model/group';
import { TextService } from '../../../core/static/text.service';
import { CrudService } from '../../../core/database/crud.service';
import { ActionService } from 'src/app/core/util/action.service';
import { StringService } from 'src/app/core/static/string.service';
import { NodeService } from 'src/app/core/static/node.service';

@Component({
  selector: 'app-edit-group',
  templateUrl: './edit-group.component.html',
  styleUrls: ['./edit-group.component.css']
})
export class EditGroupComponent implements OnInit {
  group: Group;
  groups: Group[];

  constructor(public text: TextService,
              public act: ActionService,
              public str: StringService,
              public node: NodeService) {               
  }

  ngOnInit() {
    this.act.onInitGroup();
    
  }

}
