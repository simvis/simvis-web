import { Component, OnInit } from '@angular/core';
import { TextService } from 'src/app/core/static/text.service';
import { ActionService } from 'src/app/core/util/action.service';
import { StringService } from 'src/app/core/static/string.service';

@Component({
  selector: 'app-meet',
  templateUrl: './meet.component.html',
  styleUrls: ['./meet.component.css']
})
export class MeetComponent implements OnInit {

  constructor(public text: TextService,
    public act: ActionService,
    public str: StringService) { }

  ngOnInit() {
    this.act.onInitGroupMeet();
  }

}
