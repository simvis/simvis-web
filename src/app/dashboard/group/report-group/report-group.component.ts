import { Component, OnInit } from '@angular/core';
import { ActionService } from 'src/app/core/util/action.service';
import { TextService } from 'src/app/core/static/text.service';
import { StringService } from 'src/app/core/static/string.service';

@Component({
  selector: 'app-report-group',
  templateUrl: './report-group.component.html',
  styleUrls: ['./report-group.component.css']
})
export class ReportGroupComponent implements OnInit {

  constructor(public act: ActionService, public text: TextService, public str: StringService) {
    
    
  }


  ngOnInit() {
  }

}
