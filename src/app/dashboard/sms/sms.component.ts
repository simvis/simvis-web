import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ActionService } from '../../core/util/action.service';
import { ArrayService } from '../../core/static/array.service';
import { StringService } from '../../core/static/string.service';

@Component({
  selector: 'app-sms',
  templateUrl: './sms.component.html',
  styleUrls: ['./sms.component.css']
})

export class SmsComponent implements OnInit {

  @Output()
  readonly confirm = new EventEmitter<boolean>();

  constructor(public act: ActionService, public arr: ArrayService, public str: StringService) {

  }

  ngOnInit() {

  }

  onSubmit(valid: boolean): void {
    if(valid) {
      this.confirm.emit(true);
    } else {
      this.act.onCancel();
    }
  }

}
