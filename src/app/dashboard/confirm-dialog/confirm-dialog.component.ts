import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Data } from '../../model/data';
import { ActionService } from '../../core/util/action.service';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.css']
})

export class ConfirmDialogComponent implements OnInit {

  @Input()
  title: string;

  @Input()
  message: string;

  @Output()
  readonly confirm = new EventEmitter<boolean>();
  
  constructor(public act: ActionService) {
  }

  ngOnInit() {
    
  }

  ok(): void {
    this.confirm.emit(true);    
  }
}
