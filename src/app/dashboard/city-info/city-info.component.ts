import { Component, OnInit } from '@angular/core';
import { ActionService } from 'src/app/core/util/action.service';
import { StringService } from 'src/app/core/static/string.service';

@Component({
  selector: 'app-city-info',
  templateUrl: './city-info.component.html',
  styleUrls: ['./city-info.component.css']
})
export class CityInfoComponent implements OnInit {

  constructor(public act: ActionService, public str: StringService) { }

  ngOnInit() {
    this.act.onInitCity();
  }
  
  onSubmit(valid: boolean): void {
    // if(valid) {
    //   this.act.onConfirm(this.str.editCity);
    // }
    valid ? this.act.onConfirm(this.str.editCity) : this.act.onCancel();
  }
}

