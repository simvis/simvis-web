import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { AuthService } from '../core/auth.service';
import { TextService } from '../core/static/text.service';
import { ActionService } from '../core/util/action.service';
import { ArrayService } from '../core/static/array.service';
import { StringService } from '../core/static/string.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  @Output()
  readonly confirm = new EventEmitter<boolean>();

  constructor(public auth: AuthService,
    public text: TextService,
    public act: ActionService,
    public arr: ArrayService,
    public str: StringService) {
      act.onInitDashboard()
     }

  ngOnInit() {

  }

  onSubmit(valid: boolean): void {
    if(valid) {
      this.confirm.emit(true);
    } else {
      this.act.onCancel();
    }
  }
}
