// import { Injectable } from '@angular/core';
// import { HttpClient, HttpParams } from '@angular/common/http';
// import * as firebase from 'firebase';
// import 'firebase/firestore';
// import 'firebase/storage';
// import { Address } from '../../model/address';
// import { Person } from '../../model/person';
// import { environment } from '../../../environments/environment';
// import { Observable } from 'rxjs';
// import { Note } from '../../model/note';
// import { Service } from '../../model/service';
// import { Group } from '../../model/group';
// import { AuthService } from '../../core/auth.service';
// import { $nodeNotes, $nodeFamilies, $nodeAddress, $nodePeople, $nodeGroups, $nodeCities } from '../../../environments/strings';
// import { NodeService } from '../../core/static/node.service';
// import { District } from '../../model/district';


// /*
// //Firebase rules
// match /users/{document} {
//   function getRole(role) {
//     return get(/databases/$(database)/documents/users/$(request.auth.uid)).data.roles[role]
//   }
  
//   allow read;
//   allow create: if request.resource.data.roles.keys().hasAny(['admin', 'editor']) == false;
//   allow update: if getRole('admin') == true;
// }
// }*/

// @Injectable({
//   providedIn: 'root'
// })

// export class FamilyService {
//   private db: firebase.firestore.Firestore;
//   constructor(private http: HttpClient,
//     private auth: AuthService,
//     private node: NodeService) {
//     this.db = firebase.firestore();
//   }

//   //Database
//   listGroup(code: string): firebase.firestore.Query {
//     return this.db.collection($nodeCities).doc(this.auth.user.city.code).collection($nodeGroups).where('city.code', '==', code);
//   }

//   listDistrict(): firebase.firestore.Query {
//     return this.db.collection(this.node.cities).doc(this.auth.user.city.code).collection(this.node.districts).where('city.code', '==', this.auth.user.city.code)
//   }

//   getNotes(uid: string): firebase.firestore.Query {
//     return this.db.collection($nodeFamilies).doc(uid).collection($nodeNotes).where('familyUid', '==', uid);
//   }
  
//   getGroup(uid: string): firebase.firestore.Query {
//     return this.db.collection($nodeCities).doc(this.auth.user.city.code).collection($nodeGroups).where('familyUid', '==', uid);
//   }

//   getPeople(uid: string): firebase.firestore.Query {
//     return this.db.collection('people').where('familyUid', '==', uid);
//   }

//   getAddress(uid: string): firebase.firestore.Query {
//     return this.db.collection($nodeFamilies).doc(uid).collection($nodeAddress).where('familyUid', '==', uid);
//   }

//   getService(uid: string, node: string): firebase.firestore.Query {
//     return this.db.collection(this.node.families).doc(uid).collection(node)
//               .where('familyUid', '==', uid)
//   }

//   updateAddressCoords(doc: Address): Promise<void> {
//       const coords = new firebase.firestore.GeoPoint(doc.coords._lat, doc.coords._long)

//       return this.db.collection(this.node.families).doc(doc.familyUid).collection(this.node.address).doc(doc.uid).update({coords: coords})
      
//   }

//   writePerson(doc: Person): Promise<void> {
//     let ref: firebase.firestore.CollectionReference
//     ref = this.db.collection($nodePeople)    
    
//     return this.write(doc, ref)
//   }

//   writeAddress(doc: Address): Promise<void> {
//     let ref: firebase.firestore.CollectionReference
//     ref = this.db.collection($nodeFamilies).doc(doc.familyUid).collection($nodeAddress);

//     return this.write(doc, ref)
//   }

//   write(doc: any, collection: firebase.firestore.CollectionReference): Promise<void> {
//     let docRef: firebase.firestore.DocumentReference
//     let data: any = (JSON.parse(JSON.stringify(doc)))
    
//     data.updateAt = firebase.firestore.FieldValue.serverTimestamp()

//     if (!data.uid) {
//       docRef = collection.doc();
//       data.uid = docRef.id
//       data.writeAt = firebase.firestore.FieldValue.serverTimestamp()
//     } else {
//       delete data.writeAt
//       docRef = collection.doc(data.uid);
//       return docRef.update(data)
//     }
//     console.log('WriteMethod', JSON.stringify(doc));
 
//     return docRef.set(data);
//   }

//   serviceRef(service: string): firebase.firestore.DocumentReference {
//     return this.db.collection(service).doc();
//   }

//   writeService(doc: Service, node: string): Promise<void> {
//     let ref: firebase.firestore.CollectionReference    
//     doc.user = this.auth.user
   
//     ref = this.db.collection(this.node.families).doc(doc.familyUid).collection(node)

//     return this.write(doc, ref)
//   }

//   writeNote(doc: Note): Promise<void> {
//     let ref: firebase.firestore.CollectionReference
//     ref = this.db.collection($nodeFamilies).doc(doc.familyUid).collection($nodeNotes)

//     return this.write(doc, ref);
//   }

//   writeGroup(doc: Group): Promise<void> {
//    let ref: firebase.firestore.CollectionReference
//     ref = this.db.collection($nodeCities).doc(this.auth.user.city.code).collection($nodeGroups)
    
//     return this.write(doc, ref);
//   }

  
//   writeDistrict(doc: District): Promise<void> {
//     let ref: firebase.firestore.CollectionReference
//      ref = this.db.collection(this.node.cities).doc(this.auth.user.city.code).collection(this.node.districts)
     
//      return this.write(doc, ref);
//   }

//   writeFamily(doc: any): Promise<void> {
//     const batch = this.db.batch();

//     console.log(JSON.stringify(doc))
//     let address: Address = doc.address
//     let person: Person = doc
    
//     let coords = {};
//     if (address.coords) {
//       coords = new firebase.firestore.GeoPoint(
//         address.coords._lat, address.coords._long
//       )
//     }

//     const addressString = address.street + ' '
//       + address.number + ' '
//       + address.complement + ', '
//       + address.district + ' - '
//       + address.cep + ', '
//       + address.city + ' - '
//       + address.state;

//     const familyRef = this.db.collection($nodeFamilies).doc();
//     const familyId = familyRef.id;

//     const addressRef = this.db.collection($nodeFamilies).doc(familyId).collection($nodeAddress).doc();

//     const peopleRef = this.db.collection($nodePeople).doc();

//     let addressData: any = (JSON.parse(JSON.stringify(address)))
//     addressData.familyUid = familyId
//     addressData.uid = addressRef.id

//     person.uid = peopleRef.id
//     let peopleData: any = (JSON.parse(JSON.stringify(person)))

//     peopleData.updateAt = firebase.firestore.FieldValue.serverTimestamp()
//     peopleData.writeAt = firebase.firestore.FieldValue.serverTimestamp()
//     peopleData.familyUid = familyId
//     peopleData.address = addressString

//     batch.set(addressRef, addressData);
//     batch.set(peopleRef, peopleData);

//     return batch.commit();
//   }

//   uploadFile(upload: Service): firebase.storage.UploadTask {
//     const storageRef = firebase.storage().ref();
//     return storageRef.child('/routes/' + upload.uid + '/' + upload.file.name)
//       .put(upload.file);
//   }

// }