import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ActionService } from '../../core/util/action.service';
import { ArrayService } from '../../core/static/array.service';
import { StringService } from '../../core/static/string.service';
import { TextService } from 'src/app/core/static/text.service';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {

  @Output()
  readonly confirm = new EventEmitter<boolean>();

  constructor(public act: ActionService, public arr: ArrayService, public str: StringService,
    public textService: TextService) {

  }

  ngOnInit() {
    this.act.onInitReport()

  }

  onSubmit(valid: boolean): void {
    if(valid) {
      this.confirm.emit(true);
    } else {
      this.act.onCancel();
    }
  }
}
