import { Component, OnInit } from '@angular/core';
import { ActionService } from '../../core/util/action.service';
import { ArrayService } from '../../core/static/array.service';
import { StringService } from '../../core/static/string.service';
import { transition, trigger, style, animate, state, keyframes } from "@angular/animations";

@Component({
  selector: 'app-alert-popup',
  templateUrl: './alert-popup.component.html',
  styleUrls: ['./alert-popup.component.css']
})
export class AlertPopupComponent implements OnInit {

  constructor(public act: ActionService, public arr: ArrayService, public str: StringService) {
    
  }

  ngOnInit() {
 
  }
}
