import { Component, OnInit } from '@angular/core';
import { ActionService } from '../../core/util/action.service';
import { ArrayService } from '../../core/static/array.service';
import { StringService } from '../../core/static/string.service';
import { AuthService } from 'src/app/core/auth.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor(public act: ActionService, 
              public arr: ArrayService, 
              public str: StringService, 
              public auth: AuthService) { }

  ngOnInit() {
  }

}
