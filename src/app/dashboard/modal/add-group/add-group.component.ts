import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TextService } from 'src/app/core/static/text.service';
import { ActionService } from 'src/app/core/util/action.service';
import { StringService } from 'src/app/core/static/string.service';

@Component({
  selector: 'app-add-group',
  templateUrl: './add-group.component.html',
  styleUrls: ['./add-group.component.css']
})
export class AddGroupComponent implements OnInit {
  @Output()
  readonly confirm = new EventEmitter<boolean>();

  constructor(public text: TextService,
              public act: ActionService,
              public str: StringService) { }

  ngOnInit() {
  }

  onSubmit(valid: boolean): void {
    if(valid) {
      this.confirm.emit(true);
    } else {
      this.act.onCancel();
    }
  }
}
