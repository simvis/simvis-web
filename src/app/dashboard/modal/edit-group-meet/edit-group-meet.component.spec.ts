import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditGroupMeetComponent } from './edit-group-meet.component';

describe('EditGroupMeetComponent', () => {
  let component: EditGroupMeetComponent;
  let fixture: ComponentFixture<EditGroupMeetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditGroupMeetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditGroupMeetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
