import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActionService } from 'src/app/core/util/action.service';
import { TextService } from 'src/app/core/static/text.service';

@Component({
  selector: 'app-edit-group-meet',
  templateUrl: './edit-group-meet.component.html',
  styleUrls: ['./edit-group-meet.component.css']
})
export class EditGroupMeetComponent implements OnInit {

  @Output()
  readonly confirm = new EventEmitter<boolean>();

  constructor(public act: ActionService, 
              public text: TextService) { }

  ngOnInit() {
  }

  onSubmit(valid: boolean): void {
    if(valid) {
      this.confirm.emit(true);
    } else {
      this.act.onCancel();
    }
  }

}
