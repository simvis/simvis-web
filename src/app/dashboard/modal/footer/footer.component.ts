import { Component, OnInit, Input } from '@angular/core';
import { ActionService } from '../../../core/util/action.service';
import { TextService } from '../../../core/static/text.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  @Input()
  label: string;
  @Input()
  hasSwitch: boolean;
  @Input()
  statusValue: number;
  @Input()
  valid: boolean;
  @Input()
  action: string;
  @Input()
  active: boolean = false;
  @Input() 
  actType: string = 'default';
  // @Input()
  // pos: number;
  constructor(public act: ActionService, public text: TextService) { }


  ngOnInit() {
  }
 

  onSubmit(valid: boolean): void {
    // if(valid) {
    //   this.act.onConfirm(this.action);
    // } else {
    //   this.act.onCancel();
    // }
    valid ? this.act.onConfirm(this.action) : this.act.onCancel();

  }


 

}
