import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActionService } from 'src/app/core/util/action.service';
import { ArrayService } from 'src/app/core/static/array.service';
import { TextService } from 'src/app/core/static/text.service';
import { StringService } from 'src/app/core/static/string.service';


@Component({
  selector: 'app-scvf-frequency',
  templateUrl: './scvf-frequency.component.html',
  styleUrls: ['./scvf-frequency.component.css']
})
export class ScvfFrequencyComponent implements OnInit {
  @Output()
  readonly confirm = new EventEmitter<boolean>();

  constructor(public act: ActionService, public arr: ArrayService, public text: TextService, public str: StringService) {

  }

  ngOnInit() {
    
  }

  onSubmit(valid: boolean): void {
    if(valid) {
      this.confirm.emit(true);
    } else {
      this.act.onCancel();
    }
  }
}
