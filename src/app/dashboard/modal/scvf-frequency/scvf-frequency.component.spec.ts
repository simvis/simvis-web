import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScvfFrequencyComponent } from './scvf-frequency.component';

describe('ScvfFrequencyComponent', () => {
  let component: ScvfFrequencyComponent;
  let fixture: ComponentFixture<ScvfFrequencyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScvfFrequencyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScvfFrequencyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
