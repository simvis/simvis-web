import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { TextService } from 'src/app/core/static/text.service';
import { ActionService } from 'src/app/core/util/action.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  @Output()
  readonly confirm = new EventEmitter<boolean>();  
  
  constructor(public text: TextService, public act: ActionService) { }

  ngOnInit() {
  }

  onSubmit(valid: boolean): void {
    if(valid) {
      this.confirm.emit(true);
    } else {
      this.act.onCancel();
    }
  }

}
