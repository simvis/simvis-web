import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item.component.html',
  styleUrls: ['./edit-item.component.css']
})
export class EditItemComponent implements OnInit {


  @Input()
  title: string;

  constructor() { }

  ngOnInit() {
  }

}
