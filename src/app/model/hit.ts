import { Hits } from "./hits";

export interface Hit {
    total: any;
    max_score: any;
    hits: Hits[];
}