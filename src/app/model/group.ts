import { User } from "./user";
import { City } from "./city";

export class Group {
    uid: string;
    lastUid: string;
    groupUid: string;
    name: string;
    sector: string;
    status: string;
    pickedDate: Date;
    city: City;
    updateAt: Date;
    meet: any;
}