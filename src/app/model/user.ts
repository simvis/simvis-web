import { Roles } from "./roles";
import { City } from "./city";

export class User {
    uid: string;
    password: string
    email: string;
    photoURL: string;
    displayName: string;
    fullName: string
    sector: number;
    proCode: string;
    subSector: number;
    city: City
    roles: Roles;
    signinAt: Date;
    active: boolean;
    phone: string;
}