import { Coords } from "./coords";
import { City } from './city';

export interface Address {
    uid: string;
    street: string;
    district: any;
    number: string;
    complement: string;
    reference: string;
    cep: string
    city: City;
    cityCode: string;
    state: string;
    country: string;
    coords: Coords;
    familyUid: string;
}