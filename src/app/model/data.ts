
export interface Data {
    title: string;
    text: string;
    parentUid: string;
    childUid: string;
    date: Date;
    value: string;
    amount: number;
  }
  