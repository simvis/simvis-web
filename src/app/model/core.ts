import { City } from './city';

export interface Core {
    uid: string;
    lastUid: string;
    coreUid: string; 
    pickedDate: Date;
    name: string;
    sector: string;
    status: string;
    city: City;
    updateAt: Date;
    meet: any;
}