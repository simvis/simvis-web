import { Hit } from "./hit";

export interface Elastic {
    took: any;
    timed_out: any;
    _shards: any;
    hits: Hit;
}