import { Address } from './address';

export interface Person {
    uid: string;
    name: string;
    nickname: string
    gender: number;
    kinship: number;
    responsible: boolean;
    nis: string;
    rg: string;
    cpf: string;
    birthday: Date;
    phoneNum: string;
    addressUid: string;
    address: Address;
    writeAt: Date;
    present: boolean;
    updateAt: Date;
    familyUid: string;
    target: string;
    income: string;
    oldFamilyId: string;
    //groups
    active: boolean;
    group: any;
    benefit: string;
    age: number;
    deleted: boolean;
    index: number;
    frequency: string[];
    dates: Date[];
    deficiency: string;
    deficiencies: number[];
    extraInformations: number[];
    benefitRef: number;
}