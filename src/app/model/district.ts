import { City } from "./city";

export class District {
    name: string
    city: City
    uid: string
    writeAt: Date
    updateAt: Date
}