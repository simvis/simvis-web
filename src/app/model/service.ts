import { User } from "./user";
import { Person } from "./person";
import { IO } from "./io";
import { City } from "./city";
import { Data } from "./data";
import { Note } from './note';
import { Address } from './address';
import { Core } from './core';
import { Group } from './group';
import { Plan } from './plan';
//import { School } from './school';

export class Service {
    uid: string;
    familyUid: string;
    fspUid: string;
    referenceUid: string;
    city: City;
    address: Address;
    time: Date;
    node: string;
    type: number;
    isAnswer: boolean;
    detail: string;
    extraDetail: string;
    status: number;
    fileUrl: string;
    schoolPeriod: string;
    currentSchool: string;
    school: any;
    shiftWork: string;
    file: File;
    user: User;
    notes: Note[];
    sector: string;
    person: Person;
    index: number
    perCapita: number;
    deficiency: string;
    extraInformations: string[];
    incomeSources: string[];
    reason: number;
    benefit: number;
    pickedDate: Date;
    requestDate: Data;
    course: string;
    objective: string;
    weakness: string;
    force: string;
    promise: string;
    accessForm: string;
    input: IO;
    output: IO;
    monthUpdate: Date;
    quartUpdate: Date;
    updateAt: any;
    updateBy: User
    writeAt: any;
    people: Person[];
    document: string;
    name: string;
    numNotification: string;
    numBook: string;
    gender: number;
    situation: string;
    income: string;
    schooling: string;
    shift: string;
    local: any;
    workActivity: string;
    period: string;
    moneyDestiny: string;
    socialProgram: string;
    typeSpec: string;
    complaint: string;
    goal: string;
    modality: number;
    interviewer: string;
    number: string;
    resource: string
    effect: string
    deleted: boolean 
    response: string
    datas: string;
    dataOne: Data;
    dataThree: Data;
    dataTwo: Data;
    register: string
    include: string
    activity: string;
    professional: string;
    proceed: string;
    plan: Plan;
    core: Core;
    target: number;
    listUser: string[];
    group: Group;
    responsibleUid: string;
    lastMap: {[key: string]: boolean};
    map: {[key: string]: boolean};
    note: string;
    noteMonth: string;
    month: number;
    quart: number;
    rangeAge: number;
    //Boolean status
    active: boolean;
    isRegistered: boolean
    isActived: boolean
    isIncluded: boolean
    hasInputOne: boolean
    hasInputTwo: boolean
    hasOutputOne: boolean
    hasOutputTwo: boolean
    hasOutputThree: boolean
    hasOutputFour: boolean
    isSchool: boolean
    isPsf: boolean
}