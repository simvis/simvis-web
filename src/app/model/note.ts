import { User } from "./user";

export class Note {
    uid: string;
    parentUid: string;
    familyUid: string;
    text: string;
    writeAt: Date;
    canEdit: boolean;
    onwer: string;
    sector: string;
    updateAt: any;
    user: User;
    date: Date;
    frequency: number;
}