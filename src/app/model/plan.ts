export class Plan {
    month: number;
    rangeAge: number;
    activities: string[];
    comments: string;
    objective: string;
    objects: string;
}