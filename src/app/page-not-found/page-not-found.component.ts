import { Component, OnInit } from '@angular/core';
import { TextService } from '../core/static/text.service';
import { ActionService } from '../core/util/action.service';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.css']
})
export class PageNotFoundComponent implements OnInit {

  constructor(public text: TextService, 
    public act: ActionService) { }

  ngOnInit() {
  }

}
