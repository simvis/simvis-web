import { Component, OnInit } from '@angular/core';
import { ActionService } from 'src/app/core/util/action.service';
import { ArrayService } from 'src/app/core/static/array.service';
import { TextService } from 'src/app/core/static/text.service';

@Component({
  selector: 'app-hc-plan-sheet',
  templateUrl: './hc-plan-sheet.component.html',
  styleUrls: ['./hc-plan-sheet.component.css']
})
export class HcPlanSheetComponent implements OnInit {

  constructor(public act: ActionService, public arr: ArrayService, public textService: TextService) {}

  ngOnInit() {
  }

}
