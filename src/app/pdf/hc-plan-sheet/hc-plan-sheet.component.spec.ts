import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HcPlanSheetComponent } from './hc-plan-sheet.component';

describe('HcPlanSheetComponent', () => {
  let component: HcPlanSheetComponent;
  let fixture: ComponentFixture<HcPlanSheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HcPlanSheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HcPlanSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
