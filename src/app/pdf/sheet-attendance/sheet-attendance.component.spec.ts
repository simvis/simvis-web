import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SheetAttendanceComponent } from './sheet-attendance.component';

describe('SheetAttendanceComponent', () => {
  let component: SheetAttendanceComponent;
  let fixture: ComponentFixture<SheetAttendanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SheetAttendanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SheetAttendanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
