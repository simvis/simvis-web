import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SheetVisitComponent } from './sheet-visit.component';

describe('SheetVisitComponent', () => {
  let component: SheetVisitComponent;
  let fixture: ComponentFixture<SheetVisitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SheetVisitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SheetVisitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
