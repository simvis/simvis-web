import { Component, OnInit } from '@angular/core';
import { ActionService } from 'src/app/core/util/action.service';
import { AuthService } from 'src/app/core/auth.service';
import { ArrayService } from 'src/app/core/static/array.service';

@Component({
  selector: 'app-sheet-visit',
  templateUrl: './sheet-visit.component.html',
  styleUrls: ['./sheet-visit.component.css']
})
export class SheetVisitComponent implements OnInit {

  constructor(public act: ActionService,
    public arr: ArrayService) { }

  ngOnInit() {
  }

}
