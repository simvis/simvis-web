import { Component, OnInit } from '@angular/core';
import { ActionService } from 'src/app/core/util/action.service';
import { TextService } from 'src/app/core/static/text.service';
import { ArrayService } from 'src/app/core/static/array.service';

@Component({
  selector: 'app-report-people-list',
  templateUrl: './report-people-list.component.html',
  styleUrls: ['./report-people-list.component.css']
})
export class ReportPeopleListComponent implements OnInit {

  constructor(public act: ActionService,
     public text: TextService,
     public arr: ArrayService) { }

  ngOnInit() {
  }

}
