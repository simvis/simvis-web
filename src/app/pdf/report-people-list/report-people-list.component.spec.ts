import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportPeopleListComponent } from './report-people-list.component';

describe('ReportPeopleListComponent', () => {
  let component: ReportPeopleListComponent;
  let fixture: ComponentFixture<ReportPeopleListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportPeopleListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportPeopleListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
