import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportFamilyComponent } from './report-family.component';

describe('ReportFamilyComponent', () => {
  let component: ReportFamilyComponent;
  let fixture: ComponentFixture<ReportFamilyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportFamilyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportFamilyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
