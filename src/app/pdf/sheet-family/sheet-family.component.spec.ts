import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SheetFamilyComponent } from './sheet-family.component';

describe('SheetFamilyComponent', () => {
  let component: SheetFamilyComponent;
  let fixture: ComponentFixture<SheetFamilyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SheetFamilyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SheetFamilyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
