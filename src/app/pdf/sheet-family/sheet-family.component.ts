import { Component, OnInit } from '@angular/core';
import { ActionService } from 'src/app/core/util/action.service';
import { TextService } from 'src/app/core/static/text.service';

@Component({
  selector: 'app-sheet-family',
  templateUrl: './sheet-family.component.html',
  styleUrls: ['./sheet-family.component.css']
})
export class SheetFamilyComponent implements OnInit {

  constructor(public act: ActionService, public text: TextService) { }

  ngOnInit() {
  }

}
