import { Component, OnInit } from '@angular/core';
import { ActionService } from 'src/app/core/util/action.service';

@Component({
  selector: 'app-report-meeting',
  templateUrl: './report-meeting.component.html',
  styleUrls: ['./report-meeting.component.css']
})
export class ReportMeetingComponent implements OnInit {

  constructor(public act: ActionService) { }

  ngOnInit() {
    this.act.onInitGroupMeetReport();
  }

}
