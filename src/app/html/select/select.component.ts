import { Component, Input, Output, EventEmitter, ViewChild, ElementRef, OnInit, AfterViewInit } from '@angular/core'
import { ActionService } from 'src/app/core/util/action.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.css']
})
export class SelectComponent {

  @Input()
  form: NgForm;
  @Input()
  class: any[];
  @Input()
  array: any[];
  @Input()
  index: number;
  @Input()
  label: string;
  @Input()
  hint: string;
  @Input()
  canChange: boolean = true;
  @Input()
  req: boolean = false;

  @Output()
  changeOption: EventEmitter<number> = new EventEmitter<number>();

  constructor(public act: ActionService) {

  }

  optionSelected(index: number, el): void {
    // this.index = index;
    console.log(el, 'FORM', this.form);

    this.changeOption.emit(index);
  }
}