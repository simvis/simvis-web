import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ActionService } from 'src/app/core/util/action.service';
import { NgForm, NgModel, ControlContainer } from '@angular/forms';

@Component({
  selector: 'app-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.css'], viewProviders: [ { provide: ControlContainer, useExisting: NgForm } ]
})

export class TextareaComponent {

  @Input()
  model: string;
  @Input()
  label: string;
  @Input()
  canChange: boolean = true;
  @Input()
  req: boolean;

  @Output()
  changeText: EventEmitter<string> = new EventEmitter<string>();

  constructor(public act: ActionService) {

  }

  ngOnInit() {

  }

  onEdit(text: string, textarea: NgModel): void {
    console.log(textarea);
    this.changeText.emit(text);
  }
}