import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';



@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.css']
})
export class CheckboxComponent {

  @Input()
  array: any[];
  @Input()
  indexes: number[];
  @Input()
  label: string;
  @Input()
  uid: string = '';
  
  @Output()
  changeOption: EventEmitter<number> = new EventEmitter<number>();
  
  @Input()
  required: boolean = false;
  constructor() {
    
  }

  //Used to checked a checkbox
  indexOf(index: number, indexes: number[]): boolean {
    return this.indexes.indexOf(index) != -1
  }

  optionSelected(index: number): void {
    // this.index = index;
    console.log(index);
    console.log(this.uid);
    
    
    this.changeOption.emit(index);
  }
}