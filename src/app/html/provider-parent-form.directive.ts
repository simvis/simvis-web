import { Directive, Optional, SkipSelf } from '@angular/core';
import { ControlContainer } from '@angular/forms';


export const containerFactory = (container: ControlContainer) => container;

@Directive({
  selector: '[parentForm]',
  providers: [{
      provide: ControlContainer,
      deps: [[new Optional(), new SkipSelf(), ControlContainer]],
      useFactory: containerFactory
  }]
})
export class ProviderParentFormDirective { }