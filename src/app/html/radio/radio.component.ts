import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-radio',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.css']
})
export class RadioComponent {

  @Input()
  index: number; 
  @Input()
  label: string;
  @Input()
  array: any;
  @Input()
  required: boolean = true;
  @Output()
  readonly changeOption = new EventEmitter<number>();


  constructor() { }

 
  optionSelected(index: number): void {
    console.log(index);
    
    this.index = index;
    this.changeOption.emit(index);
  }
}
