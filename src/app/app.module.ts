import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
//Router
import { RouterModule, Routes } from '@angular/router';
//Maps
import { AgmCoreModule } from '@agm/core';
//Firebase
import * as firebase from 'firebase/app';
import { environment } from '../environments/environment';
//owner components
import { GoogleMapComponent } from './dashboard/google-map/google-map.component';
import { UserProfileComponent } from './dashboard/user-profile/user-profile.component'
import { AuthService } from './core/auth.service';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AuthGuard } from './core/auth.guard';
import { FamilyDetailComponent } from './dashboard/family/family-detail/family-detail.component';
import { ConfirmDialogComponent } from './dashboard/confirm-dialog/confirm-dialog.component';
import { EditAddressComponent } from './dashboard/family/edit-address/edit-address.component';
import { EditPersonComponent } from './dashboard/family/edit-person/edit-person.component';
import { EditNoteComponent } from './dashboard/family/edit-note/edit-note.component';
import { EditRouteComponent } from './dashboard/family/edit-route/edit-route.component';
import { EditFamilyComponent } from './dashboard/family/edit-family/edit-family.component';
import { EditVisitComponent } from './dashboard/family/edit-visit/edit-visit.component';

import { EditGroupComponent } from './dashboard/group/edit-group/edit-group.component';
import { SetGroupComponent } from './dashboard/family/set-group/set-group.component';
import { EditBenefitComponent } from './dashboard/family/edit-benefit/edit-benefit.component';
import { EditReferenceComponent } from './dashboard/family/edit-reference/edit-reference.component';
import { EditAttendanceComponent } from './dashboard/family/edit-attendance/edit-attendance.component';
import { EditScheduleAttendanceComponent } from './dashboard/family/edit-schedule-attendance/edit-schedule-attendance.component';
import { EditFspComponent } from './dashboard/family/edit-fsp/edit-fsp.component';
import { EditHappyChildComponent } from './dashboard/family/edit-happy-child/edit-happy-child.component';
import { EditOldServiceComponent } from './dashboard/family/edit-old-service/edit-old-service.component';
import { EditNotificationComponent } from './dashboard/family/edit-notification/edit-notification.component';
import { EditTermComponent } from './dashboard/family/edit-term/edit-term.component';
import { EditDemandComponent } from './dashboard/family/edit-demand/edit-demand.component';
import { EditAcessuasComponent } from './dashboard/family/edit-acessuas/edit-acessuas.component';
import { EditAepetiComponent } from './dashboard/family/edit-aepeti/edit-aepeti.component';
import { EditConditionComponent } from './dashboard/family/edit-condition/edit-condition.component';
import { EditComplaintComponent } from './dashboard/family/edit-complaint/edit-complaint.component';
import { EditInterviewComponent } from './dashboard/family/edit-interview/edit-interview.component';
import { EditRequestComponent } from './dashboard/family/edit-request/edit-request.component';
import { EditFpgbComponent } from './dashboard/family/edit-fpgb/edit-fpgb.component';
import { EditMilkServiceComponent } from './dashboard/family/edit-milk-service/edit-milk-service.component';
import { EditScfvComponent } from './dashboard/family/edit-scfv/edit-scfv.component';
import { ItemPersonComponent } from './dashboard/family/item/item-person/item-person.component';
import { ItemNoteComponent } from './dashboard/family/item/item-note/item-note.component';
import { ItemServiceComponent } from './dashboard/family/item/item-service/item-service.component';
import { BsDatepickerModule, ModalModule, CollapseModule, BsLocaleService, AlertModule } from 'ngx-bootstrap';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { ptBrLocale } from 'ngx-bootstrap/locale';
import { StringService } from './core/static/string.service';
import { EditPendencyComponent } from './dashboard/family/edit-pendency/edit-pendency.component';
import { NgxMaskModule } from 'ngx-mask'
import { ReportComponent } from './dashboard/report/report.component';
import { SmsComponent } from './dashboard/sms/sms.component';
import { UsersComponent } from './dashboard/users/users.component';
import { MeetingComponent } from './dashboard/modal/meeting/meeting.component';
import { ContactComponent } from './dashboard/contact/contact.component';
import { FrequencyComponent } from './dashboard/frequency/frequency.component';
import { AddUserComponent } from './dashboard/add-user/add-user.component';
import { NewCoreComponent } from './dashboard/new-core/new-core.component';
import { DistricComponent } from './dashboard/distric/distric.component';
import { AlertPopupComponent } from './dashboard/alert-popup/alert-popup.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ContentComponent } from './dashboard/content/content.component';
import { MenuComponent } from './dashboard/menu/menu.component';
import { MenuFamilyComponent } from './dashboard/family/menu-family/menu-family.component';
import { FamilyDataListComponent } from './dashboard/family/family-data-list/family-data-list.component';
import { EditGroupMeetComponent } from './dashboard/modal/edit-group-meet/edit-group-meet.component';
import { ScvfFrequencyComponent } from './dashboard/modal/scvf-frequency/scvf-frequency.component';
import { MeetComponent } from './dashboard/group/meet/meet.component';
import { ReportScfvComponent } from './dashboard/frequency/report-scfv/report-scfv.component';
import { AddGroupComponent } from './dashboard/modal/add-group/add-group.component';
import { ReportGroupComponent } from './dashboard/group/report-group/report-group.component';
import { MeetScfvComponent } from './dashboard/frequency/meet-scfv/meet-scfv.component';
import { EditDistrictComponent } from './dashboard/modal/edit-district/edit-district.component';
import { SheetVisitComponent } from './pdf/sheet-visit/sheet-visit.component';
import { SheetAttendanceComponent } from './pdf/sheet-attendance/sheet-attendance.component';
import { SheetFamilyComponent } from './pdf/sheet-family/sheet-family.component';
import { ReportMeetingComponent } from './pdf/report-meeting/report-meeting.component';
import { ReportPeopleListComponent } from './pdf/report-people-list/report-people-list.component';
import { MeetingListComponent } from './pdf/meeting-list/meeting-list.component';
import { ReportFamilyComponent } from './pdf/report-family/report-family.component';
import { CityInfoComponent } from './dashboard/city-info/city-info.component';
import { ChangePasswordComponent } from './dashboard/modal/change-password/change-password.component';
import { SelectComponent } from './html/select/select.component';
import { RadioComponent } from './html/radio/radio.component';
import { CheckboxComponent } from './html/checkbox/checkbox.component';
import { TextareaComponent } from './html/textarea/textarea.component';
import { HcPlanSheetComponent } from './pdf/hc-plan-sheet/hc-plan-sheet.component';
import { PrintService } from './core/util/print.service';

import * as moment from 'moment';
import 'moment/locale/pt-br';
import { ModalFooterComponent } from './html/modal-footer/modal-footer.component';
import { ProviderParentFormDirective } from './html/provider-parent-form.directive';
import { FooterComponent } from './dashboard/modal/footer/footer.component';
import { SearchComponent } from './dashboard/search/search.component';
import { PsfComponent } from './dashboard/psf/psf.component';
import { SchoolComponent } from './dashboard/school/school.component';
import { CourseComponent } from './dashboard/course/course.component';
import { EditItemComponent } from './dashboard/modal/edit-item/edit-item.component';
defineLocale('pt-br', ptBrLocale);
//define locale
moment.locale('pt-br');

const appRoutes: Routes = [
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard],
  children: [
    { path: 'content', component: ContentComponent},
    { path: 'group', component: EditGroupComponent},
    { path: 'group/meet', component: MeetComponent},
    { path: 'group/report-group', component: ReportGroupComponent},
    { path: 'family', component: EditFamilyComponent },
    { path: 'profile', component: UserProfileComponent },
    { path: 'report', component: ReportComponent },
    { path: 'sms', component: SmsComponent },
    { path: 'users', component: UsersComponent },
    { path: 'meeting', component: MeetingComponent },
    { path: 'contact', component: ContactComponent },
    { path: 'city', component: CityInfoComponent },
    { path: 'frequency', component: FrequencyComponent },
    { path: 'frequency/report-scfv', component: ReportScfvComponent},
    { path: 'frequency/meet', component: MeetScfvComponent},
    { path: 'district', component: DistricComponent},
    { path: 'psf', component: PsfComponent}
  ]
  },
  { path: 'text', component: EditHappyChildComponent},

  { path: 'print/sheet-attendance', component: SheetAttendanceComponent},
  { path: 'report-people-list', component: ReportPeopleListComponent},
  { path: 'report-meeting', component: ReportMeetingComponent},
  { path: 'meeting-list', component: MeetingListComponent},
  { path: 'report-family', component: ReportFamilyComponent},
  { path: 'dashboard/family-detail/:uid', component: FamilyDetailComponent,  canActivate: [AuthGuard]},
  { path: 'dashboard/family-data-list/:uid', component: FamilyDataListComponent,  canActivate: [AuthGuard]},
  { path: '', component: HomeComponent, pathMatch: 'full'},
  { path: '**', component: PageNotFoundComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    GoogleMapComponent,
    UserProfileComponent,
    HomeComponent,
    DashboardComponent,
    PageNotFoundComponent,
    FamilyDetailComponent,
    ConfirmDialogComponent,
    EditAddressComponent,
    EditPersonComponent,
    EditNoteComponent,
    EditRouteComponent,
    EditFamilyComponent,
    EditVisitComponent,
    EditGroupComponent,
    SetGroupComponent,
    EditBenefitComponent,
    EditReferenceComponent,
    EditAttendanceComponent,
    EditScheduleAttendanceComponent,
    EditFspComponent,
    EditHappyChildComponent,
    EditOldServiceComponent,
    EditNotificationComponent,
    EditTermComponent,
    EditDemandComponent,
    EditAcessuasComponent,
    EditAepetiComponent,
    EditConditionComponent,
    EditComplaintComponent,
    EditInterviewComponent,
    EditRequestComponent,
    EditFpgbComponent,
    EditMilkServiceComponent,
    EditScfvComponent,
    ItemPersonComponent,
    ItemNoteComponent,
    ItemServiceComponent,
    EditPendencyComponent,
    ReportComponent,
    SmsComponent,
    UsersComponent,
    MeetingComponent,
    ContactComponent,
    FrequencyComponent,
    AddUserComponent,
    NewCoreComponent,
    DistricComponent,
    AlertPopupComponent,
    ContentComponent,
    MenuComponent,
    MenuFamilyComponent,
    FamilyDataListComponent,
    EditGroupMeetComponent,
    ScvfFrequencyComponent,
    MeetComponent,
    ReportScfvComponent,
    AddGroupComponent,
    ReportGroupComponent,
    MeetScfvComponent,
    EditDistrictComponent,
    SheetVisitComponent,
    SheetAttendanceComponent,
    SheetFamilyComponent,
    ReportMeetingComponent,
    ReportPeopleListComponent,
    MeetingListComponent,
    ReportFamilyComponent,
    CityInfoComponent,
    ChangePasswordComponent,
    SelectComponent,
    RadioComponent,
    CheckboxComponent,
    TextareaComponent,
    HcPlanSheetComponent,
    ModalFooterComponent,
    ProviderParentFormDirective,
    FooterComponent,
    SearchComponent,
    PsfComponent,
    SchoolComponent,
    CourseComponent,
    EditItemComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AgmCoreModule.forRoot({apiKey: environment.googleMapsKey}),
    RouterModule.forRoot(appRoutes, {enableTracing: true}),
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot(),
    CollapseModule.forRoot(),
    HttpClientModule,
    NgxMaskModule.forRoot(),
    BrowserAnimationsModule,
    AlertModule.forRoot()
  ],
  providers: [
    AuthService,
    AuthGuard,
    PrintService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private localeService: BsLocaleService, private str: StringService) {
    this.localeService.use(this.str.ptbr)
    firebase.initializeApp(environment.firebase);
  }
}
