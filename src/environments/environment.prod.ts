export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyBEIr1SlJSFBPi6xHui9t0mXimNQwGDLFw",
    authDomain: "simvis-fire.firebaseapp.com",
    databaseURL: "https://simvis-fire.firebaseio.com",
    projectId: "simvis-fire",
    storageBucket: "simvis-fire.appspot.com",
    messagingSenderId: "959335873444"
  },
  searchPeople: 'https://us-central1-simvis-fire.cloudfunctions.net/search_people',
  searchUsers: 'https://us-central1-simvis-fire.cloudfunctions.net/search_users',
  createUser: 'https://us-central1-simvis-fire.cloudfunctions.net/create_user',
  getReportData: 'https://us-central1-simvis-fire.cloudfunctions.net/get_report_data',
  googleMapsKey: 'AIzaSyBEIr1SlJSFBPi6xHui9t0mXimNQwGDLFw'
};
