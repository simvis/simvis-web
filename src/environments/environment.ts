// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBEIr1SlJSFBPi6xHui9t0mXimNQwGDLFw",
    authDomain: "simvis-fire.firebaseapp.com",
    databaseURL: "https://simvis-fire.firebaseio.com",
    projectId: "simvis-fire",
    storageBucket: "simvis-fire.appspot.com",
    messagingSenderId: "959335873444"
  },
  searchPeople: 'https://us-central1-simvis-fire.cloudfunctions.net/search_people',
  searchUsers: 'https://us-central1-simvis-fire.cloudfunctions.net/search_users',
  createUser: 'https://us-central1-simvis-fire.cloudfunctions.net/create_user',
  getReportData: 'https://us-central1-simvis-fire.cloudfunctions.net/get_report_data',
  googleMapsKey: 'AIzaSyBEIr1SlJSFBPi6xHui9t0mXimNQwGDLFw'
};