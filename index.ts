

import * as functions from 'firebase-functions';

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

const username = "dandan0092"
const password = "k2karrayos"
//const elasticRootUrl = "http://104.198.19.220//elasticsearch/"
const elasticRootUrl = "http://104.198.19.220/";
const promise = require("request-promise");
const request = require("request");
const whitelist = ["http://localhost:4200", "http://simvis.peleja.org", "https://simvis-fire.firebaseapp.com"];

//curl -XGET -u dandan0092:k2karrayos "http://104.198.19.220/people/_search" -d''

//const cors = require('cors')({origin: true});

const cors = require("cors")({
    origin: function (origin: any, callback: any) {
        if (whitelist.indexOf(origin) !== -1) {
            callback(null, true)
        } else {
            console.log('Origin> ' + origin);
            
            callback(new Error("Not allowed by CORS"));
        }
    }
});


 const admin = require("firebase-admin");
admin.initializeApp();
const db = admin.firestore();


function elasticSearchPost(data: any, url: string): Promise<any> {
    const elasticSearchMethod = "POST";
    const elasticSearchRequest = {
        method: elasticSearchMethod,
        url: url,
        auth: {
            username: username,
            password: password,
        },
        body: data,
        json: true
    };

/*     const elasticSearchRequest = {
        method: elasticSearchMethod,
        url: url,
        body: data,
        json: true
    };
 */
    // Register all writed
    console.log('DATA', JSON.stringify(data));
    return promise(elasticSearchRequest).then((response: any) => {
        console.log("ElasticSearch response", response);
    });
}

function elasticSearchGetOptions(baseUrl: string, body: any): any {
    console.log('URl ', baseUrl, 'Body', JSON.stringify(body));
    
    const user = username;
    const pass = password;
    const options = {
        "method": "GET",
        "url": baseUrl,
        "auth": {
            "user": user,
            "pass": pass
        },
        "body": body,
        json: true
    };

    ///original code
    /* "auth": {
        "user": user,
        "pass": pass,
        "sendImmediately": false
    } */

    /* const options = {
        "method": "GET",
        "url": baseUrl,
        "body": body,
        json: true
    }; */
    return options;
}

function writeServiceHcToCity(data: any, newData: boolean): void {
    const ref = db.collection('cities').doc(data.city.code).collection('service-hc-city').doc(data.uid);

    if (newData) {
        console.log('new doc');

        ref.set(data);
    } else {
        console.log('up doc');

        ref.update(data)
            ;
    }
}

//service-visit
exports.writeServiceVisitToCity = functions.firestore.document('/families/{familyUid}/service-visit/{uid}')
    .onWrite((change: any) => {
        const data = change.after.data();
        const ref = db.collection('cities').doc(data.city.code).collection('service-visit-city').doc(data.uid);

        if (!change.before.data()) {
            return ref.set(data);
        } else {
            return ref.update(data);
        }
    });

//service-schedule
exports.writeServiceScheduleToCity = functions.firestore.document('/families/{familyUid}/service-schedule/{uid}')
    .onWrite((change: any) => {
        const data = change.after.data();
        const ref = db.collection('cities').doc(data.city.code).collection('service-schedule-city').doc(data.uid);

        if (!change.before.data()) {
            return ref.set(data);
        } else {
            return ref.update(data);
        }
    });

//service-demand
exports.writeServiceDemandToCity = functions.firestore.document('/families/{familyUid}/service-demand/{uid}')
    .onWrite((change: any) => {
        const data = change.after.data();
        const ref = db.collection('cities').doc(data.city.code).collection('service-demand-city').doc(data.uid);

        if (!change.before.data()) {
            return ref.set(data);
        } else {
            return ref.update(data);
        }
    });

//service-route
exports.writeServiceRouteToCity = functions.firestore.document('/families/{familyUid}/service-route/{uid}')
    .onWrite((change: any) => {
        const data = change.after.data();
        const ref = db.collection('cities').doc(data.city.code).collection('service-route-city').doc(data.uid);

        if (!change.before.data()) {
            return ref.set(data);
        } else {
            return ref.update(data);
        }
    });

//service-fsp
exports.writeServiceFspToCity = functions.firestore.document('/families/{familyUid}/service-fsp/{uid}')
    .onWrite((change: any) => {
        const data = change.after.data();
        const ref = db.collection('cities').doc(data.city.code).collection('service-fsp-city').doc(data.uid);

        if (!change.before.data()) {
            return ref.set(data);
        } else {
            return ref.update(data);
        }
    });

function setFspActive(doc: any, isVoid?: boolean): Promise<void> {
    console.log('DATA.FSPUID', doc);
    const fspRef = db.collection('families').doc(doc.familyUid).collection('service-fsp').doc(doc.fspUid);
    return fspRef.update({ active: false }, { merge: true });
}

//service-fsp-notes
exports.serviceFspNotes = functions.firestore.document('/families/{familyUid}/service-fsp/{fspUid}/service-fsp-notes/{uid}')
    .onWrite((change: any) => {
        const data = change.after.data();

        return setFspActive(data);
    });

//service-visit-notes
exports.serviceVisitNotes = functions.firestore.document('/families/{familyUid}/service-visit/{fspUid}/service-visit-notes/{uid}')
    .onWrite((change: any) => {
        const data = change.after.data();

        return setFspActive(data);
    });

//service-hc-notes 
exports.serviceHctNotes = functions.firestore.document('/families/{familyUid}/service-hc/{fspUid}/service-hc-notes/{uid}')
    .onWrite((change: any) => {
        const data = change.after.data();

        return setFspActive(data);
    });

//service-attendance
exports.writeServiceAttendance = functions.firestore.document('/families/{familyUid}/service-attendance/{uid}')
    .onWrite((change) => {
        const data: any = change.after.data();

        let ref = null;
        //Add service-attendance detail to FSP
        if (data.fspUid) {
            const fspUid = data.fspUid;
            const familyUid = data.familyUid;
            const dataUid = data.uid;
            const note = {
                user: data.user, familyUid: familyUid, text: data.detail,
                parentUid: fspUid, uid: dataUid, updateAt: data.updateAt, writeAt: data.writeAt
            };
            ref = db.collection('families').doc(familyUid).collection('service-fsp')
                .doc(fspUid).collection('notes').doc(dataUid);
            return ref.set(note);
        }

        if (!ref) {
            return Promise.resolve(new Error('ref not setted'));
        }
    });

// Set last data to groups
function setLastMap(node: string, data: any): Promise<any> {
    const cityCode = data.city.code;
    const core = data.core;

    const refCores = db.collection('cities')
        .doc(cityCode).collection(node);
    let refCore = refCores
        .doc(core.uid).collection('people')
        .doc(core.uid);

    if (core.lastUid && data.lastMap) {
        refCore = refCores
            .doc(core.lastUid).collection('people')
            .doc(core.lastUid);
        refCore.update(data.lastMap, { merge: true });
        console.log('lastUid', core.lastUid);
        delete core.lastUid;
        if (data.group) delete data.group.lastUid;
        delete data.lastMap;
    }
    console.log('setLastMap', data);

    if (!data.lastMap) {
        console.log('!LASTMAP')
        refCore = refCores
            .doc(core.uid).collection('people')
            .doc(core.uid);
        return refCore.get()
            .then((doc: any) => {
                console.log('!GET THEN')
                if (!doc.exists) {
                    refCore.set(data.map);
                    console.log('!SET')
                } else {
                    refCore.update(data.map, { merge: true });
                    console.log('!UPDaTe')
                }
            });
    } else {
        return refCores;
    }
}

//service-group
exports.writeServiceGroupToCity = functions.firestore.document('/families/{familyUid}/service-group/{uid}')
    .onWrite((change: any) => {
        const data = change.after.data();
        data.core = data.group;
        return setLastMap('groups', data);
    });

//service-scfv
exports.writeServiceCoreToCity = functions.firestore.document('/families/{familyUid}/service-scfv/{uid}')
    .onWrite((change: any) => {
        const data = change.after.data();

        return setLastMap('cores', data);
    });

//Indexing services to elastic search
exports.indexFamilyDataToElastic = functions.firestore.document('/families/{familyUid}/{serviceCollectionId}/{uid}')
    .onWrite((change: any, context: any) => {
        const data = change.after.data();
        const collection = context.params.serviceCollectionId;

        //Elastic search POST
        // if (collection !== 'people') {
            //FSP - active false when add some info to family
            if (data.fspUid) {
                const fspRef = db.collection('families').doc(data.familyUid).collection('service-fsp').doc(data.fspUid);
                if (collection !== 'service-fsp'
                    || (collection === 'service-fsp' && data.pickedDate === change.before.data().pickedDate)) {
                    fspRef.update({ active: false }, { merge: true });
                }
                if (collection === 'service-reference' && data.status === 4) {
                    fspRef.update({ active: false, status: 4}, { merge: true });
                }
            }

            if (collection === 'service-hc') {
                writeServiceHcToCity(data, !change.before.data());
            }
            console.log('colection NaMe: ' + collection);

            const familyUid = data.familyUid;
            const uid = data.uid;
            //const elasticSearchConfig = functions.config().elasticsearch;
            const url = elasticRootUrl + collection + "/data/" + familyUid + uid;

            return elasticSearchPost(data, url);
        // } else {
        //     ret  

    });

//Indexing users to elastic search
exports.indexUserToElastic = functions.firestore.document("/users/{uid}")
    .onWrite((change: any) => {
        const data: any = change.after.data();
        const userUid = data.uid;

        //Add users to city -- start
        const oldDoc = change.before.data();
        const ref = db.collection('cities').doc(data.city.code).collection('users-city').doc(userUid);
        if (!oldDoc && ref) {
            ref.set(data);
        } else {
            ref.update(data);
        }
        //Add users to city --- end

        //const elasticSearchConfig = functions.config().elasticsearch;
        const url = elasticRootUrl + "users/data/" + userUid;
        return elasticSearchPost(data, url);
    });

//Indexing people to elastic search '/families/{familyUid}/{serviceCollectionId}/{uid}'
exports.indexPearsonToElastic = functions.firestore.document("/people/{uid}")
    .onWrite((change: any) => {
        const data: any = change.after.data();
        const pearsonUid = data.uid;

        //Add people to family -- start
        const oldDoc = change.before.data();

        const ref = db.collection('families').doc(data.familyUid).collection('people').doc(pearsonUid);
        if (!oldDoc && ref) {
            ref.set(data);
        } else {
            ref.update(data);
        }
        //Add people to family --- end

        //const elasticSearchConfig = functions.config().elasticsearch;
        const url = elasticRootUrl + "people/data/" + pearsonUid;
        return elasticSearchPost(data, url);
    });


//Indexing cities districts to elastic search
exports.indexCityDistrictsToElastic = functions.firestore.document('/cities/{cityUid}/districts/{uid}')
    .onWrite((change: any) => {
        const data: any = change.after.data();
        const uid = data.uid;

        const url = elasticRootUrl + "districts/data/" + uid;
        return elasticSearchPost(data, url);
    });


//Indexing cities data to elastic search
exports.indexCityDataToElastic = functions.firestore.document('/cities/{cityUid}/city/{uid}')
    .onWrite((change: any) => {
        const data: any = change.after.data();
        const uid = data.uid;

        const url = elasticRootUrl + "city/data/" + uid;
        return elasticSearchPost(data, url);
    });

//Indexing cities groups data to elastic search
exports.indexCityGroupsDataToElastic = functions.firestore.document('/cities/{cityUid}/groups/{uid}')
    .onWrite((change: any) => {
        const data: any = change.after.data();
        const uid = data.uid;

        const url = elasticRootUrl + "groups/data/" + uid;
        return elasticSearchPost(data, url);
    });


//Indexing cities cores to elastic search
exports.indexCityCoresDataToElastic = functions.firestore.document('/cities/{cityUid}/cores/{uid}')
    .onWrite((change: any) => {
        const data: any = change.after.data();
        const uid = data.uid;

        const url = elasticRootUrl + "cores/data/" + uid;
        return elasticSearchPost(data, url);
    });

//Indexing cores (meeting and people) data to elastic search
exports.indexCityCoresMeetingToElastic = functions.firestore.document('/cities/{cityUid}/cores/{uid}/meeting/{meetingUid}')
    .onWrite((change: any) => {
        const data: any = change.after.data();
        const uid = data.uid;

        const url = elasticRootUrl + "cores-meeting" + "/data/" + uid;
        return elasticSearchPost(data, url);
    });

//Indexing cores (meeting and people) data to elastic search
exports.indexCityCoresPeopleToElastic = functions.firestore.document('/cities/{cityUid}/cores/{uid}/people/{peopleUid}')
    .onWrite((change: any) => {
        const data: any = change.after.data();
        const uid = data.uid;

        const url = elasticRootUrl + "cores-people" + "/data/" + uid;
        return elasticSearchPost(data, url);
    });

//Indexing groups (meeting and people) data to elastic search
exports.indexCityGroupsMeetingToElastic = functions.firestore.document('/cities/{cityUid}/groups/{uid}/meeting/{meetingUid}')
    .onWrite((change: any) => {
        const data: any = change.after.data();
        const uid = data.uid;

        const url = elasticRootUrl + "groups-meeting" + "/data/" + uid;
        return elasticSearchPost(data, url);
    });

//Indexing groups (meeting and people) data to elastic search
exports.indexCityGroupsPeopleToElastic = functions.firestore.document('/cities/{cityUid}/groups/{uid}/people/{peopleUid}')
    .onWrite((change: any) => {
        const data: any = change.after.data();
        const uid = data.uid;

        const url = elasticRootUrl + "groups-people" + "/data/" + uid;
        return elasticSearchPost(data, url);
    });

//Search people
exports.search_people = functions.https.onRequest((req: any, res: any) => {
    cors(req, res, () => {
        const baseUrl = elasticRootUrl + "people/data/_search";

        if(req.query.search !== undefined) {
            console.log('query.search');
        }
        if (req.query.search) {
            console.log('SEARCH......');
            
            const query = req.query.search;
            const body = {
                "query": {
                    "bool": {
                        "must": [
                            {
                                "multi_match": {
                                    "query": query,
                                    "fields": ["name", "cpf", "nis"]
                                }
                            }
                        ],
                        "filter": {
                            "term": { "deleted": "false" }
                        }
                    }
                }
            };
            const options = elasticSearchGetOptions(baseUrl, body);
            request(options, function (error: any, response: any, resBody: any) {
                console.log(resBody);
                
                if (!error && response.statusCode === 200) {
                    res.send(resBody);
                }
                else {
                    res.status(500).send(error);
                }
            });
        }
        else {
            res.status(500).send();
        }
    });
});

exports.create_user = functions.https.onRequest((req: any, res: any) => {
    cors(req, res, () => {
        const email: string = req.query.email;
        const pass: string = req.query.password;
        //const phoneNumber = req.query.phone;
        const displayName: string = req.query.name;
        const photoURL = 'https://firebasestorage.googleapis.com/v0/b/simvis-fire.appspot.com/o/simvis%2Flogo.png?alt=media&token=92b0f338-59dd-408c-bc59-96b6f82c29a9';
        if (email && pass && displayName) {
            admin.auth().createUser({
                email: email,
                emailVerified: false,
                password: pass,
                displayName: displayName,
                photoURL: photoURL,
                disabled: false
            })
                .then(function (userRecord: any) {
                    // See the UserRecord reference doc for the contents of userRecord.
                    console.log('Successfully created new user:', userRecord.uid);
                    res.send({ uid: userRecord.uid, message: 'success' });
                })
                .catch(function (error: any) {
                    console.log('Error creating new user:', error);
                    res.status(500).send(error);
                });
        } else {
            res.status(500).send();
        }
    });
});

//Search users
exports.search_users = functions.https.onRequest((req: any, res: any) => {
    cors(req, res, () => {
        const baseUrl: string = elasticRootUrl + "users/data/_search";
        if (req.query.search && req.query.code) {
            const query: string = req.query.search;
            const cityCode: string = req.query.code;

            const body = {
                "query": {
                    "bool": {
                        "must": [
                            {
                                "multi_match": {
                                    "query": query,
                                    "fields": ["email", "cpf", "fullName"]
                                }
                            }
                        ],

                        "filter": {
                            "term": { "city.code": cityCode }
                        }
                    }
                }
            };

            const options = elasticSearchGetOptions(baseUrl, body);
            request(options, function (error: any, response: any, resBody: any) {
                if (!error && response.statusCode === 200) {
                    res.send(resBody);
                }
                else {
                    res.status(500).send(error);
                }
            });
        }
        else {
            res.status(500).send();
        }
    });
});

// addFilters To array
function addFilterMatch(filter: any, prop: string): any {
    const _filter: any = { match: {} };
    _filter.match[prop] = filter;
    return _filter;
}

// get Report Data From Elastic Search
exports.get_report_data = functions.https.onRequest((req: any, res: any) => {
    cors(req, res, () => {
        console.log('query', req.query);

        if (req.query.index && req.query.startDate && req.query.endDate) {
            const index: string = req.query.index;
            const start_date: string = req.query.startDate;
            const end_date: string = req.query.endDate;
            const city_code: string = req.query.cityCode;
            const baseUrl: string = elasticRootUrl + index + "/_search";

            // {
            //     "query": {
            //       "bool": {   
            //             "must": [
            //               { "match": { "user.uid": "wI21laLHwQPjn3wIxMpM5ML3b8R2" }} 
            //             ]
            //       }
            //     }
            //   }
            const body: any = {
                "query": {
                    "bool": {
                        "must": [
                        ]
                    }
                }
            };

            const must: any[] = [];

            if (index !== 'people') {
                const cityCode: any = {
                    "term": { "city.code": city_code }
                };
                must.push(cityCode);
            }

            if (index === 'service-attendance' && req.query.userUid) {
                must.push(addFilterMatch(req.query.userUid, 'user.uid'));
            }

            if (index === 'service-hc') {
                const query: any = req.query;
                if (query.filter0 >= 0) must.push(addFilterMatch(query.filter0, 'target'));
                if (query.filter1 >= 0) must.push(addFilterMatch(query.filter1 === 1, 'active'));
                if (query.filter2 >= 0) must.push(addFilterMatch(query.filter2, 'rangeAge'));
                if (query.filter3 >= 0) must.push(addFilterMatch(query.filter3, 'professional.uid'));
                // must.push(addFilterMatch(req.query.filter1, 'target'))
            }

            if (index === 'service-benefit' && req.query.filter1) {
                must.push(addFilterMatch(req.query.filter1, 'benefit'));
            }

            if (index === 'service-acessuas' && req.query.filter1) {
                must.push(addFilterMatch(req.query.filter1, 'local'));
            }

            if (index === 'people' && req.query.filter1) {
                must.push(addFilterMatch(req.query.filter1, 'extraInformations'));

                if (req.query.filter2) {
                    must.push(addFilterMatch(req.query.filter1, 'extraInformations'));
                }
            }

            if (index === 'service-reference' && req.query.filter1) {
                // must.push(addFilterMatch(req.query.filter1, 'extraInformations'));

                // if (req.query.filter2) {
                //     must.push(addFilterMatch(req.query.filter1, 'extraInformations'));
                // }
            }

            if (index === 'service-interview' && req.query.filter1) {
                must.push(addFilterMatch(req.query.filter1, 'modality'));
            }

            if (index === 'service-scfv' && req.query.filter1) {
                must.push(addFilterMatch(req.query.filter1, 'core.name'));
            }

            const writeAt: any = {
                "range": {
                    "writeAt._seconds": {
                        "gte": start_date,
                        "lte": end_date
                    }
                }
            }
            must.push(writeAt);
            body.query.bool.must = must;
            console.log('body--3', JSON.stringify(body));

            const options: any = elasticSearchGetOptions(baseUrl, body);
            request(options, function (error: any, response: any, resBody: any) {
                if (!error && response.statusCode === 200) {
                    console.log('body', resBody);

                    res.send(resBody);
                }
                else {
                    res.status(500).send(error);
                }
            });

        } else {
            res.status(500).send();
        }
    });
});