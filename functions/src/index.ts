import * as functions from 'firebase-functions';

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

const username = "dandan0092";
const password = "k2karrayos";
//const elasticRootUrl = "http://104.198.19.220//elasticsearch/"
const elasticRootUrl = "http://104.198.19.220/";
const promise = require("request-promise");
const request = require("request");
const whitelist = ["http://localhost:4200", "http://simvis.peleja.org", "https://simvis-fire.firebaseapp.com"];

//curl -XGET -u dandan0092:k2karrayos "http://104.198.19.220/people/_search" -d''

//const cors = require('cors')({origin: true});

const cors = require("cors")({
    origin: function (origin: any, callback: any) {
        if (whitelist.indexOf(origin) !== -1) {
            callback(null, true)
        } else {
            console.log('Origin> ' + origin);

            callback(new Error("Not allowed by CORS"));
        }
    }
});


const admin = require("firebase-admin");
admin.initializeApp();
const db = admin.firestore();


function elasticSearchPost(data: any, url: string): Promise<any> {
    const elasticSearchMethod = "POST";
    const elasticSearchRequest = {
        method: elasticSearchMethod,
        url: url,
        auth: {
            username: username,
            password: password,
        },
        body: data,
        json: true
    };

    // Register all writed
    console.log('DATA', JSON.stringify(data), 'URL', url);
    return promise(elasticSearchRequest).then((response: any) => {
        console.log("ElasticSearch response", response);
    });
}

function elasticSearchGetOptions(baseUrl: string, body: any): any {
    console.log('URl ', baseUrl, 'Body', JSON.stringify(body));

    const user = username;
    const pass = password;
    const options = {
        "method": "GET",
        "url": baseUrl,
        "auth": {
            "user": user,
            "pass": pass
        },
        "body": body,
        json: true
    };

    return options;
}

//service-attendance
exports.writeServiceAttendance = functions.firestore.document('/service-attendance/{uid}')
    .onWrite((change) => {
        const data: any = change.after.data();

        let ref = null;
        //Add service-attendance detail to FSP
        if (data.fspUid) {
            const fspUid = data.fspUid;
            const familyUid = data.familyUid;
            const dataUid = data.uid;
            const note = {
                user: data.user, familyUid: familyUid, text: data.detail,
                parentUid: fspUid, uid: dataUid, updateAt: data.updateAt, writeAt: data.writeAt
            };
            ref = db.collection('fsp-notes').doc(dataUid);
            return ref.set(note);
        }

        if (!ref) {
            return Promise.resolve(new Error('ref not setted'));
        }
    });

// Set last data to groups
function setLastMap(node: string, data: any): void {
    const core = data.core;

    const refCores = db.collection(node + '-' + 'people');

    if (core.lastUid && data.lastMap) {
        const refCore = refCores.doc(core.lastUid);
        refCore.update(data.lastMap, { merge: true });
        console.log('lastUid', core.lastUid);
        delete core.lastUid;
        if (data.group) delete data.group.lastUid;
        delete data.lastMap;
    }
    console.log('setLastMap', data);

    if (!data.lastMap) {
        console.log('!LASTMAP')
        const refCore = refCores.doc(core.uid);
        refCore.get()
            .then((doc: any) => {
                console.log('!GET THEN')
                if (!doc.exists) {
                    refCore.set(data.map);
                    console.log('!SET')
                } else {
                    refCore.update(data.map, { merge: true });
                    console.log('!UPDaTe')
                }
            });
    }
}

//Indexing services to elastic search
exports.indexDataToElastic = functions.firestore.document('/{dataCollection}/{uid}')
    .onWrite((change: any, context: any) => {
        const data = change.after.data();
        const collection = context.params.dataCollection;

        //Elastic search POST
        // if (collection !== 'people') {
        //FSP - active false when add some info to family
        if (data.fspUid) {
            const fspRef = db.collection('service-fsp').doc(data.fspUid);
            if (collection !== 'service-fsp'
                || (collection === 'service-fsp' && data.pickedDate === change.before.data().pickedDate)) {
                fspRef.update({ active: false }, { merge: true });
            }
            if (collection === 'service-reference' && data.status === 4) {
                fspRef.update({ active: false, status: 4 }, { merge: true });
            }
        }

        if (collection === 'service-group') {
            data.core = data.group;
            setLastMap('group', data);
        }
        if (collection === 'service-scfv') {
            setLastMap('core', data);
        }

        console.log('colection NaMe: ' + collection);

        const uid = context.params.uid;
        //const elasticSearchConfig = functions.config().elasticsearch;
        const url  = elasticRootUrl + collection + "/data/" + uid;

        return elasticSearchPost(data, url);
        // } else {
        //     ret  

    });

//Search people
exports.search_people = functions.https.onRequest((req: any, res: any) => {
    cors(req, res, () => {
        const baseUrl = elasticRootUrl + "people/data/_search";

        if (req.query.search !== undefined) {
            console.log('query.search');
        }
        if (req.query.search) {
            console.log('SEARCH......');

            const query = req.query.search;
            const body = {
                "query": {
                    "bool": {
                        "must": [
                            {
                                "multi_match": {
                                    "query": query,
                                    "fields": ["name", "cpf", "nis"]
                                }
                            }
                        ],
                        "filter": {
                            "term": { "deleted": "false" }
                        }
                    }
                }
            };
            const options = elasticSearchGetOptions(baseUrl, body);
            request(options, function (error: any, response: any, resBody: any) {
                console.log(resBody);

                if (!error && response.statusCode === 200) {
                    res.send(resBody);
                }
                else {
                    res.status(500).send(error);
                }
            });
        }
        else {
            res.status(500).send();
        }
    });
});

exports.create_user = functions.https.onRequest((req: any, res: any) => {
    cors(req, res, () => {
        const email: string = req.query.email;
        const pass: string = req.query.password;
        //const phoneNumber = req.query.phone;
        const displayName: string = req.query.name;
        const photoURL = 'https://firebasestorage.googleapis.com/v0/b/simvis-fire.appspot.com/o/simvis%2Flogo.png?alt=media&token=92b0f338-59dd-408c-bc59-96b6f82c29a9';
        if (email && pass && displayName) {
            admin.auth().createUser({
                email: email,
                emailVerified: false,
                password: pass,
                displayName: displayName,
                photoURL: photoURL,
                disabled: false
            })
                .then(function (userRecord: any) {
                    // See the UserRecord reference doc for the contents of userRecord.
                    console.log('Successfully created new user:', userRecord.uid);
                    res.send({ uid: userRecord.uid, message: 'success' });
                })
                .catch(function (error: any) {
                    console.log('Error creating new user:', error);
                    res.status(500).send(error);
                });
        } else {
            res.status(500).send();
        }
    });
});

//Search users
exports.search_users = functions.https.onRequest((req: any, res: any) => {
    cors(req, res, () => {
        const baseUrl: string = elasticRootUrl + "users/data/_search";
        if (req.query.search && req.query.code) {
            const query: string = req.query.search;
            const cityCode: string = req.query.code;

            const body = {
                "query": {
                    "bool": {
                        "must": [
                            {
                                "multi_match": {
                                    "query": query,
                                    "fields": ["email", "cpf", "fullName"]
                                }
                            }
                        ],

                        "filter": {
                            "term": { "city.code": cityCode }
                        }
                    }
                }
            };

            const options = elasticSearchGetOptions(baseUrl, body);
            request(options, function (error: any, response: any, resBody: any) {
                if (!error && response.statusCode === 200) {
                    res.send(resBody);
                }
                else {
                    res.status(500).send(error);
                }
            });
        }
        else {
            res.status(500).send();
        }
    });
});

// addFilters To array
function addFilterMatch(filter: any, prop: string): any {
    const _filter: any = { match: {} };
    _filter.match[prop] = filter;
    return _filter;
}

// get Report Data From Elastic Search
exports.get_report_data = functions.https.onRequest((req: any, res: any) => {
    cors(req, res, () => {
        console.log('query', req.query);

        if (req.query.index && req.query.startDate && req.query.endDate) {
            const index: string = req.query.index;
            const start_date: string = req.query.startDate;
            const end_date: string = req.query.endDate;
            const city_code: string = req.query.cityCode;
            const baseUrl: string = elasticRootUrl + index + "/_search";

            if (req.query.filter1 === '-1') {
                delete req.query.filter1;
            }

            // {
            //     "query": {
            //       "bool": {   
            //             "must": [
            //               { "match": { "user.uid": "wI21laLHwQPjn3wIxMpM5ML3b8R2" }} 
            //             ]
            //       }
            //     }
            //   }
            const body: any = {
                "query": {
                    "bool": {
                        "must": [
                        ]
                    }
                }
            };

            const must: any[] = [];

            if (index !== 'people') {
                const cityCode: any = {
                    "term": { "city.code": city_code }
                };
                must.push(cityCode);
            }

            if (index === 'service-attendance' && req.query.filter1) {
                must.push(addFilterMatch(req.query.filter1, 'user.uid'));
            }

            if (index === 'service-hc') {
                const query: any = req.query;
                if (query.filter1 >= 0) must.push(addFilterMatch(query.filter1, 'status'));
                if (query.filter2 >= 0) must.push(addFilterMatch(query.filter2, 'rangeAge'));
                if (query.filter3 >= 0) must.push(addFilterMatch(query.filter3, 'professional.uid'));
                if (query.filter4 >= 0) must.push(addFilterMatch(query.filter4, 'target'));

                // must.push(addFilterMatch(req.query.filter1, 'target'))
            }

            if (index === 'service-benefit' && req.query.filter1) {
                must.push(addFilterMatch(req.query.filter1, 'benefit'));
            }

            if (index === 'service-acessuas' && req.query.filter1) {
                must.push(addFilterMatch(req.query.filter1, 'local'));
            }

            if (index === 'people' && req.query.filter1) {
                must.push(addFilterMatch(req.query.filter1, 'extraInformations'));

                if (req.query.filter2) {
                    must.push(addFilterMatch(req.query.filter1, 'extraInformations'));
                }
            }

            if (index === 'service-reference' && req.query.filter1) {
                // must.push(addFilterMatch(req.query.filter1, 'extraInformations'));

                // if (req.query.filter2) {
                //     must.push(addFilterMatch(req.query.filter1, 'extraInformations'));
                // }
            }

            if (index === 'service-interview' && req.query.filter1) {
                must.push(addFilterMatch(req.query.filter1, 'modality'));
            }

            if (index === 'service-scfv' && req.query.filter1) {
                must.push(addFilterMatch(req.query.filter1, 'core.name'));
            }

            const writeAt: any = {
                "range": {
                    "writeAt._seconds": {
                        "gte": start_date,
                        "lte": end_date
                    }
                }
            }
            must.push(writeAt);
            body.query.bool.must = must;
            console.log('body--3', JSON.stringify(body));

            const options: any = elasticSearchGetOptions(baseUrl, body);
            request(options, function (error: any, response: any, resBody: any) {
                if (!error && response.statusCode === 200) {
                    console.log('body', resBody);

                    res.send(resBody);
                }
                else {
                    res.status(500).send(error);
                }
            });

        } else {
            res.status(500).send();
        }
    });
});
  
